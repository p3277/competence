﻿using Competence.Api.Controllers;
using Competence.Api.Models;
using Competence.Domain.Models;
using Competence.Domain.Services.Abstract;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Competence.Api.Test
{
    public class DepartmentTests : IClassFixture<FixureInMemory>
    {
        private readonly DepartmentsController _departmentController;
        private readonly IDepartmentService _departmentService;


        public DepartmentTests(FixureInMemory fixureInMemory)
        {
            _departmentController = fixureInMemory.ServiceProvider.GetService<DepartmentsController>();
            _departmentService = fixureInMemory.ServiceProvider.GetService<IDepartmentService>();

        }

        /// <summary>
        /// Department request test with an ID that is not in the database.
        /// </summary>
        /// <returns>
        /// When executing the test, the request should return the value NotFound, which will mean the successful execution of the test
        /// </returns>
        [Fact]
        public async Task GetDepartment_ReturnOk_ForDesiredValue()
        {
            //Arrange
            var department = _departmentService.GetAllAsync().Result.FirstOrDefault();

            //Act
            var result = await _departmentController.GetDepartmentById(department.Id);

            //Assert
            result.Should().BeOfType<OkObjectResult>();
        }

        /// <summary>
        /// Department request test with an ID that is in the database.
        /// </summary>
        /// <returns>
        /// When executing the test, the query should return the value OK, which means that the test was successful
        /// </returns>
        [Fact]
        public async Task GetDepartment_ReturnsNotFound_ForMissingDepartment()
        {
            //Arrange
            int idDepartment = -1;

            //Act
            var result = await _departmentController.GetDepartmentById(idDepartment);

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }

        /// <summary>
        /// Department creation Test
        /// </summary>
        /// <returns>
        /// Upon successful completion of the test, a department is created in the database and information about it is returned.
        /// </returns>
        [Fact]
        public async Task AddDepartment_ReturnsOk_ForSuccessfullyCreate()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            DepartmentRequest departmentRequest = new DepartmentRequest()
            {
                Name = name
            };

            //Act
            var result = await _departmentController.CreateDepartment(departmentRequest);

            //Assert
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<DepartmentResponse>().Name.Should().Be(name);
        }

        /// <summary>
        ///  Test for the name of department already existing in the specified database
        /// </summary>
        /// <returns>
        /// Returns BadRequest if an attempt is made to create a department with the same name as the specified database.
        /// </returns>
        [Fact]
        public async Task AddDepartment_ReturnsBadRequest_ForExistingDepartment()
        {
            //Arrange
            var name = _departmentService.GetAllAsync().Result.FirstOrDefault().Name;
            DepartmentRequest departmentRequest = new DepartmentRequest()
            {
                Name = name
            };

            //Act
            var result = await _departmentController.CreateDepartment(departmentRequest);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }

        /// <summary>
        /// Department update test with correct data
        /// </summary>
        /// <returns>
        /// Returns the group competency on successful creation
        /// </returns>
        [Fact]
        public async Task UpdateDepartment_ReturnsOk_ForSuccessfullyUpdate()
        {
            //Arrange
            string name = Guid.NewGuid().ToString(); 
            DepartmentRequest departmentRequest = new DepartmentRequest()
            {
                Name = name
            };

            //Act
            var result = await _departmentController.UpdateDepartment(1, departmentRequest);

            //Assert
            result.Should().BeAssignableTo<OkObjectResult>().Which.Value.As<DepartmentResponse>().Name.Should().Be(name);
        }

        /// <summary>
        /// Department update test when specifying an incorrect department number
        /// </summary>
        /// <returns>
        /// Returns a BadRequest if a non-existent department number is specified on update
        /// </returns>
        [Fact]
        public async Task UpdateDepartment_ReturnsBadRequest_ForNonExistingDepartment()
        {
            //Arrange
            DepartmentRequest departmentRequest = new DepartmentRequest()
            {
                Name = "Department_" + DateTime.Now.Ticks
            };

            //Act
            var result = await _departmentController.UpdateDepartment(-1, departmentRequest);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Department update test when specifying the name of a department that is already 
        /// </summary>
        /// <returns>
        /// Returns a BadRequest when given an existing name in a department when updating a department
        /// </returns>
        [Fact]
        public async Task UpdateDepartment_ReturnsBadRequest_ForExistingName()
        {
            //Arrange
            var department1 = _departmentService.GetAllAsync().Result.ElementAt(0);
            var department2 = _departmentService.GetAllAsync().Result.ElementAt(1);

            DepartmentRequest departmentRequest = new DepartmentRequest()
            {
                Name = department2.Name
            };

            //Act
            var result = await _departmentController.UpdateDepartment(department1.Id, departmentRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Checking if a department is deleted when specifying its number
        /// </summary>
        /// <returns>
        /// Returns OkObjectResult if a valid number was specified when deleting a department
        /// </returns>
        [Fact]
        public async Task DeleteDepartment_ReturnsOk_ForSuccessfullyDelete()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            DepartmentDTO entity = new DepartmentDTO()
            {
                Name = name
            };
            var department = await _departmentService.CreateAsync(entity);

            //Act
            var result = await _departmentController.DeleteDepartment(department.Id);

            //Assert
            result.Should().BeOfType<OkResult>();
        }

        /// <summary>
        /// Checking if a department is deleted when specifying its number
        /// </summary>
        /// <returns>
        /// Returns OkObjectResult if a valid number was specified when deleting a department
        /// </returns>
        [Fact]
        public async Task DeleteDepartment_ReturnsNotFound_ForMissingDepartment()
        {
            //Arrange
            int idDepartment = -1;

            //Act
            var result = await _departmentController.DeleteDepartment(idDepartment);

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }
    }
}
