﻿using Competence.Api.Controllers;
using Competence.DataAccess.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Competence.Domain.Services.Abstract;
using Moq;
using Competence.Api.Services;

namespace Competence.Api.Test
{
    public class FixureInMemory : IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }
        public IServiceCollection ServiceCollection { get; set; }
        public FixureInMemory()
        {
            var _loggingService = new Mock<ILoggingService>();
            var _signalService = new Mock<ISignalRService>();
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();

            this.ServiceCollection = Configurator.GetServiceCollection(configuration, null);
            this.ServiceCollection.AddHttpContextAccessor();
            this.ServiceCollection.AddSingleton<ILoggingService>((s) => _loggingService.Object);
            this.ServiceCollection.AddSingleton<ISignalRService>((s) => _signalService.Object);
            this.ServiceCollection.AddTransient<CompetenceController>();
            this.ServiceCollection.AddTransient<CompetenceGroupsController>();
            this.ServiceCollection.AddTransient<DepartmentsController>();
            this.ServiceCollection.AddTransient<CountriesController>();
            this.ServiceCollection.AddTransient<PositionsController>();
            this.ServiceCollection.AddTransient<EmployeeController>();
            this.ServiceProvider = GetServiceProvider();
        }

        private IServiceProvider GetServiceProvider()
        {
            var serviceProvider = ServiceCollection
                .ConfigureInMemoryContext()
                .BuildServiceProvider();

            SeedData(serviceProvider);
            return serviceProvider;
        }

        private void SeedData(IServiceProvider serviceProvider)
        {
            var initializer = serviceProvider.GetService<IDataContextInitializer>();
            initializer.InitializeDb();
        }

        public void Dispose()
        {
        }
    }
}
