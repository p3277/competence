﻿using Competence.Api.Controllers;
using Competence.Api.Models;
using Competence.Domain.Models;
using Competence.Domain.Services.Abstract;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Competence.Api.Test
{
    /// <summary>
    /// Competency test class
    /// </summary>
    public class CompeteceTests : IClassFixture<FixureInMemory>
    {
        private readonly CompetenceController _competenceController;
        private readonly ICompetenceService _competenceService;

        public CompeteceTests(FixureInMemory fixureInMemory)
        {
            _competenceController = fixureInMemory.ServiceProvider.GetService<CompetenceController>();
            _competenceService = fixureInMemory.ServiceProvider.GetService<ICompetenceService>();
        }

        /// <summary>
        /// Competency request test with an ID that is not in the database.
        /// </summary>
        /// <returns>
        /// When executing the test, the request should return the value NotFound, which will mean the successful execution of the test
        /// </returns>
        [Fact]
        public async Task GetCompetence_ReturnsNotFound_ForMissingCompetence()
        {
            //Arrange
            int id = -1;

            //Act
            var result = await _competenceController.GetCompetenceById(id);

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }

        /// <summary>
        /// Competency request test with an ID that is in the database.
        /// </summary>
        /// <returns>
        /// When executing the test, the query should return the value OK , which means that the test was successful
        /// </returns>
        [Fact]
        public async Task GetCompetence_ReturnsOk_ForDesiredValue()
        {
            //Arrange
            var competence = _competenceService.GetAllAsync().Result.FirstOrDefault();

            //Act
            var result = await _competenceController.GetCompetenceById(competence.Id);

            //Assert
            result.Should().BeOfType<OkObjectResult>(); ;
        }

        /// <summary>
        /// Competency Creation Test
        /// </summary>
        /// <returns>
        /// Upon successful completion of the test, a competency is created in the database and information about it is returned.
        /// </returns>
        [Fact]
        public async Task AddCompetence_ReturnsOk_ForSuccessfullyCreate()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            CompetenceRequest competenceRequest = new CompetenceRequest()
            {
                Name = name,
                GroupId = 1
            };

            //Act
            var result = await _competenceController.CreateCompetence(competenceRequest);

            //Assert
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<CompetenceResponse>().Name.Should().Be(name);
        }

        /// <summary>
        /// Test for the name of competencies already existing in the specified group
        /// </summary>
        /// <returns>
        /// Returns BadRequest if an attempt is made to create a competency with the same name as the specified group.
        /// </returns>
        [Fact]
        public async Task AddCompetence_ReturnsBadRequest_ForExistingCompetenciesInGroup()
        {
            //Arrange
            var competence = _competenceService.GetAllAsync().Result.FirstOrDefault();
            CompetenceRequest competenceRequest = new CompetenceRequest()
            {
                Name = competence.Name,
                GroupId = competence.GroupId
            };

            //Act
            var result = await _competenceController.CreateCompetence(competenceRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Test for creating a competency when specifying a group that is not in the database
        /// </summary>
        /// <returns>
        /// Returns BadRequest when specifying a group that does not exist when creating a new competency
        /// </returns>
        [Fact]
        public async Task AddCompetence_ReturnsBadRequest_ForMissingGroups()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            CompetenceRequest competenceRequest = new CompetenceRequest()
            {
                Name = name,
                GroupId = -1
            };

            //Act
            var result = await _competenceController.CreateCompetence(competenceRequest);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>();

        }

        /// <summary>
        /// Competency update test with correct data
        /// </summary>
        /// <returns>
        /// Returns the competency on successful creation
        /// </returns>
        [Fact]
        public async Task UpdateCompetence_ReturnsOk_ForSuccessfullyUpdate()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            CompetenceRequest competenceRequest = new CompetenceRequest()
            {
                Name = name,
                GroupId = 1
            };

            //Act
            var result = await _competenceController.UpdateCompetence(1, competenceRequest);

            //Assert
            result.Should().BeAssignableTo<OkObjectResult>().Which.Value.As<CompetenceResponse>().Name.Should().Be(name);
        }

        /// <summary>
        /// Competency update test when specifying an incorrect competency number
        /// </summary>
        /// <returns>
        /// Returns a BadRequest if a non-existent competency number is specified on update
        /// </returns>
        [Fact]
        public async Task UpdateCompetence_ReturnsBadRequest_ForNonExistingCompetence()
        {
            //Arrange
            CompetenceRequest competenceRequest = new CompetenceRequest()
            {
                Name = "Competence" + DateTime.Now.Ticks,
                GroupId = 1
            };

            //Act
            var result = await _competenceController.UpdateCompetence(-1, competenceRequest);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Competency update test when specifying the name of a competency that is already in the group
        /// </summary>
        /// <returns>
        /// Returns a BadRequest when given an existing name in a group when updating a competency
        /// </returns>
        [Fact]
        public async Task UpdateCompetence_ReturnsBadRequest_ForExistingCompetenceInGroup()
        {
            //Arrange
            var competence1 = _competenceService.GetAllAsync().Result.First(x => x.GroupId == 1);
            var competence2 = _competenceService.GetAllAsync().Result.First(x => x.GroupId == 1 && x.Id != competence1.Id);

            CompetenceRequest competenceRequest = new CompetenceRequest()
            {
                Name = competence2.Name,
                GroupId = 1
            };

            //Act
            var result = await _competenceController.UpdateCompetence(competence1.Id, competenceRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Competency update test when specifying a group that is not in the database
        /// </summary>
        /// <returns>
        /// Returns a BadRequest if a non-existent group is specified when updating a competency
        /// </returns>
        [Fact]
        public async Task UpdateCompetence_ReturnsBadRequest_ForNonExisingGroup()
        {
            //Arrange
            CompetenceRequest competenceRequest = new CompetenceRequest()
            {
                Name = "Competence" + DateTime.Now.Ticks,
                GroupId = -1
            };

            //Act
            var result = await _competenceController.UpdateCompetence(1, competenceRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Checking if a competency is deleted when specifying its number
        /// </summary>
        /// <returns>
        /// Returns OkObjectResult if a valid number was specified when deleting a competency
        /// </returns>
        /// Doesn't work with InMemory context, comment it for a while
        //[Fact]
        //public async Task DeleteCompetence_ReturnsOk_ForSuccessfullyDelete()
        //{
        //    //Arrange
        //    string name = Guid.NewGuid().ToString();
        //    CompetenceDTO entity = new CompetenceDTO()
        //    {
        //        Name = name,
        //        GroupId = 1
        //    };
        //    var competence = await _competenceService.CreateAsync(entity);

        //    //Act
        //    var result = await _competenceController.DeleteCompetence(competence.Id);

        //    //Assert
        //    result.Should().BeOfType<OkResult>();
        //}

        /// <summary>
        /// Checking if a competency is deleted when specifying its number
        /// </summary>
        /// <returns>
        /// Returns OkObjectResult if a valid number was specified when deleting a competency
        /// </returns>
        [Fact]
        public async Task DeleteCompetence_ReturnsNotFound_ForMissingCompetence()
        {
            //Arrange
            int idCompetence = -1;

            //Act
            var result = await _competenceController.DeleteCompetence(idCompetence);

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }
    }
}
