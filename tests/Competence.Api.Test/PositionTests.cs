﻿using Competence.Api.Controllers;
using Competence.Api.Models;
using Competence.Domain.Models;
using Competence.Domain.Services.Abstract;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Competence.Api.Test
{
    public class PositionTests : IClassFixture<FixureInMemory>
    {
        private readonly PositionsController _positionsController;
        private readonly IPositionService _positionService;

        public PositionTests(FixureInMemory fixureInMemory)
        {
            _positionsController = fixureInMemory.ServiceProvider.GetService<PositionsController>();
            _positionService = fixureInMemory.ServiceProvider.GetService<IPositionService>();
        }

        /// <summary>
        /// Position request test with an ID that is not in the database.
        /// </summary>
        /// <returns>
        /// When executing the test, the request should return the value NotFound, which will mean the successful execution of the test
        /// </returns>
        [Fact]
        public async Task GetPosition_ReturnsOk_ForDesiredValue()
        {
            //Arrange
            var position = _positionService.GetAllAsync().Result.FirstOrDefault();

            //Act
            var result = await _positionsController.GetPositionById(position.Id);

            //Assert
            result.Should().BeOfType<OkObjectResult>();
        }

        /// <summary>
        /// Position request test with an ID that is in the database.
        /// </summary>
        /// <returns>
        /// When executing the test, the query should return the value OK, which means that the test was successful
        /// </returns>
        [Fact]
        public async Task GetPosition_ReturnsNotFound_ForMissingPosition()
        {
            //Arrange
            int idPosition = -1;

            //Act
            var result = await _positionsController.GetPositionById(idPosition);

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }

        /// <summary>
        /// Position creation Test
        /// </summary>
        /// <returns>
        /// Upon successful completion of the test, a Position is created in the database and information about it is returned.
        /// </returns>
        [Fact]
        public async Task AddPosition_ReturnsOk_ForSuccessfullyCreate()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            PositionRequest PositionRequest = new PositionRequest()
            {
                Name = name
            };

            //Act
            var result = await _positionsController.CreatePosition(PositionRequest);

            //Assert
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<PositionResponse>().Name.Should().Be(name);
        }

        /// <summary>
        ///  Test for the name of Position already existing in the specified database
        /// </summary>
        /// <returns>
        /// Returns BadRequest if an attempt is made to create a Position with the same name as the specified database.
        /// </returns>
        [Fact]
        public async Task AddPosition_ReturnsBadRequest_ForExistingPosition()
        {
            //Arrange
            var name =_positionService.GetAllAsync().Result.FirstOrDefault().Name;

            PositionRequest PositionRequest = new PositionRequest()
            {
                Name = name
            };

            //Act
            var result = await _positionsController.CreatePosition(PositionRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Position update test with correct data
        /// </summary>
        /// <returns>
        /// Returns the group competency on successful creation
        /// </returns>
        [Fact]
        public async Task UpdatePosition_ReturnsOk_ForSuccessfullyUpdate()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            PositionRequest PositionRequest = new PositionRequest()
            {
                Name = name
            };

            //Act
            var result = await _positionsController.UpdatePosition(1, PositionRequest);

            //Assert
            result.Should().BeAssignableTo<OkObjectResult>().Which.Value.As<PositionResponse>().Name.Should().Be(name);
        }

        /// <summary>
        /// Position update test when specifying an incorrect Position number
        /// </summary>
        /// <returns>
        /// Returns a BadRequest if a non-existent Position number is specified on update
        /// </returns>
        [Fact]
        public async Task UpdatePosition_ReturnsBadRequest_ForNotExistingPosition()
        {
            //Arrange
            PositionRequest PositionRequest = new PositionRequest()
            {
                Name = "Position_" + DateTime.Now.Ticks
            };

            //Act
            var result = await _positionsController.UpdatePosition(-1, PositionRequest);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Position update test when specifying the name of a Position that is already 
        /// </summary>
        /// <returns>
        /// Returns a BadRequest when given an existing name in a Position when updating a Position
        /// </returns>
        [Fact]
        public async Task UpdatePosition_ReturnsBadRequest_ForExistingName()
        {
            //Arrange
            var position1 = _positionService.GetAllAsync().Result.ElementAt(0);
            var position2 = _positionService.GetAllAsync().Result.ElementAt(1);

            PositionRequest PositionRequest = new PositionRequest()
            {
                Name = position2.Name
            };

            //Act
            var result = await _positionsController.UpdatePosition(position1.Id, PositionRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Checking if a Position is deleted when specifying its number
        /// </summary>
        /// <returns>
        /// Returns OkObjectResult if a valid number was specified when deleting a Position
        /// </returns>
        [Fact]
        public async Task DeletePosition_ReturnsOk_ForSuccessfullyDelete()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            PositionDTO entity = new PositionDTO()
            {
                Name = name
            };
            var position = await _positionService.CreateAsync(entity);

            //Act
            var result = await _positionsController.DeletePosition(position.Id);

            //Assert
            result.Should().BeOfType<OkResult>();
        }

        /// <summary>
        /// Checking if a Position is deleted when specifying its number
        /// </summary>
        /// <returns>
        /// Returns OkObjectResult if a valid number was specified when deleting a Position
        /// </returns>
        [Fact]
        public async Task DeletePosition_ReturnsNotFound_ForMissingPosition()
        {
            //Arrange
            int idPosition = -1;

            //Act
            var result = await _positionsController.DeletePosition(idPosition);

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }
    }
}
