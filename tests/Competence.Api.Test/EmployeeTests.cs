﻿using AutoMapper;
using Competence.Api.Controllers;
using Competence.Api.Models;
using Competence.Domain.Models;
using Competence.Domain.Services.Abstract;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Competence.Api.Test
{
    public class EmployeeTest : IClassFixture<FixureInMemory>
    {
        private readonly EmployeeController _employeeController;
        private readonly IEmployeeService _employeeService;
        private readonly IMapper _mapper;
        private readonly ICountryService _countryService;

        public EmployeeTest(FixureInMemory fixureInMemory)
        {
            _employeeController = fixureInMemory.ServiceProvider.GetService<EmployeeController>();
            _employeeService = fixureInMemory.ServiceProvider.GetService<IEmployeeService>();
            _mapper = fixureInMemory.ServiceProvider.GetService<IMapper>();
            _countryService = fixureInMemory.ServiceProvider.GetService<ICountryService>();
        }

        /// <summary>
        /// Employee request test with an ID that is not in the database.
        /// </summary>
        /// <returns>
        /// When executing the test, the request should return the value NotFound, which will mean the successful execution of the test
        /// </returns>
        [Fact]
        public async Task GetEmployee_ReturnsOk_ForDesiredValue()
        {
            //Arrange
            var employee = _employeeService.GetAllAsync().Result.FirstOrDefault();


            //Act
            var result = await _employeeController.GetEmployeeById(employee.Id);

            //Assert
            result.Should().BeOfType<OkObjectResult>();
        }

        /// <summary>
        /// Employee request test with an ID that is in the database.
        /// </summary>
        /// <returns>
        /// When executing the test, the query should return the value OK, which means that the test was successful
        /// </returns>
        [Fact]
        public async Task GetEmployee_ReturnsNotFound_ForMissingEmployee()
        {
            //Arrange
            int idEmployee = -1;

            //Act
            var result = await _employeeController.GetEmployeeById(idEmployee);

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }

        /// <summary>
        /// Employee creation Test
        /// </summary>
        /// <returns>
        /// Upon successful completion of the test, a Employee is created in the database and information about it is returned.
        /// </returns>
        [Fact]
        public async Task AddEmployee_ReturnsOk_ForSuccessfullyCreate()
        {
            //Arrange
            var firstName = "FirstName";
            var lastName = "LastName";

            EmployeeRequest employeeRequest = new EmployeeRequest()
            {
                FirstName = firstName,
                MiddleName = "",
                LastName = lastName,
                Birthday = DateTime.Now.AddYears(-20).Date,
                Sex = 1,
                CountryId = 1,
                DepartmentId = 1,
                PositionId = 1,
                CompetenceIds = new System.Collections.Generic.List<int>()
            };

            //Act
            var result = await _employeeController.CreateEmployee(employeeRequest);

            //Assert
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().FirstName.Should().Be(employeeRequest.FirstName);
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().MiddleName.Should().Be(employeeRequest.MiddleName);
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().LastName.Should().Be(employeeRequest.LastName);
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().Birthday.Should().Be(employeeRequest.Birthday);
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().Country.Id.Should().Be(employeeRequest.CountryId);
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().Position.Id.Should().Be(employeeRequest.PositionId);
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().Competencies.Should().BeNullOrEmpty();
        }

        /// <summary>
        /// Employee update test with correct data
        /// </summary>
        /// <returns>
        /// Returns the group competency on successful creation
        /// </returns>
        /// Doesn't work with InMemory context
        //[Fact]
        //public async Task UpdateEmployee_ReturnsOk_ForSuccessfullyUpdate()
        //{
        //    //Arrange
        //    var firstName = "FirstName";
        //    var lastName = "LastName";

        //    EmployeeRequest employeeRequest = new EmployeeRequest()
        //    {
        //        FirstName = firstName,
        //        MiddleName = "",
        //        LastName = lastName,
        //        Birthday = DateTime.Now.AddYears(-20).Date,
        //        Sex = 1,
        //        CountryId = 1,
        //        DepartmentId = 1,
        //        PositionId = 1,
        //        CompetenceIds = new System.Collections.Generic.List<int>()
        //    };

        //    //Act
        //    var result = await _employeeController.UpdateEmployee(1, employeeRequest);

        //    //Assert
        //    result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().FirstName.Should().Be(employeeRequest.FirstName);
        //    result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().MiddleName.Should().Be(employeeRequest.MiddleName);
        //    result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().LastName.Should().Be(employeeRequest.LastName);
        //    result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().Birthday.Should().Be(employeeRequest.Birthday);
        //    result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().Country.Id.Should().Be(employeeRequest.CountryId);
        //    result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().Position.Id.Should().Be(employeeRequest.PositionId);
        //    result.Should().BeOfType<OkObjectResult>().Which.Value.As<EmployeeResponse>().Competencies.Should().BeNullOrEmpty();
        //}

        /// <summary>
        /// Employee update test when specifying an incorrect Employee number
        /// </summary>
        /// <returns>
        /// Returns a BadRequest if a non-existent Employee number is specified on update
        /// </returns>
        [Fact]
        public async Task UpdateEmployee_ReturnsBadRequest_ForNonExistingEmployee()
        {
            //Arrange
            EmployeeRequest EmployeeRequest = new EmployeeRequest()
            {
                FirstName = "Vitaly",
                MiddleName = "",
                LastName = "Petrov",
                Birthday = DateTime.Now.AddYears(-10),
                Sex = 1,
                CountryId = 1,
                DepartmentId = 1
            };

            //Act
            var result = await _employeeController.UpdateEmployee(-1, EmployeeRequest);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }
              
        /// <summary>
        /// Checking if a Employee is deleted when specifying its number
        /// </summary>
        /// <returns>
        /// Returns OkObjectResult if a valid number was specified when deleting a Employee
        /// </returns>
        [Fact]
        public async Task DeleteEmployee_ReturnsOk_ForSuccessfullyDelete()
        {
            //Arrange
            EmployeeRequest entity = new EmployeeRequest()
            {
                FirstName = "FirstName",
                MiddleName = "",
                LastName = "LastName",
                Birthday = DateTime.Now.AddYears(-20).Date,
                Sex = 1,
                CountryId = 1,
                DepartmentId = 1,
                PositionId = 1,
                CompetenceIds = new System.Collections.Generic.List<int>()
            };
            var employee = await _employeeService.CreateAsync(_mapper.Map<EmployeeRequest, EmployeeDTO>(entity));

            //Act
            var result = await _employeeController.DeleteEmployee(employee.Id);

            //Assert
            result.Should().BeOfType<OkResult>();
        }

        /// <summary>
        /// Checking if a Employee is deleted when specifying its number
        /// </summary>
        /// <returns>
        /// Returns OkObjectResult if a valid number was specified when deleting a Employee
        /// </returns>
        [Fact]
        public async Task DeleteEmployee_ReturnsNotFound_ForMissingEmployee()
        {
            //Arrange
            int idEmployee = -1;

            //Act
            var result = await _employeeController.DeleteEmployee(idEmployee);

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }
    }
}
