﻿using Competence.Api.Controllers;
using Competence.Api.Models;
using Competence.Domain.Models;
using Competence.Domain.Services.Abstract;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Competence.Api.Test
{
    public class CountryTests : IClassFixture<FixureInMemory>
    {
        private readonly CountriesController _countriesController;
        private readonly ICountryService _countryService;

        public CountryTests(FixureInMemory fixureInMemory)
        {
            _countriesController = fixureInMemory.ServiceProvider.GetService<CountriesController>();
            _countryService = fixureInMemory.ServiceProvider.GetService<ICountryService>();
        }

        /// <summary>
        /// Country request test with an ID that is not in the database.
        /// </summary>
        /// <returns>
        /// When executing the test, the request should return the value NotFound, which will mean the successful execution of the test
        /// </returns>
        [Fact]
        public async Task GetCountry_ReturnsOk_ForDesiredValue()
        {
            //Arrange
            var country = _countryService.GetAllAsync().Result.FirstOrDefault();

            //Act
            var result = await _countriesController.GetCountryById(country.Id);

            //Assert
            result.Should().BeOfType<OkObjectResult>();
        }

        /// <summary>
        /// Country request test with an ID that is in the database.
        /// </summary>
        /// <returns>
        /// When executing the test, the query should return the value OK, which means that the test was successful
        /// </returns>
        [Fact]
        public async Task GetCountry_ReturnsNotFound_ForMissingCountry()
        {
            //Arrange
            int idCountry = -1;

            //Act
            var result = await _countriesController.GetCountryById(idCountry);

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }

        /// <summary>
        /// Country creation Test
        /// </summary>
        /// <returns>
        /// Upon successful completion of the test, a Country is created in the database and information about it is returned.
        /// </returns>
        [Fact]
        public async Task AddCountry_ReturnsOk_ForSuccessfullyCreate()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            CountryRequest CountryRequest = new CountryRequest()
            {
                Code = "00",
                Name = name
            };

            //Act
            var result = await _countriesController.CreateCountry(CountryRequest);

            //Assert
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<CountryResponse>().Name.Should().Be(name);
        }

        /// <summary>
        ///  Test for the name of Country already existing in the specified database
        /// </summary>
        /// <returns>
        /// Returns BadRequest if an attempt is made to create a Country with the same name as the specified database.
        /// </returns>
        [Fact]
        public async Task AddCountry_ReturnsBadRequest_ForExistingCountry()
        {
            //Arrange
            var name = _countryService.GetAllAsync().Result.FirstOrDefault().Name;
            CountryRequest CountryRequest = new CountryRequest()
            {
                Name = name
            };

            //Act
            var result = await _countriesController.CreateCountry(CountryRequest);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }

        /// <summary>
        /// Country update test with correct data
        /// </summary>
        /// <returns>
        /// Returns the group competency on successful creation
        /// </returns>
        [Fact]
        public async Task UpdateCountry_ReturnsOk_ForSuccessfullyUpdate()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            CountryRequest CountryRequest = new CountryRequest()
            {
                Code = "00",
                Name = name
            };

            //Act
            var result = await _countriesController.UpdateCountry(1, CountryRequest);

            //Assert
            result.Should().BeAssignableTo<OkObjectResult>().Which.Value.As<CountryResponse>().Name.Should().Be(name);
        }

        /// <summary>
        /// Country update test when specifying an incorrect Country number
        /// </summary>
        /// <returns>
        /// Returns a BadRequest if a non-existent Country number is specified on update
        /// </returns>
        [Fact]
        public async Task UpdateCountry_ReturnsBadRequest_ForNonExistingCountry()
        {
            //Arrange
            CountryRequest CountryRequest = new CountryRequest()
            {
                Code = "00",
                Name = "Country_" + DateTime.Now.Ticks
            };

            //Act
            var result = await _countriesController.UpdateCountry(-1, CountryRequest);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Country update test when specifying the name of a Country that is already 
        /// </summary>
        /// <returns>
        /// Returns a BadRequest when given an existing name in a Country when updating a Country
        /// </returns>
        [Fact]
        public async Task UpdateCountry_ReturnsBadRequest_ForExistingName()
        {
            //Arrange
            var country1 = _countryService.GetAllAsync().Result.ElementAt(0);
            var country2 = _countryService.GetAllAsync().Result.ElementAt(1);

            CountryRequest CountryRequest = new CountryRequest()
            {
                Code = "00",
                Name = country2.Name
            };

            //Act
            var result = await _countriesController.UpdateCountry(country1.Id, CountryRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Checking if a Country is deleted when specifying its number
        /// </summary>
        /// <returns>
        /// Returns OkObjectResult if a valid number was specified when deleting a Country
        /// </returns>
        [Fact]
        public async Task DeleteCountry_ReturnsOk_ForSuccessfullyDelete()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            CountryDTO entity = new CountryDTO()
            {
                Code = "00",
                Name = name
            };
            var country = await _countryService.CreateAsync(entity);

            //Act
            var result = await _countriesController.DeleteCountry(country.Id);

            //Assert
            result.Should().BeOfType<OkResult>();
        }

        /// <summary>
        /// Checking if a Country is deleted when specifying its number
        /// </summary>
        /// <returns>
        /// Returns OkObjectResult if a valid number was specified when deleting a Country
        /// </returns>
        [Fact]
        public async Task DeleteCountry_ReturnsNotFound_ForMissingCountry()
        {
            //Arrange
            int idCountry = -1;

            //Act
            var result = await _countriesController.DeleteCountry(idCountry);

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }
    }
}
