﻿using Competence.Api.Controllers;
using Competence.Api.Models;
using Competence.Domain.Models;
using Competence.Domain.Services.Abstract;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Competence.Api.Test
{
    /// <summary>
    /// Competence group test class
    /// </summary>
    public class CompetenceGroupTests : IClassFixture<FixureInMemory>
    {
        private readonly CompetenceGroupsController _competenceGroupsController;
        private readonly ICompetenceGroupService _competenceGroupService;

        public CompetenceGroupTests(FixureInMemory fixureInMemory)
        {
            _competenceGroupsController = fixureInMemory.ServiceProvider.GetService<CompetenceGroupsController>();
            _competenceGroupService = fixureInMemory.ServiceProvider.GetService<ICompetenceGroupService>();
        }

        /// <summary>
        /// Group competency request test with an ID that is not in the database.
        /// </summary>
        /// <returns>
        /// When executing the test, the request should return the value NotFound, which will mean the successful execution of the test
        /// </returns>
        [Fact]
        public async Task GetCompetenceGroup_ReturnOk_ForDesiredValue()
        {
            //Arrange
            var group = _competenceGroupService.GetAllAsync().Result.FirstOrDefault();

            //Act
            var result = await _competenceGroupsController.GetCompetenceGroupById(group.Id);

            //Assert
            result.Should().BeOfType<OkObjectResult>();
        }

        /// <summary>
        /// Group competency request test with an ID that is in the database.
        /// </summary>
        /// <returns>
        /// When executing the test, the query should return the value OK, which means that the test was successful
        /// </returns>
        [Fact]
        public async Task GetCompetenceGroup_ReturnsNotFound_ForMissingGroup()
        {
            //Arrange
            int idGroup = -1;

            //Act
            var result = await _competenceGroupsController.GetCompetenceGroupById(idGroup);

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }

        /// <summary>
        /// Group competency creation Test
        /// </summary>
        /// <returns>
        /// Upon successful completion of the test, a group competency is created in the database and information about it is returned.
        /// </returns>
        [Fact]
        public async Task AddCompetenceGroup_ReturnsOk_ForSuccessfullyCreate()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            CompetenceGroupRequest competenceRequest = new CompetenceGroupRequest()
            {
                Name = name
            };

            //Act
            var result = await _competenceGroupsController.CreateCompetenceGroup(competenceRequest);

            //Assert
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<CompetenceGroupResponse>().Name.Should().Be(name);
        }

        /// <summary>
        ///  Test for the name of group competence already existing in the specified database
        /// </summary>
        /// <returns>
        /// Returns BadRequest if an attempt is made to create a group competence with the same name as the specified database.
        /// </returns>
        [Fact]
        public async Task AddCompetenceGroup_ReturnsBadRequest_ForExistingGroup()
        {
            //Arrange
            var name = _competenceGroupService.GetAllAsync().Result.FirstOrDefault().Name;
            CompetenceGroupRequest competenceRequest = new CompetenceGroupRequest()
            {
                Name = name
            };

            //Act
            var result = await _competenceGroupsController.CreateCompetenceGroup(competenceRequest);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }

        /// <summary>
        /// Group competency update test with correct data
        /// </summary>
        /// <returns>
        /// Returns the group competency on successful creation
        /// </returns>
        [Fact]
        public async Task UpdateCompetenceGroup_ReturnOk_ForSuccessfullyUpdate()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            CompetenceGroupRequest competenceGroupRequest = new CompetenceGroupRequest()
            {
                Name = name
            };

            //Act
            var result = await _competenceGroupsController.UpdateCompetenceGroup(1, competenceGroupRequest);

            //Assert
            result.Should().BeAssignableTo<OkObjectResult>().Which.Value.As<CompetenceGroupResponse>().Name.Should().Be(name);
        }

        /// <summary>
        /// Group competency update test when specifying an incorrect competency number
        /// </summary>
        /// <returns>
        /// Returns a BadRequest if a non-existent group competency number is specified on update
        /// </returns>
        [Fact]
        public async Task UpdateCompetenceGroup_ReturnsBadRequest_ForNonExistingGroup()
        {
            //Arrange
            CompetenceGroupRequest competenceGroupRequest = new CompetenceGroupRequest()
            {
                Name = "Group_" + DateTime.Now.Ticks
            };

            //Act
            var result = await _competenceGroupsController.UpdateCompetenceGroup(-1, competenceGroupRequest);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Group competence update test when specifying the name of a competency that is already in the group
        /// </summary>
        /// <returns>
        /// Returns a BadRequest when given an existing name in a group when updating a group competency
        /// </returns>
        [Fact]
        public async Task UpdateCompetenceGroup_ReturnsBadRequest_ForExistingName()
        {
            //Arrange
            var group1 = _competenceGroupService.GetAllAsync().Result.ElementAt(0);
            var group2 = _competenceGroupService.GetAllAsync().Result.ElementAt(1);

            CompetenceGroupRequest competenceGroupRequest = new CompetenceGroupRequest()
            {
                Name = group2.Name
            };

            //Act
            var result = await _competenceGroupsController.UpdateCompetenceGroup(group1.Id, competenceGroupRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Checking if a group competency is deleted when specifying its number
        /// </summary>
        /// <returns>
        /// Returns OkObjectResult if a valid number was specified when deleting a group competency
        /// </returns>
        [Fact]
        public async Task DeleteCompetenceGroup_ReturnsOk_ForSuccessfullyDelete()
        {
            //Arrange
            string name = Guid.NewGuid().ToString();
            CompetenceGroupDTO entity = new CompetenceGroupDTO()
            {
                Name = name
            };
            var group = await _competenceGroupService.CreateAsync(entity);

            //Act
            var result = await _competenceGroupsController.DeleteCompetenceGroup(group.Id);

            //Assert
            result.Should().BeOfType<OkResult>();
        }

        /// <summary>
        /// Checking if a group competency is deleted when specifying its number
        /// </summary>
        /// <returns>
        /// Returns OkObjectResult if a valid number was specified when deleting a group competency
        /// </returns>
        [Fact]
        public async Task DeleteCompetenceGroup_ReturnsNotFound_ForMissingGroup()
        {
            //Arrange
            int idCompetence = -1;

            //Act
            var result = await _competenceGroupsController.DeleteCompetenceGroup(idCompetence);

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }
    }
}
