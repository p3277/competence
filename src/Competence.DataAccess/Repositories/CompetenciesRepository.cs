﻿using AutoMapper;
using Competence.DataAccess.Data;
using Competence.Domain.Models;

namespace Competence.DataAccess.Repositories
{
    public class CompetenciesRepository : BaseRepository<CompetenceDTO, Models.Competence>
    {
        public CompetenciesRepository(DataContext dataContext
            , IMapper mapper)
            : base(dataContext, mapper)
        {

        }
    }
}
