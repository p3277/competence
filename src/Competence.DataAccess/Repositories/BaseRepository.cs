﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Competence.DataAccess.Data;
using Competence.DataAccess.Models.Abstract;
using Competence.Domain.Models.Abstract;
using Competence.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Competence.DataAccess.Repositories
{
    public class BaseRepository<T, E> : IRepository<T> where T: EntityDTO where E : Entity
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        public BaseRepository(DataContext dataContext
            , IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dataContext.Set<E>().ProjectTo<T>(_mapper.ConfigurationProvider).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetMany(Expression<Func<T, bool>> where)
        {
            return await _dataContext.Set<E>().ProjectTo<T>(_mapper.ConfigurationProvider).Where(where).ToListAsync();
        }


        public async Task<T> GetByIdAsync(int id)
        {
            return await _dataContext.Set<E>().ProjectTo<T>(_mapper.ConfigurationProvider).FirstOrDefaultAsync(x=> x.Id == id);
        }

        public T GetById(int id)
        {
            return _dataContext.Set<E>().ProjectTo<T>(_mapper.ConfigurationProvider).FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<T> GetByRange(IEnumerable<int> range)
        {
            return _dataContext.Set<E>().ProjectTo<T>(_mapper.ConfigurationProvider).Where(x => range.Contains(x.Id));
        }

        public async Task<IEnumerable<T>> GetByRangeAsync(IEnumerable<int> range)
        {
            return await _dataContext.Set<E>().ProjectTo<T>(_mapper.ConfigurationProvider).Where(x => range.Contains(x.Id)).ToListAsync();
        }

        public IEnumerable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return _dataContext.Set<E>().ProjectTo<T>(_mapper.ConfigurationProvider).Where(predicate).ToList();
        }

        public async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dataContext.Set<E>().ProjectTo<T>(_mapper.ConfigurationProvider).Where(predicate).ToListAsync();
        }

        public async Task<T> AddAsync(T dto)
        {
            var item = DtoToEntity(dto);
            await _dataContext.Set<E>().AddAsync(item);
            await _dataContext.SaveChangesAsync();
            return await GetByIdAsync(item.Id);
        }

        public async Task<T> UpdateAsync(T dto)
        {
            var item = DtoToEntity(dto);
            _dataContext.Entry(await _dataContext.Set<E>().FirstOrDefaultAsync(x => x.Id == dto.Id)).CurrentValues.SetValues(item);
            await _dataContext.SaveChangesAsync();
            return await GetByIdAsync(item.Id);
        }

        public async Task DeleteAsync(T dto)
        {
            _dataContext.Set<E>().Remove(await _dataContext.Set<E>().FirstOrDefaultAsync(x => x.Id == dto.Id));
            await _dataContext.SaveChangesAsync();
        }

        private E DtoToEntity(T dto)
        {
            return _mapper.Map<T, E>(dto);
        }
    }
}
