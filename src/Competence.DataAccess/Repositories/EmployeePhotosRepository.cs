﻿using AutoMapper;
using Competence.DataAccess.Data;
using Competence.DataAccess.Models;
using Competence.Domain.Models;

namespace Competence.DataAccess.Repositories
{
    public class EmployeePhotosRepository : BaseRepository<EmployeePhotoDTO, EmployeePhoto>
    {
        public EmployeePhotosRepository(DataContext dataContext
            , IMapper mapper)
            : base(dataContext, mapper)
        {

        }
    }
}
