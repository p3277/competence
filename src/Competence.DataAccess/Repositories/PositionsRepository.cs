﻿using AutoMapper;
using Competence.DataAccess.Data;
using Competence.DataAccess.Models;
using Competence.Domain.Models;

namespace Competence.DataAccess.Repositories
{
    public class PositionsRepository : BaseRepository<PositionDTO, Position>
    {
        public PositionsRepository(DataContext dataContext
            , IMapper mapper)
            : base(dataContext, mapper)
        {

        }
    }
}
