﻿using AutoMapper;
using Competence.DataAccess.Data;
using Competence.DataAccess.Models;
using Competence.Domain.Models;

namespace Competence.DataAccess.Repositories
{
    public class DepartmentsRepository : BaseRepository<DepartmentDTO, Department>
    {
        public DepartmentsRepository(DataContext dataContext
            , IMapper mapper)
            : base(dataContext, mapper)
        {

        }
    }
}
