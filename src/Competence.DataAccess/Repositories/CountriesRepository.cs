﻿using AutoMapper;
using Competence.DataAccess.Data;
using Competence.DataAccess.Models;
using Competence.Domain.Models;

namespace Competence.DataAccess.Repositories
{
    public class CountriesRepository : BaseRepository<CountryDTO, Country>
    {
        public CountriesRepository(DataContext dataContext
            , IMapper mapper)
            : base(dataContext, mapper)
        {

        }
    }
}
