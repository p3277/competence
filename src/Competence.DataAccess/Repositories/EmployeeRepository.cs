﻿using AutoMapper;
using Competence.DataAccess.Data;
using Competence.Domain.Models;

namespace Competence.DataAccess.Repositories
{
    public class EmployeeRepository : BaseRepository<EmployeeDTO, Models.Employee>
    {
        public EmployeeRepository(DataContext dataContext
            , IMapper mapper)
            : base(dataContext, mapper)
        {

        }
    }
}
