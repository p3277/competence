﻿using AutoMapper;
using Competence.DataAccess.Data;
using Competence.DataAccess.Models;
using Competence.Domain.Models;

namespace Competence.DataAccess.Repositories
{
    public class CompetenceGroupsRepository : BaseRepository<CompetenceGroupDTO, CompetenceGroup>
    {
        public CompetenceGroupsRepository(DataContext dataContext
            , IMapper mapper)
            : base(dataContext, mapper)
        {

        }
    }
}
