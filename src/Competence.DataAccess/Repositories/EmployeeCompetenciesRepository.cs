﻿using AutoMapper;
using Competence.DataAccess.Data;
using Competence.Domain.Models;

namespace Competence.DataAccess.Repositories
{
    public class EmployeeCompetenciesRepository : BaseRepository<EmployeeCompetenciesDTO, Models.EmployeeCompetencies>
    {
        public EmployeeCompetenciesRepository(DataContext dataContext
            , IMapper mapper)
            : base(dataContext, mapper)
        {

        }
    }
}
