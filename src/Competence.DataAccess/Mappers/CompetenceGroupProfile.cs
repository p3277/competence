﻿using AutoMapper;
using Competence.DataAccess.Models;
using Competence.Domain.Models;

namespace Competence.DataAccess.Mappers
{
    public class CompetenceGroupProfile : Profile
    {
        public CompetenceGroupProfile()
        {
            CreateMap<CompetenceGroupDTO, CompetenceGroup>();
            CreateMap<CompetenceGroup, CompetenceGroupDTO>();
        }
    }
}
