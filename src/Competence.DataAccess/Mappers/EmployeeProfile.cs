﻿using AutoMapper;
using Competence.Domain.Models;

namespace Competence.DataAccess.Mappers
{
    public class EmployeeProfile : Profile
    {
        public EmployeeProfile()
        {
            CreateMap<EmployeeDTO, Models.Employee>();
            CreateMap<Models.Employee, EmployeeDTO>();
        }
    }
}
