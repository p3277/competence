﻿using AutoMapper;
using Competence.DataAccess.Models;
using Competence.Domain.Models;

namespace Competence.DataAccess.Mappers
{
    public class DepartmentProfile : Profile
    {
        public DepartmentProfile()
        {
            CreateMap<DepartmentDTO, Department>();
            CreateMap<Department, DepartmentDTO>();
        }
    }
}
