﻿using AutoMapper;
using Competence.DataAccess.Models;
using Competence.Domain.Models;

namespace Competence.DataAccess.Mappers
{
    public class EmployeePhotoProfile : Profile
    {
        public EmployeePhotoProfile()
        {
            CreateMap<EmployeePhotoDTO, EmployeePhoto>();
            CreateMap<EmployeePhoto, EmployeePhotoDTO>();
        }
    }
}

