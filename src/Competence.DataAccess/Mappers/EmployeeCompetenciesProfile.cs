﻿using AutoMapper;
using Competence.Domain.Models;

namespace Competence.DataAccess.Mappers
{
    public class EmployeeCompetenciesProfile : Profile
    {
        public EmployeeCompetenciesProfile()
        {
            CreateMap<EmployeeCompetenciesDTO, Models.EmployeeCompetencies>();
            CreateMap<Models.EmployeeCompetencies, EmployeeCompetenciesDTO>();
        }
    }
}
