﻿using AutoMapper;
using Competence.Domain.Models;

namespace Competence.DataAccess.Mappers
{
    public class CompetenceProfile : Profile
    {
        public CompetenceProfile()
        {
            CreateMap<CompetenceDTO, Models.Competence>();
            CreateMap<Models.Competence, CompetenceDTO>();
        }
    }
}
