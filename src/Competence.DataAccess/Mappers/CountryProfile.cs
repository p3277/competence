﻿using AutoMapper;
using Competence.DataAccess.Models;
using Competence.Domain.Models;

namespace Competence.DataAccess.Mappers
{
    public class CountryProfile : Profile
    {
        public CountryProfile()
        {
            CreateMap<CountryDTO, Country>();
            CreateMap<Country, CountryDTO>();
        }
    }
}
