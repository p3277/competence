﻿using AutoMapper;
using Competence.DataAccess.Models;
using Competence.Domain.Models;

namespace Competence.DataAccess.Mappers
{
    public class PositionProfile : Profile
    {
        public PositionProfile()
        {
            CreateMap<PositionDTO, Position>();
            CreateMap<Position, PositionDTO>();
        }
    }
}
