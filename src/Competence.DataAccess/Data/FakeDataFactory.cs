using Competence.DataAccess.Models;
using System.Collections.Generic;

namespace Competence.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Country> GetCountries()
        {
            yield return new Country() { Id = 1, Name = "Belgium", Code = "BE" };
            yield return new Country() { Id = 2, Name = "Switzerland", Code = "CH" };
            yield return new Country() { Id = 3, Name = "Russia", Code = "RU" };
            yield return new Country() { Id = 4, Name = "Chile", Code = "CL" };
            yield return new Country() { Id = 5, Name = "USA", Code = "US" };
            yield return new Country() { Id = 6, Name = "Greece", Code = "GR" };
        }

        public static IEnumerable<Department> GetDepartments()
        {
            yield return new Department() { Id = 1, Name = "Legal" };
            yield return new Department() { Id = 2, Name = "Drivers" };
            yield return new Department() { Id = 3, Name = "Security" };
            yield return new Department() { Id = 4, Name = "Data Management" };
            yield return new Department() { Id = 5, Name = "Systems Development" };
            yield return new Department() { Id = 6, Name = "IT Infrastructure Services" };
        }

        public static IEnumerable<CompetenceGroup> GetCompetenceGroups()
        {
            yield return new CompetenceGroup() { Id = 1, Name = "Foreign languages" };
            yield return new CompetenceGroup() { Id = 2, Name = "Programming languages" };
            yield return new CompetenceGroup() { Id = 3, Name = "Other" };
        }

        public static IEnumerable<Models.Competence> GetCompetencies()
        {
            yield return new Models.Competence() { Id = 1, Name = "French", GroupId = 1 };
            yield return new Models.Competence() { Id = 2, Name = "English", GroupId = 1 };
            yield return new Models.Competence() { Id = 3, Name = "German", GroupId = 1 };
            yield return new Models.Competence() { Id = 4, Name = "Russian", GroupId = 1 };
            yield return new Models.Competence() { Id = 5, Name = "C#", GroupId = 2 };
            yield return new Models.Competence() { Id = 6, Name = "SQL", GroupId = 2 };
            yield return new Models.Competence() { Id = 7, Name = "JS", GroupId = 2 };
            yield return new Models.Competence() { Id = 8, Name = "Python", GroupId = 2 };
        }

        public static IEnumerable<EmployeePhoto> GetEmployeePhotos()
        {
            yield return new Models.EmployeePhoto() { Id = 1, EmployeeId = 1, Photo = Resources.EmployeeResources.employee5, Extension = "jpg" };
            yield return new Models.EmployeePhoto() { Id = 2, EmployeeId = 2, Photo = Resources.EmployeeResources.employee2, Extension = "jpg" };
            yield return new Models.EmployeePhoto() { Id = 3, EmployeeId = 3, Photo = Resources.EmployeeResources.employee3, Extension = "jpg" };
            yield return new Models.EmployeePhoto() { Id = 4, EmployeeId = 4, Photo = Resources.EmployeeResources.employee4, Extension = "jpg" };
        }

        public static IEnumerable<Employee> GetEmployee()
        {
            yield return new Employee()
            {
                Id = 1,
                FirstName = "Vitaly",
                MiddleName = "",
                LastName = "Petrov",
                Birthday = System.DateTime.Today.AddYears(-20),
                Sex = 1,
                CountryId = 1,
                DepartmentId = 5,
                PositionId = 1,
                Email = "vitaly.petrov@gmail.com"
            };
            yield return new Employee()
            {
                Id = 2,
                FirstName = "Roman",
                MiddleName = "",
                LastName = "Korsakov",
                Birthday = System.DateTime.Today.AddYears(-25),
                Sex = 2,
                CountryId = 2,
                DepartmentId = 2,
                PositionId = 2,
                Email = "roman.korsakov@gmail.com"
            };
            yield return new Employee()
            {
                Id = 3,
                FirstName = "Leonid",
                MiddleName = "",
                LastName = "Mets",
                Birthday = System.DateTime.Today.AddYears(-30),
                Sex = 1,
                CountryId = 3,
                DepartmentId = 3,
                PositionId = 3,
                Email = "leonid.mets@gmail.com"
            };
            yield return new Employee()
            {
                Id = 4,
                FirstName = "Vladislav",
                MiddleName = "",
                LastName = "Sekisov",
                Birthday = System.DateTime.Today.AddYears(-30),
                Sex = 1,
                CountryId = 3,
                DepartmentId = 1,
                PositionId = 2,
                Email = "vladislav.sekisov@gmail.com"
            };
        }

        public static IEnumerable<EmployeeCompetencies> GetEmployeeCompetencies()
        {
            yield return new EmployeeCompetencies() { EmployeeId = 1, CompetenceId = 2 };
            yield return new EmployeeCompetencies() { EmployeeId = 1, CompetenceId = 1 };
            yield return new EmployeeCompetencies() { EmployeeId = 1, CompetenceId = 5 };
            yield return new EmployeeCompetencies() { EmployeeId = 2, CompetenceId = 2 };
            yield return new EmployeeCompetencies() { EmployeeId = 2, CompetenceId = 6 };
            yield return new EmployeeCompetencies() { EmployeeId = 3, CompetenceId = 4 };
            yield return new EmployeeCompetencies() { EmployeeId = 3, CompetenceId = 3 };
            yield return new EmployeeCompetencies() { EmployeeId = 4, CompetenceId = 3 };
            yield return new EmployeeCompetencies() { EmployeeId = 4, CompetenceId = 8 };
            yield return new EmployeeCompetencies() { EmployeeId = 4, CompetenceId = 7 };
        }


        public static IEnumerable<Position> GetPositions()
        {
            yield return new Position() { Id = 1, Name = "Project Manager" };
            yield return new Position() { Id = 2, Name = "Programmer" };
            yield return new Position() { Id = 3, Name = "Systems Administrator" };
            yield return new Position() { Id = 4, Name = "Head Office" };
            yield return new Position() { Id = 5, Name = "Driver" };
        }

    }
}

