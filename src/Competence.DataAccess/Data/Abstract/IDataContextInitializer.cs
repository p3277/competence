﻿namespace Competence.DataAccess.Data
{
    public interface IDataContextInitializer
    {
        public void InitializeDb();
    }
}
