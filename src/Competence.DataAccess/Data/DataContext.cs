﻿using Competence.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Competence.DataAccess.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Department> Departments { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<CompetenceGroup> CompetenceGroups { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<EmployeeCompetencies> EmployeeCompetencies { get; set; }
        public DbSet<EmployeePhoto> EmployeePhotos { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Models.Competence>()
                .HasOne(p => p.Group)
                .WithMany(b => b.Competencies)
                .IsRequired()
                .Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            modelBuilder.Entity<EmployeeCompetencies>()
                .HasOne(bc => bc.Employee)
                .WithMany(b => b.EmployeeCompetencies)
                .HasForeignKey(bc => bc.EmployeeId);

            modelBuilder.Entity<EmployeeCompetencies>()
                .HasOne(bc => bc.Competence)
                .WithMany(b => b.EmployeeCompetencies)
                .HasForeignKey(bc => bc.CompetenceId);

        }

    }
}
