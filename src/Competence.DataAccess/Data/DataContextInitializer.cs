﻿namespace Competence.DataAccess.Data
{
    public class DataContextInitializer : IDataContextInitializer
    {
        private readonly DataContext _dataContext;

        public DataContextInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
            _dataContext.AddRange(FakeDataFactory.GetDepartments());
            _dataContext.AddRange(FakeDataFactory.GetCountries());
            _dataContext.AddRange(FakeDataFactory.GetCompetenceGroups());
            _dataContext.AddRange(FakeDataFactory.GetCompetencies());
            _dataContext.AddRange(FakeDataFactory.GetPositions());
            _dataContext.AddRange(FakeDataFactory.GetEmployee());
            _dataContext.AddRange(FakeDataFactory.GetEmployeeCompetencies());
            _dataContext.AddRange(FakeDataFactory.GetEmployeePhotos());

            _dataContext.SaveChanges();

        }
    }
}
