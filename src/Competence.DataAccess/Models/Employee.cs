﻿using System;
using System.Collections.Generic;
using Competence.DataAccess.Models.Abstract;

namespace Competence.DataAccess.Models
{
    public class Employee : NamedEntity
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int Sex { get; set; }
        public DateTime Birthday { get; set; }
        public string Email { get; set; }

        public int CountryId { get; set; }
        public virtual Country Country { get; set; }

        public int DepartmentId { get; set; }
        public virtual Department Department { get; set; }

        public int PositionId { get; set; }
        public virtual Position Position { get; set; }

        public virtual ICollection<EmployeeCompetencies> EmployeeCompetencies { get; set; }

    }
}
