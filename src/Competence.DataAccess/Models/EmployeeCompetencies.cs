﻿using Competence.DataAccess.Models.Abstract;

namespace Competence.DataAccess.Models
{
    public class EmployeeCompetencies : Entity
    {
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        public int CompetenceId { get; set; }
        public virtual Competence Competence { get; set; }
    }
}
