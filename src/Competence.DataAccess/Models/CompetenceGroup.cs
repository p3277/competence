﻿using Competence.DataAccess.Models.Abstract;
using System.Collections.Generic;

namespace Competence.DataAccess.Models
{
    public class CompetenceGroup : NamedEntity
    {
        public virtual ICollection<Competence> Competencies { get; set; }
    }
}
