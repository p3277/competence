﻿namespace Competence.DataAccess.Models.Abstract
{
    public class NamedEntity : Entity
    {
        public string Name { get; set; }
    }
}
