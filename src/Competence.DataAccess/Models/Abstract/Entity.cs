﻿namespace Competence.DataAccess.Models.Abstract
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}
