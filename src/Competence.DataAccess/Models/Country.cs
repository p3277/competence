﻿using Competence.DataAccess.Models.Abstract;

namespace Competence.DataAccess.Models
{
    public class Country : NamedEntity
    {
        public string Code { get; set; }
    }
}
