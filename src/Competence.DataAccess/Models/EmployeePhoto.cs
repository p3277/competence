﻿using Competence.DataAccess.Models.Abstract;

namespace Competence.DataAccess.Models
{
    public class EmployeePhoto : Entity
    {
        public byte[] Photo { get; set; }
        public int EmployeeId { get; set; }
        public string Extension { get; set; }
    }
}

