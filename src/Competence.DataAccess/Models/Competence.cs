﻿using Competence.DataAccess.Models.Abstract;
using System.Collections.Generic;

namespace Competence.DataAccess.Models
{
    public class Competence : NamedEntity
    {
        public int GroupId { get; set; }
        public virtual CompetenceGroup Group { get; set; }
        public virtual ICollection<EmployeeCompetencies> EmployeeCompetencies { get; set; }
    }
}
