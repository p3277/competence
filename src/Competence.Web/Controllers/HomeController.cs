﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using IdentityModel.Client;
using Competence.Web.Services;

namespace Competence.Web
{
    [Route("[controller]/[action]")]

    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ITokenService _tokenService;

        public HomeController(ILogger<HomeController> logger, ITokenService tokenService)
        {
            _logger = logger;
            _tokenService = tokenService;
        }

        [HttpGet]
        [ActionName("GetCompetencies")]
        [Authorize]
        public async Task<IActionResult> GetCompetencies()
        {
            var tokenResponse = await _tokenService.GetToken("competence.api.default");

            string resultData = "";
            using (var httpClient = new HttpClient())
            {
                httpClient.SetBearerToken(tokenResponse.AccessToken);
                var result = httpClient.GetAsync("https://localhost:44397/api/Competence/GetCompetencies").Result;
                if (result.IsSuccessStatusCode)
                {
                    resultData = result.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    throw new HttpRequestException("Unable to get data");
                }
            }
            return Ok(resultData);
        }

    }
}
