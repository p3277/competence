﻿using System.Threading.Tasks;
using IdentityModel.Client;

namespace Competence.Web.Services
{
    public interface ITokenService
    {
        Task<TokenResponse> GetToken(string scope);
    }
}
