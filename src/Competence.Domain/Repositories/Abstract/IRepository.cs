﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Competence.Domain.Models.Abstract;

namespace Competence.Domain.Repositories
{
    public interface IRepository<T> where T : EntityDTO
    {
        Task<T> GetByIdAsync(int id);
        T GetById(int id);
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> GetByRangeAsync(IEnumerable<int> range);
        IEnumerable<T> GetByRange(IEnumerable<int> range);
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> AddAsync(T entity);
        Task<T> UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        Task<IEnumerable<T>> GetMany(Expression<Func<T, bool>> where);
    }
}
