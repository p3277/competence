﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Resources;
using FluentValidation;
using System.Linq;

namespace Competence.Domain.Validators
{
    public class DepartmentValidator : AbstractValidator<DepartmentDTO>
    {
        public DepartmentValidator(IRepository<DepartmentDTO> repository
            , IRepository<EmployeeDTO> employeesRepository)
        {
            RuleFor(x => x.Name).NotEmpty()
                .WithMessage(string.Format(ValidationErrors.StringPropertyEmpty, "Department name"));
            RuleFor(x => repository.GetById(x.Id)).NotNull().When(x => x.Id != 0)
                .WithMessage(x => string.Format(ValidationErrors.PropertyByIdNotFound, "Department", x.Id));
            RuleFor(x => repository.Get(c => c.Name == x.Name)).Empty().When(x => x.Id == 0)
                .WithMessage(x => string.Format(ValidationErrors.RecordWithSpecifiedValueAlreadyExists, "Department name"));
            RuleFor(x => repository.Get(p => p.Name == x.Name && p.Id != x.Id)).Empty().When(x => x.Id != 0)
                .WithMessage(x => string.Format(ValidationErrors.RecordWithSpecifiedValueAlreadyExists, "Department name"));
            RuleSet("Delete", () =>
            {
                RuleFor(x => employeesRepository.Get(y => y.DepartmentId == x.Id).FirstOrDefault()).Null()
                    .WithMessage(x => string.Format(ValidationErrors.EntityUsedInOtherEntities, "Department", x.Id)); ;
            });
        }
    }
}
