﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Resources;
using FluentValidation;
using System.Linq;

namespace Competence.Domain.Validators
{
    public class CompetenceValidator : AbstractValidator<CompetenceDTO>
    {
        public CompetenceValidator(IRepository<CompetenceDTO> repository
            , IRepository<CompetenceGroupDTO> groupsRepository
            , IRepository<EmployeeCompetenciesDTO> employeeCompetenciesRepository)
        {
            RuleFor(x => x.Name).NotEmpty()
                .WithMessage(string.Format(ValidationErrors.StringPropertyEmpty, "Competence name"));
            RuleFor(x => groupsRepository.GetById(x.GroupId)).NotNull()
                .WithName("GroupId")
                .WithMessage(x => string.Format(ValidationErrors.PropertyByIdNotFound, "Competence group", x.GroupId));
            RuleFor(x => repository.GetById(x.Id)).NotNull().When(x => x.Id != 0)
                .WithMessage(x => string.Format(ValidationErrors.PropertyByIdNotFound, "Competence", x.Id));
            RuleFor(x => repository.Get(c => c.Name == x.Name && c.GroupId == x.GroupId)).Empty().When(x => x.Id == 0)
                .WithMessage(x => string.Format(ValidationErrors.RecordWithSpecifiedValueAlreadyExists, "Competence in the Group"));
            RuleFor(x => repository.Get(c => c.Name == x.Name && c.GroupId == x.GroupId && c.Id != x.Id)).Empty().When(x => x.Id != 0)
                .WithMessage(x => string.Format(ValidationErrors.RecordWithSpecifiedValueAlreadyExists, "Competence in the Group"));
            RuleSet("Delete", () =>
            {
                RuleFor(x => employeeCompetenciesRepository.Get(y => y.CompetenceId == x.Id).FirstOrDefault()).Null()
                    .WithMessage(x => string.Format(ValidationErrors.EntityUsedInOtherEntities, "Competence", x.Id)); ;
            });
        }
    }
}
