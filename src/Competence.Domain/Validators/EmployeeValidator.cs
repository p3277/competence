﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Resources;
using FluentValidation;
using System.Linq;

namespace Competence.Domain.Validators
{
    public class EmployeeValidator : AbstractValidator<EmployeeDTO>
    {
        public EmployeeValidator(IRepository<DepartmentDTO> departmentsRepository
            , IRepository<CountryDTO> countriesRepository
            , IRepository<PositionDTO> positionsRepository
            , IRepository<EmployeeDTO> employeesRepository)
        {
            RuleFor(x => x.FirstName).NotEmpty()
                .WithMessage(string.Format(ValidationErrors.StringPropertyEmpty, "First name"));
            RuleFor(x => x.LastName).NotEmpty()
                .WithMessage(string.Format(ValidationErrors.StringPropertyEmpty, "Last name"));
            RuleFor(x => countriesRepository.GetById(x.CountryId)).NotNull()
                .WithName("CountryId")
                .WithMessage(x => string.Format(ValidationErrors.PropertyByIdNotFound, "Country", x.CountryId));
            RuleFor(x => departmentsRepository.GetById(x.DepartmentId)).NotNull()
                .WithName("DepartmentId")
                .WithMessage(x => string.Format(ValidationErrors.PropertyByIdNotFound, "Department", x.DepartmentId));
            RuleFor(x => positionsRepository.GetById(x.PositionId)).NotNull()
                .WithName("PositionId")
                .WithMessage(x => string.Format(ValidationErrors.PropertyByIdNotFound, "Position", x.PositionId));
            RuleFor(x => employeesRepository.GetById(x.Id)).NotNull().When(x => x.Id != 0)
                .WithMessage(x => string.Format(ValidationErrors.PropertyByIdNotFound, "Employee", x.Id));
        }
    }
}
