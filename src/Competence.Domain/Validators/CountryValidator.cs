﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Resources;
using FluentValidation;
using System.Linq;

namespace Competence.Domain.Validators
{
    public class CountryValidator : AbstractValidator<CountryDTO>
    {
        public CountryValidator(IRepository<CountryDTO> repository
            , IRepository<EmployeeDTO> employeesRepository)
        {
            RuleFor(x => x.Name).NotEmpty()
                .WithMessage(string.Format(ValidationErrors.StringPropertyEmpty, "Country name"));
            RuleFor(x => x.Code).NotEmpty()
                .WithMessage(string.Format(ValidationErrors.StringPropertyEmpty, "Country code"));
            RuleFor(x => repository.GetById(x.Id)).NotNull().When(x => x.Id != 0)
                .WithMessage(x => string.Format(ValidationErrors.PropertyByIdNotFound, "Country", x.Id));
            RuleFor(x => repository.Get(c => c.Name == x.Name)).Empty().When(x => x.Id == 0)
                .WithMessage(x => string.Format(ValidationErrors.RecordWithSpecifiedValueAlreadyExists, "Country name"));
            RuleFor(x => repository.Get(c => c.Name == x.Name && c.Id != x.Id)).Empty().When(x => x.Id != 0)
                .WithMessage(x => string.Format(ValidationErrors.RecordWithSpecifiedValueAlreadyExists, "Country name"));
            RuleSet("Delete", () =>
            {
                RuleFor(x => employeesRepository.Get(y => y.CountryId == x.Id).FirstOrDefault()).Null()
                    .WithMessage(x => string.Format(ValidationErrors.EntityUsedInOtherEntities, "Country", x.Id)); ;
            });
        }
    }
}
