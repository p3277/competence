﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Resources;
using FluentValidation;
using System.Linq;

namespace Competence.Domain.Validators
{
    public class CompetenceGroupValidator : AbstractValidator<CompetenceGroupDTO>
    {
        public CompetenceGroupValidator(IRepository<CompetenceGroupDTO> repository
            , IRepository<CompetenceDTO> competenciesRepository)
        {
            RuleFor(x => x.Name).NotEmpty()
                .WithMessage(string.Format(ValidationErrors.StringPropertyEmpty, "Competence group name"));
            RuleFor(x => repository.GetById(x.Id)).NotNull().When(x => x.Id != 0)
                .WithMessage(x => string.Format(ValidationErrors.PropertyByIdNotFound, "Competence group", x.Id));
            RuleFor(x => repository.Get(c => c.Name == x.Name)).Empty().When(x => x.Id == 0)
                .WithMessage(x => string.Format(ValidationErrors.RecordWithSpecifiedValueAlreadyExists, "Competence group name"));
            RuleFor(x => repository.Get(p => p.Name == x.Name && p.Id != x.Id)).Empty().When(x => x.Id != 0)
                .WithMessage(x => string.Format(ValidationErrors.RecordWithSpecifiedValueAlreadyExists, "Competence group name"));
            RuleSet("Delete", () =>
            {
                RuleFor(x => competenciesRepository.Get(y => y.GroupId == x.Id).FirstOrDefault()).Null()
                    .WithMessage(x => string.Format(ValidationErrors.EntityUsedInOtherEntities, "Competence group", x.Id)); ;
            });
        }
    }
}
