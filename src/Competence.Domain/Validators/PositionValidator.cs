﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Resources;
using FluentValidation;
using System.Linq;

namespace Competence.Domain.Validators
{
    public class PositionValidator : AbstractValidator<PositionDTO>
    {
        public PositionValidator(IRepository<PositionDTO> repository
            , IRepository<EmployeeDTO> employeesRepository)
        {
            RuleFor(x => x.Name).NotEmpty()
                .WithMessage(string.Format(ValidationErrors.StringPropertyEmpty, "Position name"));
            RuleFor(x => repository.GetById(x.Id)).NotNull().When(x => x.Id != 0)
                .WithMessage(x => string.Format(ValidationErrors.PropertyByIdNotFound, "Position", x.Id));
            RuleFor(x => repository.Get(p => p.Name == x.Name)).Empty().When(x => x.Id == 0)
                .WithMessage(x => string.Format(ValidationErrors.RecordWithSpecifiedValueAlreadyExists, "Position name"));
            RuleFor(x => repository.Get(p => p.Name == x.Name && p.Id != x.Id)).Empty().When(x => x.Id != 0)
                .WithMessage(x => string.Format(ValidationErrors.RecordWithSpecifiedValueAlreadyExists, "Position name"));
            RuleSet("Delete", () =>
            {
                RuleFor(x => employeesRepository.Get(y => y.PositionId == x.Id).FirstOrDefault()).Null()
                    .WithMessage(x => string.Format(ValidationErrors.EntityUsedInOtherEntities, "Position", x.Id)); 
            });
        }
    }
}
