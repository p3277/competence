﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Services.Abstract;
using FluentValidation;
using Microsoft.Extensions.Logging;

namespace Competence.Domain.Services
{
    public class CompetenceGroupService : BaseCrudService<CompetenceGroupDTO>, ICompetenceGroupService
    {
        public CompetenceGroupService(IRepository<CompetenceGroupDTO> competenceGroupsRepository
            , ILogger<CompetenceGroupService> logger
            , IValidator<CompetenceGroupDTO> validator)
            : base(competenceGroupsRepository, logger, validator)
        {

        }
    }
}
