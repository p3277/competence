﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Services.Abstract;
using FluentValidation;
using Microsoft.Extensions.Logging;

namespace Competence.Domain.Services
{
    public class CountryService : BaseCrudService<CountryDTO>, ICountryService
    {
        public CountryService(IRepository<CountryDTO> countriesRepository
            , ILogger<CountryService> logger
            , IValidator<CountryDTO> validator)
            : base(countriesRepository, logger, validator)
        {
        }
    }
}
