﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Services.Abstract;
using FluentValidation;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competence.Domain.Services
{
    public class EmployeeCompetenceService : BaseCrudService<EmployeeCompetenciesDTO>, IEmployeeCompetenceService
    {
        public EmployeeCompetenceService(IRepository<EmployeeCompetenciesDTO> repository
            , ILogger<EmployeeCompetenceService> logger)
            : base(repository, logger)
        {
        
        }

        public override async Task<EmployeeCompetenciesDTO> CreateAsync(EmployeeCompetenciesDTO item)
        {
            return await base.CreateAsync(item);
        }

        public async Task DeleteAsync(int employeeId)
        {
            foreach (var item in _repository.Get(x => x.EmployeeId == employeeId))
                await base.DeleteAsync(item);
        }

        public async Task CreateAsync(IEnumerable<EmployeeCompetenciesDTO> items)
        {
            foreach (var item in items)
                await CreateAsync(item);
        }


    }
}
