﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Services.Abstract;
using FluentValidation;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Competence.Domain.Services
{
    public class DepartmentService : BaseCrudService<DepartmentDTO>, IDepartmentService
    {
        public DepartmentService(IRepository<DepartmentDTO> departmentRepository
            , IValidator<DepartmentDTO> validator
            , ILogger<DepartmentService> logger)
            : base(departmentRepository, logger, validator)
        {
        }

    }
}
