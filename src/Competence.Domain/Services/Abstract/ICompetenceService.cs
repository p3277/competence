﻿using Competence.Domain.Models;

namespace Competence.Domain.Services.Abstract
{
    public interface ICompetenceService : IBaseCrudService<CompetenceDTO>
    {

    }

}
