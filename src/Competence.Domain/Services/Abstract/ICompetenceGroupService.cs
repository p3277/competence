﻿using Competence.Domain.Models;

namespace Competence.Domain.Services.Abstract
{
    public interface ICompetenceGroupService : IBaseCrudService<CompetenceGroupDTO>
    {

    }

}
