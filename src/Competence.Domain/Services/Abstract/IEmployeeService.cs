﻿using Competence.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Competence.Domain.Services.Abstract
{
    public interface IEmployeeService : IBaseCrudService<EmployeeDTO>
    {
        Task<EmployeeDTO> CreateAsync(EmployeeDTO item, IEnumerable<int> competencies);
        Task<EmployeeDTO> UpdateAsync(EmployeeDTO item, IEnumerable<int> competencies);
        Task<string> GetPhotoAsync(int employeeId);
        Task UpdatePhotoAsync(int employeeId, string photo);

    }
}
