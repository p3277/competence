﻿using Competence.Domain.Models.Abstract;
using Competence.Domain.Repositories;
using FluentValidation;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Competence.Domain.Services.Abstract
{
    public abstract class BaseCrudService<T> : IBaseCrudService<T> where T: EntityDTO
    {
        protected readonly IRepository<T> _repository;
        protected readonly IValidator<T> _validator;
        protected readonly ILogger _logger;
        public BaseCrudService(IRepository<T> repository
            , ILogger logger
            , IValidator<T> validator = null)
        {
            _repository = repository;
            _validator = validator;
            _logger = logger;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _repository.GetAllAsync();
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await _repository.GetByIdAsync(id);
        }

        public virtual async Task<T> CreateAsync(T item)
        {
            if (_validator != null)
                _validator.ValidateAndThrow(item);
            var newItem = await _repository.AddAsync(item);
            _logger.LogInformation("New item created: {@T}", newItem);
            return newItem;
        }

        public virtual async Task<T> UpdateAsync(T item)
        {
            if (_validator != null)
                _validator.ValidateAndThrow(item);
            var oldItem = _repository.GetById(item.Id);
            var updatedItem =  await _repository.UpdateAsync(item);
            var logInfo = new { OldValue = oldItem, NewValue = updatedItem };
            _logger.LogInformation("Item updated. Old value: {@LogInfo} ", logInfo);
            return updatedItem;
        }

        public virtual async Task DeleteAsync(T item)
        {
            if (_validator != null)
                _validator.Validate(item, options =>
                {
                    options.ThrowOnFailures();
                    options.IncludeRuleSets("Delete");
                });

            await _repository.DeleteAsync(item);
            _logger.LogInformation("Item deleted. {@T} ", item);
        }
    }
}
