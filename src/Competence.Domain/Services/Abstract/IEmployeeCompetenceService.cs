﻿using Competence.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Competence.Domain.Services.Abstract
{
    public interface IEmployeeCompetenceService : IBaseCrudService<EmployeeCompetenciesDTO>
    {
        Task CreateAsync(IEnumerable<EmployeeCompetenciesDTO> items);
        Task DeleteAsync(int employeeId);
    }

}
