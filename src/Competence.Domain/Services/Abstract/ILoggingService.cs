﻿using Competence.Domain.Models;
using System;

namespace Competence.Domain.Services.Abstract
{
    public interface ILoggingService
    {

        public void AddError(string message, string username, string appName, Exception exception = null);
        public void AddWarning(string message, string username, string appName, Exception exception = null);
        public void AddInformation(string message, string username, string appName, Exception exception = null);
        public void AddDebug(string message, string username, string appName, Exception exception = null);
        public void AddFatal(string message, string username, string appName, Exception exception = null);

    }

}
