﻿using Competence.Domain.Models;

namespace Competence.Domain.Services.Abstract
{
    public interface ICountryService : IBaseCrudService<CountryDTO>
    {

    }

}
