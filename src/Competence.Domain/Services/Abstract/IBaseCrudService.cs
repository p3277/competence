﻿using Competence.Domain.Models.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Competence.Domain.Services.Abstract
{
    public interface IBaseCrudService<T> where T : EntityDTO
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(int id);
        Task<T> CreateAsync(T item);
        Task<T> UpdateAsync(T item);
        Task DeleteAsync(T item);
    }

}
