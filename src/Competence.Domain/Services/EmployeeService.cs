﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Services.Abstract;
using FluentValidation;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competence.Domain.Services
{
    public class EmployeeService : BaseCrudService<EmployeeDTO>, IEmployeeService
    {
        private readonly IEmployeeCompetenceService _employeeCompetenceService;
        private readonly IRepository<CompetenceDTO> _competenciesRepository;
        private readonly IRepository<EmployeePhotoDTO> _employeePhotosRepository;
        public EmployeeService(IRepository<EmployeeDTO> employeeRepository
            , IRepository<CompetenceDTO> competenciesRepository
            , IRepository<EmployeePhotoDTO> employeePhotosRepository
            , IEmployeeCompetenceService employeeCompetenceService
            , ILogger<EmployeeService> logger
            , IValidator<EmployeeDTO> validator)
            : base(employeeRepository, logger, validator)
        {
            _employeeCompetenceService = employeeCompetenceService;
            _competenciesRepository = competenciesRepository;
            _employeePhotosRepository = employeePhotosRepository;
        }

        public async Task<string> GetPhotoAsync(int employeeId)
        {
            var photo = (await _employeePhotosRepository.GetMany(x => x.EmployeeId == employeeId)).FirstOrDefault();
            if (photo == null)
                return null;
            return $"data:image/png;base64,{Convert.ToBase64String(photo.Photo)}";
        }

        public async Task UpdatePhotoAsync(int employeeId, string photo)
        {
            var model = (await _employeePhotosRepository.GetMany(x => x.EmployeeId == employeeId)).FirstOrDefault();
            var base64Value = photo?.Split(",", StringSplitOptions.None)?.LastOrDefault()?.Trim();
            var ext = GetPhotoExtension(base64Value);
            var photoArray = string.IsNullOrEmpty(base64Value) ? null : Convert.FromBase64String(base64Value);

            if (model == null)
            {
                if (base64Value != null)
                    await _employeePhotosRepository.AddAsync(new EmployeePhotoDTO() { EmployeeId = employeeId, Photo = photoArray, Extension = ext });
            }
            else
            {
                if (base64Value == null)
                {
                    await _employeePhotosRepository.DeleteAsync(model);
                }
                else
                {
                    model.Photo = photoArray;
                    model.Extension = ext;
                    await _employeePhotosRepository.UpdateAsync(model);
                }
            }

        }

        private string GetPhotoExtension(string photo)
        {
            if ((photo ?? "").Length < 6)
                return null;

            switch (photo.Substring(0, 5).ToUpper())
            {
                case "IVBOR":
                    return "png";
                case "/9J/4":
                    return "jpg";
                case "AAAAF":
                    return "mp4";
                case "JVBER":
                    return "pdf";
                case "AAABA":
                    return "ico";
                case "UMFYI":
                    return "rar";
                case "E1XYD":
                    return "rtf";
                case "U1PKC":
                    return "txt";
                case "MQOWM":
                case "77U/M":
                    return "srt";
                default:
                    return null;
            }
        }

        public async Task<EmployeeDTO> CreateAsync(EmployeeDTO item, IEnumerable<int> competencies)
        {
            Validate(item, competencies);
            item.EmployeeCompetencies = new List<EmployeeCompetenciesDTO>();
            var employee = await _repository.AddAsync(item);
            await _employeeCompetenceService.CreateAsync(competencies.Select(x => new EmployeeCompetenciesDTO() { EmployeeId = employee.Id, CompetenceId = x }));
            var newItem = await GetByIdAsync(employee.Id);
            _logger.LogInformation("New item created: {@EmployeeDTO}", newItem);
            return newItem;
        }

        public async Task<EmployeeDTO> UpdateAsync(EmployeeDTO item, IEnumerable<int> competencies)
        {
            Validate(item, competencies);
            var oldItem = _repository.GetById(item.Id);
            await _employeeCompetenceService.DeleteAsync(item.Id);
            item.EmployeeCompetencies = new List<EmployeeCompetenciesDTO>();
            await _repository.UpdateAsync(item);
            await _employeeCompetenceService.CreateAsync(competencies.Select(x => new EmployeeCompetenciesDTO() { EmployeeId = item.Id, CompetenceId = x }));
            var updatedItem = await GetByIdAsync(item.Id);
            var logInfo = new { OldValue = oldItem, NewValue = updatedItem };
            _logger.LogInformation("Item updated. Old value: {@LogInfo} ", logInfo);
            return updatedItem;
        }

        private void Validate(EmployeeDTO employee, IEnumerable<int> competencies)
        {
            var result = _validator.Validate(employee);
            var nullCompetencies = competencies.Select(x => new { id = x, comp = _competenciesRepository.GetById(x) }).Where(x => x.comp == null)
                .Select(x => new FluentValidation.Results.ValidationFailure("Competence", string.Format(Resources.ValidationErrors.PropertyByIdNotFound, "Competence", x.id)));
            if (!result.IsValid || nullCompetencies.Any())
                throw new ValidationException(result.Errors.Union(nullCompetencies));
        }

    }
}
