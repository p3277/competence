﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Services.Abstract;
using FluentValidation;
using Microsoft.Extensions.Logging;

namespace Competence.Domain.Services
{
    public class CompetenceService : BaseCrudService<CompetenceDTO>, ICompetenceService
    {
        public CompetenceService(IRepository<CompetenceDTO> competenceRepository
            , ILogger<CompetenceService> logger
            , IValidator<CompetenceDTO> validator)
            : base(competenceRepository, logger, validator)
        {

        }
    }
}
