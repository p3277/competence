﻿using Competence.Domain.Models;
using Competence.Domain.Services.Abstract;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using System.Text;
using System.Text.Json;

namespace Competence.Domain.Services
{
    public class LoggingService : ILoggingService
    {

        private const string exchangeName = "exchange.loggers";
        private const string routingKey = "loggers_routing";
        private IConnection connection
        {
            get
            {
                ConnectionFactory factory = new ConnectionFactory
                {
                    UserName = "guest",
                    Password = "guest",
                    HostName = "localhost"
                };
                IConnection conn = factory.CreateConnection();
                return conn;
            }
        }
        private IModel channel => connection.CreateModel();

        public LoggingService()
        {

        }

        private void pushMessage(string message, string username, string appName, LogLevel logLevel, Exception exception = null)
        {
            try
            {
                channel.ExchangeDeclare(exchangeName, ExchangeType.Direct);

                Console.WriteLine(connection.ChannelMax);

                var messagePush = new LoggerMessageDTO()
                {
                    Content = new LoggerMessageDTO.ContentMessage()
                    {
                        Message = message,
                        Exception = exception
                    },
                    Username = username,
                    AppName = appName,
                    LogLevel = logLevel
                };
                var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(messagePush));

                channel.BasicPublish(exchange: exchangeName,
                    routingKey: routingKey,
                    basicProperties: null,
                    body: body);

                Console.WriteLine($"{exchangeName}");
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
        public void AddError(string message, string username, string appName, Exception exception = null)
        {
            pushMessage(message: message, username: username, appName: appName, logLevel: LogLevel.Error, exception: exception);
        }
        public void AddWarning(string message, string username, string appName, Exception exception = null)
        {
            pushMessage(message: message, username: username, appName: appName, logLevel: LogLevel.Warning, exception: exception);
        }
        public void AddInformation(string message, string username, string appName, Exception exception = null)
        {
            pushMessage(message: message, username: username, appName: appName, logLevel: LogLevel.Information, exception: exception);
        }
        public void AddDebug(string message, string username, string appName, Exception exception = null)
        {
            pushMessage(message: message, username: username, appName: appName, logLevel: LogLevel.Debug, exception: exception);
        }
        public void AddFatal(string message, string username, string appName, Exception exception = null)
        {
            pushMessage(message: message, username: username, appName: appName, logLevel: LogLevel.Critical, exception: exception);
        }

    }
}
