﻿using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Services.Abstract;
using FluentValidation;
using Microsoft.Extensions.Logging;

namespace Competence.Domain.Services
{
    public class PositionService : BaseCrudService<PositionDTO>, IPositionService
    {
        public PositionService(IRepository<PositionDTO> positionRepository
            , ILogger<PositionService> logger
            , IValidator<PositionDTO> validator)
            : base(positionRepository, logger, validator)
        {

        }
    }
}
