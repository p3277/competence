﻿using Competence.Domain.Models.Abstract;

namespace Competence.Domain.Models
{
    public class EmployeeCompetenciesDTO : EntityDTO
    {
        public int EmployeeId { get; set; }
        public EmployeeDTO Employee { get; set; }
        public int CompetenceId { get; set; }
        public CompetenceDTO Competence { get; set; }
    }
}
