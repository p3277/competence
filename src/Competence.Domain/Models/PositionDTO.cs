﻿using Competence.Domain.Models.Abstract;

namespace Competence.Domain.Models
{
    public class PositionDTO : EntityDTO
    {
        public string Name { get; set; }
    }
}
