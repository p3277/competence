﻿using Competence.Domain.Models.Abstract;

namespace Competence.Domain.Models
{
    public class CompetenceGroupDTO : EntityDTO
    {
        public string Name { get; set; }
    }
}
