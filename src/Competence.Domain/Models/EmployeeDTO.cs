﻿using Competence.Domain.Models.Abstract;
using System;
using System.Collections.Generic;

namespace Competence.Domain.Models
{
    public class EmployeeDTO : EntityDTO
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public int CountryId { get; set; }
        public CountryDTO Country { get; set; }
        public int DepartmentId { get; set; }
        public DepartmentDTO Department { get; set; }
        public int PositionId { get; set; }
        public PositionDTO Position { get; set; }
        public ICollection<EmployeeCompetenciesDTO> EmployeeCompetencies { get; set; }
    }
}
