﻿using Competence.Domain.Models.Abstract;

namespace Competence.Domain.Models
{
    public class DepartmentDTO : EntityDTO
    {
        public string Name { get; set; }
    }
}
