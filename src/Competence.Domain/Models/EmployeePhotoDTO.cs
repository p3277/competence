﻿using Competence.Domain.Models.Abstract;

namespace Competence.Domain.Models
{
    public class EmployeePhotoDTO : EntityDTO
    {
        public byte[] Photo { get; set; }
        public int EmployeeId { get; set; }
        public string Extension { get; set; }
    }
}

