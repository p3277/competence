﻿using Competence.Domain.Models.Abstract;

namespace Competence.Domain.Models
{
    public class CompetenceDTO : EntityDTO
    {
        public string Name { get; set; }
        public int GroupId { get; set; }
        public CompetenceGroupDTO Group { get; set; }
    }
}
