﻿namespace Competence.Domain.Models.Abstract
{
    public abstract class EntityDTO
    {
        public int Id { get; set; }
    }
}
