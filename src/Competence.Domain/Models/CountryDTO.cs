﻿using Competence.Domain.Models.Abstract;

namespace Competence.Domain.Models
{
    public class CountryDTO : EntityDTO
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
