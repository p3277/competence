﻿using Microsoft.Extensions.Logging;
using System;

namespace Competence.Domain.Models
{
    public class LoggerMessageDTO
    {
        public string AppName { get; set; }
        public string Username { get; set; }
        public LogLevel LogLevel { get; set; }
        public ContentMessage Content { get; set; }
        public class ContentMessage
        {
            public string Message { get; set; }
            public Exception Exception { get; set; }
        }
    }
}
