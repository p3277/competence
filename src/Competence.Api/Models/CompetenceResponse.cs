﻿namespace Competence.Api.Models
{
    public class CompetenceResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CompetenceGroupResponse Group { get; set; }
    }
}
