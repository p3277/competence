﻿namespace Competence.Api.Models
{
    public class CompetenceRequest
    {
        public string Name { get; set; }
        public int GroupId { get; set; }
    }
}
