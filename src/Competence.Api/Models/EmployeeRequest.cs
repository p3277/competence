﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competence.Api.Models
{
    public class EmployeeRequest
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }

        public int Sex { get; set; }
        public int CountryId { get; set; }
        public int DepartmentId { get; set; }
        public int PositionId { get; set; }
        public List<int> CompetenceIds { get; set; }

        public string Email { get; set; }
        public string Photo { get; set; }
    }
}
