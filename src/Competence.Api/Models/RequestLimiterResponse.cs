﻿using System;

namespace Competence.Api.Models
{
    public class RequestLimiterResponse
    {
        public DateTime RequestDate { get; set; }
        public string RequestPath { get; set; }
    }
}
