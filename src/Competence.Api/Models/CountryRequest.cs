﻿namespace Competence.Api.Models
{
    public class CountryRequest
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
