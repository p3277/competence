﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Competence.Api.Models
{
    public class EmployeeResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{LastName} {FirstName} {MiddleName}";
        public string Email { get; set; }
        public string Photo { get; set; }
        
        public DateTime Birthday { get; set; }

        public CountryResponse Country { get; set; }
        public DepartmentResponse Department { get; set; }
        public List<CompetenceResponse> Competencies { get; set; }
        public PositionResponse Position { get; set; }
    }
}
