﻿namespace Competence.Api.Models
{
    public class CompetenceGroupResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
