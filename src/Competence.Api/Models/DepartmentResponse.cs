﻿namespace Competence.Api.Models
{
    public class DepartmentResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
