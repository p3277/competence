﻿namespace Competence.Api.Models
{
    public class CompetenceGroupRequest
    {
        public string Name { get; set; }
    }
}
