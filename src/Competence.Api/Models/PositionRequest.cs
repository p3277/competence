﻿namespace Competence.Api.Models
{
    public class PositionRequest
    {
        public string Name { get; set; }
    }
}
