﻿namespace Competence.Api.Models
{
    public class PositionResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
