﻿namespace Competence.Api.Models
{
    public class CountryResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
