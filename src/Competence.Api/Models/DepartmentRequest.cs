﻿namespace Competence.Api.Models
{
    public class DepartmentRequest
    {
        public string Name { get; set; }
    }
}
