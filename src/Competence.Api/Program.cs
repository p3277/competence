using Competence.DataAccess.Data;
using Competence.Domain.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;
using System;
using System.Diagnostics;

namespace Competence.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            LoggingService _loggingService = new LoggingService();
            try
            {
                _loggingService.AddInformation("Start up application", "Application", "API");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                _loggingService.AddFatal("Application start-up failed", "Application", "API", ex);
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                    .UseSerilog()
                    .ConfigureWebHostDefaults(webBuilder =>
                    {
                        webBuilder.UseStartup<Startup>();
                        webBuilder.UseUrls("http://+:5000", "https://+:5001");
                    });
        }
    }
}
