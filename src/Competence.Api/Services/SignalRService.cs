﻿using Competence.Api.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace Competence.Api.Services
{
    public class SignalRService : ISignalRService
    {
        private readonly IHubContext<NotificationsHub> _hubContext;

        public SignalRService(IHubContext<NotificationsHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public async void Send(string method, object arg)
        {
            await _hubContext.Clients.All.SendAsync(method, arg);
        }
    }
}
