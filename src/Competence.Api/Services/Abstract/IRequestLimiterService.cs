﻿using System;
using CircularBuffer;

namespace Competence.Api.Services
{
    public interface IRequestLimiterService
    {
        public CircularBuffer<RequestLimiter> GetRequestBuffer();
        public void Put(RequestLimiter requestLimiter);
        public RequestLimiter Get();
    }
}
