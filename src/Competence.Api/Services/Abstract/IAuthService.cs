﻿using System;
using CircularBuffer;

namespace Competence.Api.Services
{
    public interface IAuthService
    {
        public string GetConnectionId();
        public string GetUserName();
    }
}
