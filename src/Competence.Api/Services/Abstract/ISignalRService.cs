﻿using System;
using CircularBuffer;

namespace Competence.Api.Services
{
    public interface ISignalRService
    {
        public void Send(string method, object arg);
    }
}
