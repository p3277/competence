﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace Competence.Api.Services
{
    public class AuthService : IAuthService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        
        public string GetConnectionId()
        {
            var connectionId = _httpContextAccessor.HttpContext?.Connection?.Id;

            return connectionId ?? "";
        }

        public string GetUserName()
        {
            var username = _httpContextAccessor.HttpContext?.User?.Identity?.Name;

            return username ?? "";
        }
    }
}
