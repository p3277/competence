﻿using CircularBuffer;
using System;

namespace Competence.Api.Services
{
    public class RequestLimiterService : IRequestLimiterService
    {
        private CircularBuffer<RequestLimiter> _requestBuffer;

        public RequestLimiterService()
        {
            _requestBuffer = new CircularBuffer<RequestLimiter>(10);
        }

        public RequestLimiter Get()
        {
            return _requestBuffer.Back();
        }

        public void Put(RequestLimiter requestLimiter)
        {
            _requestBuffer.PushBack(requestLimiter);
        }

        public CircularBuffer<RequestLimiter> GetRequestBuffer()
        {
            return _requestBuffer;
        }

    }
}
