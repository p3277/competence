﻿using System;

namespace Competence.Api.Services
{
    public class RequestLimiter
    {
        public DateTime RequestDate { get; set; }
        public string RequestPath { get; set; }
    }
}
