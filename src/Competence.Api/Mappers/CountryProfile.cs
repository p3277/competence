﻿using AutoMapper;
using Competence.Api.Models;
using Competence.Domain.Models;

namespace Competence.Api.Mappers
{
    public class CountryProfile : Profile
    {
        public CountryProfile()
        {
            CreateMap<CountryDTO, CountryResponse>();
            CreateMap<CountryRequest, CountryDTO>();
        }
    }
}
