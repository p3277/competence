﻿using AutoMapper;
using Competence.Api.Models;
using Competence.Domain.Models;
using System;
using System.Linq;

namespace Competence.Api.Mappers
{
    public class EmplouyeeProfile : Profile
    {
        public EmplouyeeProfile()
        {
            CreateMap<EmployeeDTO, EmployeeResponse>()
                .ForMember(x => x.Competencies, z => z.MapFrom(s => s.EmployeeCompetencies))
                .ForMember(x => x.Photo, z => z.Ignore());
            CreateMap<EmployeeRequest, EmployeeDTO>()
                .ForMember(x => x.Birthday, z => z.MapFrom(c => c.Birthday.Date));
            //.ForMember(x => x.Photo, z => z.MapFrom(c => Convert.FromBase64String(c.Photo.Split(",", StringSplitOptions.None).LastOrDefault())));
        }
    }
}


