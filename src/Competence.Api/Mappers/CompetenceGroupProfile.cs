﻿using AutoMapper;
using Competence.Api.Models;
using Competence.Domain.Models;

namespace Competence.Api.Mappers
{
    public class CompetenceGroupProfile : Profile
    {
        public CompetenceGroupProfile()
        {
            CreateMap<CompetenceGroupDTO, CompetenceGroupResponse>();
            CreateMap<CompetenceGroupRequest, CompetenceGroupDTO>();
        }
    }
}
