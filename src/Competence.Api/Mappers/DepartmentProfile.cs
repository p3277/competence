﻿using AutoMapper;
using Competence.Api.Models;
using Competence.Domain.Models;

namespace Competence.Api.Mappers
{
    public class DepartmentProfile : Profile
    {
        public DepartmentProfile()
        {
            CreateMap<DepartmentDTO, DepartmentResponse>();
            CreateMap<DepartmentRequest, DepartmentDTO>();
        }
    }
}
