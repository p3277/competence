﻿using AutoMapper;
using Competence.Api.Models;
using Competence.Domain.Models;

namespace Competence.Api.Mappers
{
    public class CompetenceProfile : Profile
    {
        public CompetenceProfile()
        {
            CreateMap<CompetenceDTO, CompetenceResponse>();
            CreateMap<CompetenceRequest, CompetenceDTO>();
            CreateMap<EmployeeCompetenciesDTO, CompetenceResponse>()
                .ForMember(x => x.Id, z => z.MapFrom(c => c.CompetenceId))
                .ForMember(x => x.Name, z => z.MapFrom(c => c.Competence.Name));
        }
    }
}
