﻿using AutoMapper;
using Competence.Api.Models;
using Competence.Domain.Models;

namespace Competence.Api.Mappers
{
    public class PositionProfile : Profile
    {
        public PositionProfile()
        {
            CreateMap<PositionDTO, PositionResponse>();
            CreateMap<PositionRequest, PositionDTO>();
        }
    }
}
