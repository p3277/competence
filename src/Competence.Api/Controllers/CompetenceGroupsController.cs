﻿using AutoMapper;
using Competence.Api.Hubs;
using Competence.Api.Models;
using Competence.Api.Services;
using Competence.Domain.Models;
using Competence.Domain.Services;
using Competence.Domain.Services.Abstract;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competence.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class CompetenceGroupsController : ControllerBase
    {
        private readonly ICompetenceGroupService _competenceGroupService;
        private readonly ILoggingService _loggingService;
        private readonly IAuthService _authService;
        private readonly IMapper _mapper;
        private readonly ISignalRService _signalService;

        public CompetenceGroupsController(ICompetenceGroupService competenceGroupService
            , ILoggingService loggingService
            , IAuthService authService
            , IMapper mapper
            , ISignalRService signalService)
        {
            _competenceGroupService = competenceGroupService;
            _loggingService = loggingService;
            _authService = authService;
            _mapper = mapper;
            _signalService = signalService;
        }


        [HttpGet]
        [ActionName("GetCompetenceGroups")]
        public async Task<IActionResult> GetCompetenceGroups()
        {
            var groups = await _competenceGroupService.GetAllAsync();
            if(groups == null)
            {
                _loggingService.AddError("Not found competencies groups", _authService.GetConnectionId(), "API");
                return BadRequest();
            }
            var result = _mapper.Map<IEnumerable<CompetenceGroupDTO>, IEnumerable<CompetenceGroupResponse>>(groups);
            return Ok(result);
        }

        [HttpGet]
        [ActionName("GetCompetenceGroupById")]
        public async Task<IActionResult> GetCompetenceGroupById(int id)
        {
            var group = await _competenceGroupService.GetByIdAsync(id);
            if (group == null)
            {
                _loggingService.AddError("Not found id competence group", _authService.GetConnectionId(), "API");
                return NotFound();
            }

            var result = _mapper.Map<CompetenceGroupDTO, CompetenceGroupResponse>(group);
            return Ok(result);
        }

        [HttpPost]
        [ActionName("CreateCompetenceGroup")]
        public async Task<IActionResult> CreateCompetenceGroup(CompetenceGroupRequest request)
        {
            try
            {
                var entity = _mapper.Map<CompetenceGroupRequest, CompetenceGroupDTO>(request);
                var group = await _competenceGroupService.CreateAsync(entity);
                var result = _mapper.Map<CompetenceGroupDTO, CompetenceGroupResponse>(group);
                _signalService.Send("ADD_COMPETENCEGROUP", result);
                return Ok(result);
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to create entry competence group", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

        [HttpPut]
        [ActionName("UpdateCompetenceGroup")]
        public async Task<IActionResult> UpdateCompetenceGroup(int id, CompetenceGroupRequest request)
        {
            var group = await _competenceGroupService.GetByIdAsync(id);
            if (group == null)
            {
                _loggingService.AddError("Competency group not found to update", _authService.GetConnectionId(), "API");
                return NotFound();
            }
            try
            {
                group = _mapper.Map(request, group);
                var newGroup = await _competenceGroupService.UpdateAsync(group);
                var result = _mapper.Map<CompetenceGroupDTO, CompetenceGroupResponse>(newGroup);
                _signalService.Send("UPDATE_COMPETENCEGROUP", result);
                return Ok(result);
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to update entry competence group", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

        [HttpDelete]
        [ActionName("DeleteCompetenceGroup")]
        public async Task<IActionResult> DeleteCompetenceGroup(int id)
        {
            var group = await _competenceGroupService.GetByIdAsync(id);
            if (group == null)
            {
                _loggingService.AddError("Competency group not found to delete", _authService.GetConnectionId(), "API");
                return NotFound();
            }

            try
            {
                await _competenceGroupService.DeleteAsync(group);
                _signalService.Send("DELETE_COMPETENCEGROUP", id);
                return Ok();
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to delete entry competence group", _authService.GetConnectionId(), "API", ex);
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }
    }
}
