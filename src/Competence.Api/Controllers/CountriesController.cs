﻿using AutoMapper;
using Competence.Api.Hubs;
using Competence.Api.Models;
using Competence.Api.Services;
using Competence.Domain.Models;
using Competence.Domain.Services;
using Competence.Domain.Services.Abstract;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competence.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class CountriesController : ControllerBase
    {
        private readonly ICountryService _countryService;
        private readonly ILoggingService _loggingService;
        private readonly IAuthService _authService;
        private readonly IMapper _mapper;
        private readonly ISignalRService _signalService;

        public CountriesController(ICountryService countryService
            , ILoggingService loggingService
            , IAuthService authService
            , IMapper mapper
            , ISignalRService signalService)
        {
            _countryService = countryService;
            _loggingService = loggingService;
            _authService = authService;
            _mapper = mapper;
            _signalService = signalService;
        }


        [HttpGet]
        [ActionName("GetCountries")]

        public async Task<IActionResult> GetCountries()
        {
            var countries = await _countryService.GetAllAsync();
            if (countries == null)
            {
                _loggingService.AddError("Not found countries", _authService.GetConnectionId(), "API");
                return NotFound();
            }
            var result = _mapper.Map<IEnumerable<CountryDTO>, IEnumerable<CountryResponse>>(countries);
            return Ok(result);
        }

        [HttpGet]
        [ActionName("GetCountryById")]
        public async Task<IActionResult> GetCountryById(int id)
        {
            var country = await _countryService.GetByIdAsync(id);
            if (country == null)
            {
                _loggingService.AddError("Not found country", _authService.GetConnectionId(), "API");
                return NotFound();
            }

            var result = _mapper.Map<CountryDTO, CountryResponse>(country);
            return Ok(result);
        }

        [HttpPost]
        [ActionName("CreateCountry")]
        public async Task<IActionResult> CreateCountry(CountryRequest request)
        {
            try
            {
                var entity = _mapper.Map<CountryRequest, CountryDTO>(request);
                var country = await _countryService.CreateAsync(entity);
                var result = _mapper.Map<CountryDTO, CountryResponse>(country);
                _signalService.Send("ADD_COUNTRY", result);
                return Ok(result);
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to create entry country", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

        [HttpPut]
        [ActionName("UpdateCountry")]
        public async Task<IActionResult> UpdateCountry(int id, CountryRequest request)
        {
            var country = await _countryService.GetByIdAsync(id);
            if (country == null)
            {
                _loggingService.AddError("Country not found to update", _authService.GetConnectionId(), "API");
                return NotFound();
            }
            try
            {
                country = _mapper.Map(request, country);
                var newCountry = await _countryService.UpdateAsync(country);
                var result = _mapper.Map<CountryDTO, CountryResponse>(newCountry);
                _signalService.Send("UPDATE_COUNTRY", result);
                return Ok(result);
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to update entry country", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

        [HttpDelete]
        [ActionName("DeleteCountry")]
        public async Task<IActionResult> DeleteCountry(int id)
        {
            var country = await _countryService.GetByIdAsync(id);
            if (country == null)
            {
                _loggingService.AddError("Country not found to delete", _authService.GetConnectionId(), "API");
                return NotFound();
            }
            try
            {
                await _countryService.DeleteAsync(country);
                _signalService.Send("DELETE_COUNTRY", id);
                return Ok();
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to delete entry country", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

    }
}
