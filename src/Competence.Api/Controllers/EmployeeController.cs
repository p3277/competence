﻿using AutoMapper;
using Competence.Api.Hubs;
using Competence.Api.Models;
using Competence.Api.Services;
using Competence.Domain.Models;
using Competence.Domain.Services.Abstract;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competence.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        private readonly ILoggingService _loggingService;
        private readonly IAuthService _authService;
        private readonly IMapper _mapper;
        private readonly IMemoryCache _cache;
        private const int _cachMinutes = 5;
        private readonly ISignalRService _signalService;

        public EmployeeController(IEmployeeService employeeService
            , ILoggingService loggingService
            , IAuthService authService
            , IMapper mapper
            , IMemoryCache memoryCache
            , ISignalRService signalService)
        {
            _employeeService = employeeService;
            _loggingService = loggingService;
            _authService = authService;
            _mapper = mapper;
            _cache = memoryCache;
            _signalService = signalService;
        }

        [HttpGet]
        [ActionName("GetEmployees")]
        public async Task<IActionResult> GetEmployees()
        {
            var employees = await _employeeService.GetAllAsync();
            var result = _mapper.Map<IEnumerable<EmployeeDTO>, IEnumerable<EmployeeResponse>>(employees).ToList();
            foreach (var r in result)
            {
                r.Photo = await GetCachedPhoto(r.Id); 
            }
            return Ok(result);
        }

        [HttpGet]
        [ActionName("GetEmployeeById")]
        public async Task<IActionResult> GetEmployeeById(int id)
        {
            var employee = await _employeeService.GetByIdAsync(id);
            if (employee == null)
            {
                _loggingService.AddError("Not found employee", _authService.GetConnectionId(), "API");
                return NotFound();
            }
            var result = _mapper.Map<EmployeeDTO, EmployeeResponse>(employee);
            result.Photo = await GetCachedPhoto(id);
            return Ok(result);
        }

        [HttpGet]
        [ActionName("GetPhoto")]
        public async Task<ActionResult<string>> GetPhoto(int employeeId)
        {
            return await GetCachedPhoto(employeeId);
        }


        [HttpPost]
        [ActionName("CreateEmployee")]
        public async Task<IActionResult> CreateEmployee(EmployeeRequest request)
        {
            try
            {
                var entity = _mapper.Map<EmployeeRequest, EmployeeDTO>(request);
                var employee = await _employeeService.CreateAsync(entity, request.CompetenceIds);
                await SetCachedPhoto(employee.Id, request.Photo);
                var result = _mapper.Map<EmployeeDTO, EmployeeResponse>(employee);
                result.Photo = await GetCachedPhoto(employee.Id);
                _signalService.Send("ADD_EMPLOYEE", result);
                return Ok(result);
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to create entry employee", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }

        }

        [HttpPut]
        [ActionName("UpdateEmployee")]
        public async Task<IActionResult> UpdateEmployee(int id, EmployeeRequest request)
        {
            var employee = await _employeeService.GetByIdAsync(id);
            if (employee == null)
            {
                _loggingService.AddError("Employee not found to update", _authService.GetConnectionId(), "API");
                return NotFound();
            }

            try
            {
                employee = _mapper.Map(request, employee);
                var newEmployee = await _employeeService.UpdateAsync(employee, request.CompetenceIds);
                await SetCachedPhoto(id, request.Photo);
                var result = _mapper.Map<EmployeeDTO, EmployeeResponse>(newEmployee);
                result.Photo = await GetCachedPhoto(newEmployee.Id);
                _signalService.Send("UPDATE_EMPLOYEE", result);
                return Ok(result);
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to update entry employee", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

        [HttpDelete]
        [ActionName("DeleteEmployee")]
        public async Task<IActionResult> DeleteEmployee(int id)
        {
            var employee = await _employeeService.GetByIdAsync(id);
            if (employee == null)
            {
                _loggingService.AddError("Employee not found to delete", _authService.GetConnectionId(), "API");
                return NotFound();
            }

            try
            {
                await _employeeService.DeleteAsync(employee);
                await DeleteCachedPhoto(id);
                _signalService.Send("DELETE_EMPLOYEE", id);
                return Ok();
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to delete entry employee", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

        private async Task<string> GetCachedPhoto(int employeeId)
        {
            string photo = null;
            if (!_cache.TryGetValue(employeeId, out photo))
            {
                photo = await _employeeService.GetPhotoAsync(employeeId);
                if (!string.IsNullOrEmpty(photo))
                {
                    _cache.Set(employeeId, photo, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(_cachMinutes)));
                }
            }
            return photo;
        }

        private async Task SetCachedPhoto(int employeeId, string photoValue)
        {
            await _employeeService.UpdatePhotoAsync(employeeId, photoValue);
            var photo = await _employeeService.GetPhotoAsync(employeeId);
            _cache.Set(employeeId, photo, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(_cachMinutes)
            });
        }

        private async Task DeleteCachedPhoto(int employeeId)
        {
            await _employeeService.UpdatePhotoAsync(employeeId, null);
            _cache.Remove(employeeId);
        }

    }
}
