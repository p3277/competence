﻿using AutoMapper;
using Competence.Api.Hubs;
using Competence.Api.Models;
using Competence.Api.Services;
using Competence.Domain.Models;
using Competence.Domain.Services;
using Competence.Domain.Services.Abstract;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competence.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class PositionsController : ControllerBase
    {
        private readonly IPositionService _positionService;
        private readonly ILoggingService _loggingService;
        private readonly IAuthService _authService;
        private readonly IMapper _mapper;
        private readonly ISignalRService _signalService;

        public PositionsController(IPositionService positionService
            , ILoggingService loggingService
            , IAuthService authService
            , IMapper mapper
            , ISignalRService signalService)
        {
            _positionService = positionService;
            _loggingService = loggingService;
            _authService = authService;
            _mapper = mapper;
            _signalService = signalService;
        }


        [HttpGet]
        [ActionName("GetPositions")]
        public async Task<IActionResult> GetPositions()
        {
            var positions = await _positionService.GetAllAsync();
            if (positions == null)
            {
                _loggingService.AddError("Not found positions", _authService.GetConnectionId(), "API");
                return NotFound();
            }
            var result = _mapper.Map<IEnumerable<PositionDTO>, IEnumerable<PositionResponse>>(positions);
            return Ok(result);
        }

        [HttpGet]
        [ActionName("GetPositionById")]
        public async Task<IActionResult> GetPositionById(int id)
        {
            var position = await _positionService.GetByIdAsync(id);
            if (position == null)
            {
                _loggingService.AddError("Not found position", _authService.GetConnectionId(), "API");
                return NotFound();
            }

            var result = _mapper.Map<PositionDTO, PositionResponse>(position);
            return Ok(result);
        }

        [HttpPost]
        [ActionName("CreatePosition")]
        public async Task<IActionResult> CreatePosition(PositionRequest request)
        {
            try
            {
                var entity = _mapper.Map<PositionRequest, PositionDTO>(request);
                var position = await _positionService.CreateAsync(entity);
                var result = _mapper.Map<PositionDTO, PositionResponse>(position);
                _signalService.Send("ADD_POSITION", result);
                return Ok(result);
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to create entry position", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

        [HttpPut]
        [ActionName("UpdatePosition")]
        public async Task<IActionResult> UpdatePosition(int id, PositionRequest request)
        {
            var position = await _positionService.GetByIdAsync(id);
            if (position == null)
            {
                _loggingService.AddError("Position not found to update", _authService.GetConnectionId(), "API");
                return NotFound();
            }
            try
            {
                position = _mapper.Map(request, position);
                var newPosition = await _positionService.UpdateAsync(position);
                var result = _mapper.Map<PositionDTO, PositionResponse>(newPosition);
                _signalService.Send("UPDATE_POSITION", result);
                return Ok(result);
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to update entry position", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

        [HttpDelete]
        [ActionName("DeletePosition")]
        public async Task<IActionResult> DeletePosition(int id)
        {
            var position = await _positionService.GetByIdAsync(id);
            if (position == null)
            {
                _loggingService.AddError("Position not found to delete", _authService.GetConnectionId(), "API");
                return NotFound();
            }
            try
            {
                await _positionService.DeleteAsync(position);
                _signalService.Send("DELETE_POSITION", id);
                return Ok();
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to delete entry position", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

    }
}
