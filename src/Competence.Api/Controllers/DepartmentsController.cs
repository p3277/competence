﻿using AutoMapper;
using Competence.Api.Hubs;
using Competence.Api.Models;
using Competence.Api.Services;
using Competence.Domain.Models;
using Competence.Domain.Services;
using Competence.Domain.Services.Abstract;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competence.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class DepartmentsController : ControllerBase
    {
        private readonly IDepartmentService _departmentService;
        private readonly ILoggingService _loggingService;
        private readonly IAuthService _authService;
        private readonly IMapper _mapper;
        private readonly ISignalRService _signalService;

        public DepartmentsController(IDepartmentService departmentService
            , ILoggingService loggingService
            , IAuthService authService
            , IMapper mapper
            , ISignalRService signalService)
        {
            _departmentService = departmentService;
            _loggingService = loggingService;
            _authService = authService;
            _mapper = mapper;
            _signalService = signalService;
        }


        [HttpGet]
        [ActionName("GetDepartments")]
        public async Task<IActionResult> GetDepartments()
        {
            var departments = await _departmentService.GetAllAsync();
            if(departments== null)
            {
                _loggingService.AddError("Not found departments", _authService.GetConnectionId(), "API");
                return NotFound();
            }
            var result = _mapper.Map<IEnumerable<DepartmentDTO>, IEnumerable<DepartmentResponse>>(departments);
            return Ok(result);
        }

        [HttpGet]
        [ActionName("GetDepartmentById")]
        public async Task<IActionResult> GetDepartmentById(int id)
        {
            var department = await _departmentService.GetByIdAsync(id);
            if (department == null)
            {
                _loggingService.AddError("Not found department", _authService.GetConnectionId(), "API");
                return NotFound();
            }

            var result = _mapper.Map<DepartmentDTO, DepartmentResponse>(department);
            return Ok(result);
        }

        [HttpPost]
        [ActionName("CreateDepartment")]
        public async Task<IActionResult> CreateDepartment(DepartmentRequest request)
        {
            try
            {
                var entity = _mapper.Map<DepartmentRequest, DepartmentDTO>(request);
                var department = await _departmentService.CreateAsync(entity);
                var result = _mapper.Map<DepartmentDTO, DepartmentResponse>(department);
                _signalService.Send("ADD_DEPARTMENT", result);
                return Ok(result);
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to create entry department", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

        [HttpPut]
        [ActionName("UpdateDepartment")]
        public async Task<IActionResult> UpdateDepartment(int id, DepartmentRequest request)
        {
            var department = await _departmentService.GetByIdAsync(id);
            if (department == null)
            {
                _loggingService.AddError("Department not found to update", _authService.GetConnectionId(), "API");
                return NotFound();
            }
            try
            {
                department = _mapper.Map(request, department);
                var newDepartment = await _departmentService.UpdateAsync(department);
                var result = _mapper.Map<DepartmentDTO, DepartmentResponse>(newDepartment);
                _signalService.Send("UPDATE_DEPARTMENT", result);
                return Ok(result);
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to update entry department", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

        [HttpDelete]
        [ActionName("DeleteDepartment")]
        public async Task<IActionResult> DeleteDepartment(int id)
        {
            var department = await _departmentService.GetByIdAsync(id);
            if (department == null)
            {
                _loggingService.AddError("Department not found to delete", _authService.GetConnectionId(), "API");
                return NotFound();
            }
            try
            {
                await _departmentService.DeleteAsync(department);
                _signalService.Send("DELETE_DEPARTMENT", id);
                return Ok();
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to delete entry department", _authService.GetConnectionId(), "API");
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

    }
}
