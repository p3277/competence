﻿using AutoMapper;
using Competence.Api.Hubs;
using Competence.Api.Models;
using Competence.Api.Services;
using Competence.Domain.Models;
using Competence.Domain.Services;
using Competence.Domain.Services.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competence.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class CompetenceController : ControllerBase
    {
        private readonly ICompetenceService _competenceService;
        private readonly ILoggingService _loggingService;
        private readonly IAuthService _authService;
        private readonly IMapper _mapper;
        private readonly ISignalRService _signalService;
        

        public CompetenceController(ICompetenceService competenceGroupService
            , ILoggingService loggingService
            , IAuthService authService
            , IMapper mapper
            , ISignalRService signalService)
        {
            _competenceService = competenceGroupService;
            _loggingService = loggingService;
            _authService = authService;
            _mapper = mapper;
            _signalService = signalService;
        }


        [HttpGet]
        [ActionName("GetCompetencies")]
        public async Task<IActionResult> GetCompetencies()
        {
            var competencies = await _competenceService.GetAllAsync();
            if (competencies == null)
            {
                _loggingService.AddError("Not found competencies", _authService.GetConnectionId(), "API");
                return BadRequest();
            }
            var result = _mapper.Map<IEnumerable<CompetenceDTO>, IEnumerable<CompetenceResponse>>(competencies);

            return Ok(result);

        }

        [HttpGet]
        [ActionName("GetCompetenceById")]
        public async Task<IActionResult> GetCompetenceById(int id)
        {
            var competencie = await _competenceService.GetByIdAsync(id);
            if (competencie == null)
            {
                _loggingService.AddError("Not found id competence", _authService.GetConnectionId(), "API");
                return NotFound();
            }

            var result = _mapper.Map<CompetenceDTO, CompetenceResponse>(competencie);
            return Ok(result);
        }

        [HttpPost]
        [ActionName("CreateCompetence")]
        public async Task<IActionResult> CreateCompetence(CompetenceRequest request)
        {
            try
            {
                var entity = _mapper.Map<CompetenceRequest, CompetenceDTO>(request);
                var competencie = await _competenceService.CreateAsync(entity);
                var result = _mapper.Map<CompetenceDTO, CompetenceResponse>(competencie);
                _signalService.Send("ADD_COMPETENCE", result);
                return Ok(result);
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to create entry competence", _authService.GetConnectionId(), "API",ex);
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

        [HttpPut]
        [ActionName("UpdateCompetence")]
        public async Task<IActionResult> UpdateCompetence(int id, CompetenceRequest request)
        {
            var competencie = await _competenceService.GetByIdAsync(id);
            if (competencie == null)
            {
                _loggingService.AddError("Competency not found to update", _authService.GetConnectionId(), "API");
                return NotFound();
            }
            try
            {
                competencie = _mapper.Map(request, competencie);
                var newCompetencie = await _competenceService.UpdateAsync(competencie);
                var result = _mapper.Map<CompetenceDTO, CompetenceResponse>(newCompetencie);
                _signalService.Send("UPDATE_COMPETENCE", result);
                return Ok(result);
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to update entry competence", _authService.GetConnectionId(), "API", ex);
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }

        [HttpDelete]
        [ActionName("DeleteCompetence")]
        public async Task<IActionResult> DeleteCompetence(int id)
        {
            var competencie = await _competenceService.GetByIdAsync(id);
            if (competencie == null)
            {
                _loggingService.AddError("Competency not found to delete", _authService.GetConnectionId(), "API"); 
                return NotFound();
            }
            try
            {
                await _competenceService.DeleteAsync(competencie);
                _signalService.Send("DELETE_COMPETENCE", id);
                return Ok();
            }
            catch (FluentValidation.ValidationException ex)
            {
                _loggingService.AddError("Failed to delete entry competence", _authService.GetConnectionId(), "API", ex);
                return BadRequest(string.Join("\n", ex.Errors.Select(x => x.ErrorMessage)));
            }
        }
    }
}
