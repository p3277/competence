﻿using Competence.Api.Models;
using Competence.Api.Services;
using Competence.Domain.Services;
using Competence.Domain.Services.Abstract;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace Competence.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class AdministrationController : ControllerBase
    {
        private readonly IRequestLimiterService _requestLimiterService;
        private readonly ILoggingService _loggingService;
        private readonly IAuthService _authService;

        public AdministrationController(IRequestLimiterService requestLimiterService, ILoggingService loggingService, IAuthService authService)
        {
            _requestLimiterService = requestLimiterService;
            _loggingService = loggingService;
            _authService = authService;
        }


        [HttpGet]
        [ActionName("GetLimiterRequests")]
        public IActionResult GetLimiterRequests()
        {
            try
            {
                var result = _requestLimiterService.GetRequestBuffer().Select(x => new RequestLimiterResponse() { RequestDate = x.RequestDate, RequestPath = x.RequestPath });

                return Ok(result);
            }
            catch (Exception ex)
            {
                _loggingService.AddError("Error limitter request", _authService.GetConnectionId(), "API", ex);
                return BadRequest(ex.Message);
            }
        }

    }
}
