﻿using System;
using System.Net;
using System.Threading.Tasks;
using CircularBuffer;
using Competence.Api.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Competence.Api.Middlewares
{

    public class RequestLimiterMiddleware : IMiddleware
    {
        private readonly ISystemClock _systemClock;
        private readonly IRequestLimiterService _requestLimiterService;
        private TimeSpan _interval;

        public RequestLimiterMiddleware(IConfiguration configuration, ISystemClock systemClock, IRequestLimiterService requestLimiterService)
        { 
            _systemClock = systemClock;
            _requestLimiterService = requestLimiterService;
            _interval = new TimeSpan(0, 0, 0, 0, configuration.GetValue<int>("RequestLimiter:IntervalMsec"));
            _requestLimiterService.Put(new RequestLimiter()
            {
                RequestDate = _systemClock.UtcNow.DateTime.ToLocalTime().AddMilliseconds(-1) - _interval,
                RequestPath =  ""
            });
            
        }


        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var now = _systemClock.UtcNow.DateTime.ToLocalTime();
            {
                var lastRequest = _requestLimiterService.Get();

                if (lastRequest.RequestDate >= now - _interval && _interval.TotalMilliseconds != 0)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.TooManyRequests;
                    context.Response.Headers["Retry-After"] = (lastRequest.RequestDate - now + _interval).TotalMilliseconds.ToString();
                }
                else
                {
                    _requestLimiterService.Put(new RequestLimiter()
                    {
                        RequestDate = now,
                        RequestPath = context.Request.Path
                    });
                    await next(context);
                }
            }
        }
    }

}
