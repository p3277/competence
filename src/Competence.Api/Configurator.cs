﻿using Competence.Api.Services;
using Competence.DataAccess.Data;
using Competence.DataAccess.Repositories;
using Competence.Domain.Models;
using Competence.Domain.Repositories;
using Competence.Domain.Services;
using Competence.Domain.Services.Abstract;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;

namespace Competence.Api
{
    public static class Configurator
    {
        public static IServiceCollection ConfigureInMemoryContext(this IServiceCollection services)
        {
            services.AddEntityFrameworkInMemoryDatabase()

                .AddDbContext<DataContext>(x =>
                {
                    x.UseInMemoryDatabase($"InMemoryCompetenceDb_{Guid.NewGuid()}");
                    x.UseSnakeCaseNamingConvention();
                    x.UseLazyLoadingProxies();
                });
            return services;
        }

        public static IServiceCollection GetServiceCollection(IConfiguration configuration, IServiceCollection services)
        {
            if (services == null)
            {
                services = new ServiceCollection();
            }

           
            services.AddControllers().AddMvcOptions(x =>
                                                    x.SuppressAsyncSuffixInActionNames = false)
                                                        .AddControllersAsServices();

            services.AddScoped<IDataContextInitializer, DataContextInitializer>();
            services.AddValidatorsFromAssembly(typeof(Domain.Validators.EmployeeValidator).Assembly);
            services.AddAutoMapper(typeof(Startup).Assembly, typeof(DataContext).Assembly);

            services.AddSingleton<IMemoryCache, MemoryCache>();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "competence.api", Version = "v1" });
            });


            services.AddCors(options =>
            {
                options.AddPolicy(name: "AllowAll",
                builder =>
                {
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                    builder.AllowAnyOrigin();
                });
            });


            RegisterServices(services);
            RegisterRepositories(services);

            return services;
        }

        public static IServiceCollection ConfigureDatabaseContext(this IServiceCollection services)
        {
            services.AddDbContext<DataContext>(x =>
            {
                x.UseSqlite("Filename=CompetenceDb.sqlite");
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });
            return services;

        }
        private static void RegisterServices(IServiceCollection services)
        {
            services.AddAuthentication("Bearer")
           .AddIdentityServerAuthentication("Bearer", options =>
           {
               options.ApiName = "competence.api";
               options.Authority = "https://localhost:44394";
           });

            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IPositionService, PositionService>();
            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<ICompetenceService, CompetenceService>();
            services.AddScoped<ICompetenceGroupService, CompetenceGroupService>();
            services.AddScoped<IEmployeeCompetenceService, EmployeeCompetenceService>();
            services.AddScoped<IAuthService, AuthService>();
        }

        private static void RegisterRepositories(IServiceCollection services)
        {
            services.AddScoped<IRepository<CountryDTO>, CountriesRepository>();
            services.AddScoped<IRepository<EmployeeDTO>, EmployeeRepository>();
            services.AddScoped<IRepository<PositionDTO>, PositionsRepository>();
            services.AddScoped<IRepository<DepartmentDTO>, DepartmentsRepository>();
            services.AddScoped<IRepository<CompetenceDTO>, CompetenciesRepository>();
            services.AddScoped<IRepository<CompetenceGroupDTO>, CompetenceGroupsRepository>();
            services.AddScoped<IRepository<EmployeeCompetenciesDTO>, EmployeeCompetenciesRepository>();
            services.AddScoped<IRepository<EmployeePhotoDTO>, EmployeePhotosRepository>();

        }
    }
}
