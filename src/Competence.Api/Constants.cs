﻿using System;

namespace Competence.Api
{
    public static class Constants
    {
        public const int CacheMinutes = 5;
    }
}
