using Competence.Api.Hubs;
using Competence.Api.Middlewares;
using Competence.Api.Services;
using Competence.DataAccess.Data;
using Competence.Domain.Services;
using Competence.Domain.Services.Abstract;
using Elastic.Apm.NetCoreAll;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Competence.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Configurator.GetServiceCollection(Configuration, services);

            services.AddSingleton<ILoggingService, LoggingService>();
            services.AddSingleton<ISignalRService, SignalRService>();
            services.AddSignalR();

            services.ConfigureDatabaseContext();
            services.AddTransient<ISystemClock, SystemClock>()
                .AddSingleton<IRequestLimiterService, RequestLimiterService>()
                .AddSingleton<RequestLimiterMiddleware>()
                .AddHttpContextAccessor();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDataContextInitializer dataContextInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "competence.api v1"));
            }


            app.UseHttpsRedirection();

            app.UseSerilogRequestLogging();

            app.UseRouting();

            app.UseCors("AllowAll");

            app.UseAuthentication();
            app.UseAuthorization();

            //app.UseMiddleware<RequestLimiterMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<NotificationsHub>("/notifications");
            });

            dataContextInitializer.InitializeDb();
        }
    }
}
