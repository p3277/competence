﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Competence.Identity.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Security.Claims;
using IdentityModel;

namespace Competence.Identity.Data
{
    public class SeedIdentityData
    {
        public static void EnsureSeedData()
        {
            var services = new ServiceCollection();
            services.AddLogging();
            services.AddDbContext<IdentityDataContext>(x =>
            {
                x.UseSqlite("Filename=IdentityDb.sqlite");
            });

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                })
                .AddEntityFrameworkStores<IdentityDataContext>()
                .AddDefaultTokenProviders();

            using (var serviceProvider = services.BuildServiceProvider())
            {
                using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var context = scope.ServiceProvider.GetService<IdentityDataContext>();
                    context.Database.Migrate();

                    var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

                    var user = userMgr.FindByNameAsync("petrov").Result;
                    if (user == null)
                    {
                        user = new ApplicationUser
                        {
                            UserName = "petrov",
                            Email = "petrov@email.com",
                            EmailConfirmed = true,
                        };
                        var result = userMgr.CreateAsync(user, "petrov").Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }

                        result = userMgr.AddClaimsAsync(user, new Claim[]{
                            new Claim(JwtClaimTypes.Name, "Vitaly Petrov"),
                            new Claim(JwtClaimTypes.GivenName, "Vitaly"),
                            new Claim(JwtClaimTypes.FamilyName, "Petrov")
                        }).Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }
                    }

                    user = userMgr.FindByNameAsync("korsakov").Result;
                    if (user == null)
                    {
                        user = new ApplicationUser
                        {
                            UserName = "korsakov",
                            Email = "korsakov@email.com",
                            EmailConfirmed = true,
                        };
                        var result = userMgr.CreateAsync(user, "korsakov").Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }

                        result = userMgr.AddClaimsAsync(user, new Claim[]{
                            new Claim(JwtClaimTypes.Name, "Roman Korsakov"),
                            new Claim(JwtClaimTypes.GivenName, "Roman"),
                            new Claim(JwtClaimTypes.FamilyName, "Korsakov")
                        }).Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }
                    }

                }
            }
        }

    }
}
