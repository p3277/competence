﻿using System.Collections.Generic;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace Competence.Identity
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResource
                {
                    Name="role",
                    UserClaims = new List<string>{ "role" }
                }
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("competence.api.default")
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new ApiResource[]
            {
                new ApiResource("competence.api")
                {
                    Scopes = new List<string>{ "competence.api.default" },
                    ApiSecrets = new List<Secret>{ new Secret("api.secret".Sha256()) }
                }
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                // m2m client credentials flow client
                new Client
                {
                    ClientId = "m2m.client",
                    ClientName = "Client Credentials Client",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    ClientSecrets = { new Secret("m2m.secret".Sha256()) },
                    AllowedScopes = { "competence.api.default" }
                },

                // interactive client using code flow + pkce
                new Client
                {
                    ClientId = "interactive",
                    ClientSecrets = { new Secret("interactive.secret".Sha256()) },
                    AllowedGrantTypes = GrantTypes.Implicit,
                    RedirectUris = { "http://localhost:3000/signin-oidc" },
                    FrontChannelLogoutUri = "http://localhost:3000/signout-oidc",
                    PostLogoutRedirectUris = { "http://localhost:3000/signout-oidc" },
                    AllowOfflineAccess = true,
                    AllowedScopes = { "openid", "profile", "competence.api.default" },
                    AllowAccessTokensViaBrowser = true,
                },
            };

        //public static List<TestUser> TestUsers =>
        //    new List<TestUser>
        //    {
        //        new TestUser
        //        {
        //            SubjectId = "1",
        //            Username = "petrov",
        //            Password = "petrov",
        //            Claims =
        //            {
        //                new Claim(JwtClaimTypes.Name, "Petrov Vitaly"),
        //                new Claim(JwtClaimTypes.GivenName, "Vitaly"),
        //                new Claim(JwtClaimTypes.FamilyName, "Petrov"),
        //                new Claim(JwtClaimTypes.Role, "Admin")
        //            }
        //        },
        //        new TestUser
        //        {
        //            SubjectId = "2",
        //            Username = "korsakov",
        //            Password = "korsakov",
        //            Claims =
        //            {
        //                new Claim(JwtClaimTypes.Name, "Korsakov Roman"),
        //                new Claim(JwtClaimTypes.GivenName, "Korsakov"),
        //                new Claim(JwtClaimTypes.FamilyName, "Roman"),
        //                new Claim(JwtClaimTypes.Role, "User")
        //            }
        //        }

        //};
    }
}
