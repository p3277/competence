﻿using Microsoft.AspNetCore.Identity;

namespace Competence.Identity.Models
{
    public class ApplicationUser: IdentityUser
    {
    }
}
