
const setEntities = (entityType, entities) => {
    return {
        type: `SET_${entityType.toUpperCase()}_LIST`,
        payload: entities
    }
}

const updateEntity = (entityType, entity) => {
    return {
        type: `UPDATE_${entityType.toUpperCase()}`,
        payload: entity
    }
}

const addEntity = (entityType, entity) => {
    return {
        type: `ADD_${entityType.toUpperCase()}`,
        payload: entity
    }
}

const deleteEntity = (entityType, id) => {
    return {
        type: `DELETE_${entityType.toUpperCase()}`,
        payload: id
    }
}

export default {
    setEntities,
    updateEntity,
    addEntity,
    deleteEntity
}

