
export const Positions = (state = [], action) => {
    switch (action.type) {
        case "SET_POSITION_LIST":
            return {
                ...state,
                Positions: action.payload
            }
        case "UPDATE_POSITION":
            const updatePositions = state.Positions.map((x,i) => x.id === action.payload.id ? action.payload : x);
            return {
                ...state,
                Positions: updatePositions
            }
        case "ADD_POSITION":
            var addPositions = [...new Set([...state.Positions, ...[action.payload]])];

            return {
                ...state,
                Positions: addPositions
            }
        case "DELETE_POSITION":
            var deletePositions = state.Positions.filter(item => {
                return item.id != action.payload;
            });

            return {
                ...state,
                Positions: deletePositions
            }
        default:
            return state
    }
}