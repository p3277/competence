
export const CompetenceGroups = (state = [], action) => {
    switch (action.type) {
        case "SET_COMPETENCEGROUP_LIST":
            return {
                ...state,
                CompetenceGroups: action.payload
            }
        case "UPDATE_COMPETENCEGROUP":
            const updateCompetenceGroups = state.CompetenceGroups.map((x,i) => x.id === action.payload.id ? action.payload : x);
            return {
                ...state,
                CompetenceGroups: updateCompetenceGroups
            }
        case "ADD_COMPETENCEGROUP":
            var addCompetenceGroups = [...new Set([...state.CompetenceGroups, ...[action.payload]])];

            return {
                ...state,
                CompetenceGroups: addCompetenceGroups
            }
        case "DELETE_COMPETENCEGROUP":
            var deleteCompetenceGroups = state.CompetenceGroups.filter(item => {
                return item.id != action.payload;
            });

            return {
                ...state,
                CompetenceGroups: deleteCompetenceGroups
            }
        default:
            return state
    }
}