
export const Countries = (state = [], action) => {
    switch (action.type) {
        case "SET_COUNTRY_LIST":
            return {
                ...state,
                Countries: action.payload
            }
        case "UPDATE_COUNTRY":
            const updateCountries = state.Countries.map((x,i) => x.id === action.payload.id ? action.payload : x);
            return {
                ...state,
                Countries: updateCountries
            }
        case "ADD_COUNTRY":
            var addCountries = [...new Set([...state.Countries, ...[action.payload]])];

            return {
                ...state,
                Countries: addCountries
            }
        case "DELETE_COUNTRY":
            var deleteCountries = state.Countries.filter(item => {
                return item.id != action.payload;
            });

            return {
                ...state,
                Countries: deleteCountries
            }
        default:
            return state
    }
}