
export const Competences = (state = [], action) => {
    switch (action.type) {
        case "SET_COMPETENCE_LIST":
            return {
                ...state,
                Competences: action.payload
            }
        case "UPDATE_COMPETENCE":
            const updateCompetences = state.Competences.map((x,i) => x.id === action.payload.id ? action.payload : x);
            return {
                ...state,
                Competences: updateCompetences
            }
        case "ADD_COMPETENCE":
            var addCompetences = [...new Set([...state.Competences, ...[action.payload]])];

            return {
                ...state,
                Competences: addCompetences
            }
        case "DELETE_COMPETENCE":
            var deleteCompetences = state.Competences.filter(item => {
                return item.id != action.payload;
            });

            return {
                ...state,
                Competences: deleteCompetences
            }
        default:
            return state
    }
}