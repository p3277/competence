
export const Employees = (state = [], action) => {
    switch (action.type) {
        case "SET_EMPLOYEE_LIST":
            return {
                ...state,
                Employees: action.payload
            }
        case "UPDATE_EMPLOYEE":
            const updateEmployees = state.Employees.map((x,i) => x.id === action.payload.id ? action.payload : x);
            return {
                ...state,
                Employees: updateEmployees
            }
        case "ADD_EMPLOYEE":
            var addEmployees = [...new Set([...state.Employees, ...[action.payload]])];

            return {
                ...state,
                Employees: addEmployees
            }
        case "DELETE_EMPLOYEE":
            var deleteEmployees = state.Employees.filter(item => {
                return item.id != action.payload;
            });

            return {
                ...state,
                Employees: deleteEmployees
            }
        default:
            return state
    }
}