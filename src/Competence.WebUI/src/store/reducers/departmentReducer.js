
export const Departments = (state = [], action) => {
    switch (action.type) {
        case "SET_DEPARTMENT_LIST":
            return {
                ...state,
                Departments: action.payload
            }
        case "UPDATE_DEPARTMENT":
            const updateDepartments = state.Departments.map((x,i) => x.id === action.payload.id ? action.payload : x);
            return {
                ...state,
                Departments: updateDepartments
            }
        case "ADD_DEPARTMENT":
            var addDepartments = [...new Set([...state.Departments, ...[action.payload]])];

            return {
                ...state,
                Departments: addDepartments
            }
        case "DELETE_DEPARTMENT":
            var deleteDepartments = state.Departments.filter(item => {
                return item.id != action.payload;
            });

            return {
                ...state,
                Departments: deleteDepartments
            }
        default:
            return state
    }
}