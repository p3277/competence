import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import Auth from './reducers/authReducer';
import { Departments } from './reducers/departmentReducer';
import { Positions } from './reducers/positiontReducer';
import { Countries } from './reducers/countryReducer';
import { CompetenceGroups } from './reducers/competencegroupReducer';
import { Competences } from './reducers/competenceReducer';
import { Employees } from './reducers/employeeReducer';


export default function configureStore(initialState) {

    const reducers = {
        auth: Auth,
        Departments,
        Positions,
        Countries,
        CompetenceGroups,
        Competences,
        Employees
    };
    const middleware = [thunk];
    const rootReducer = combineReducers({
        ...reducers
    });
    return createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() , compose(applyMiddleware(...middleware)));
}