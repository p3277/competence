import { HubConnectionBuilder, LogLevel, HttpTransportType } from "@microsoft/signalr";
import { URL } from './Constants';
import entityActions from '../store/actions/entityActions';


const connection = new HubConnectionBuilder()
    .withUrl("https://localhost:44397/notifications", {
        skipNegotiation: true,
        transport: HttpTransportType.WebSockets
    })
    .configureLogging(LogLevel.Information)
    .withAutomaticReconnect()
    .build();

connection.start();

connection.on("ADD_COMPETENCE", data => {
    console.log("SignalR");
    console.log(data);
});



const signalRMiddleware = store => next => action => {
    //connection.on("ADD_COMPETENCE", data => {
    //    console.log("SignalR");
    //    console.log(data);
    //});

    return next => action => {
        return next(action);
    };
}

export default signalRMiddleware;