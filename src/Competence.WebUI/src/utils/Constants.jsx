export const GET_ITEMS_SUCCESS = 'GET_POSTS_SUCCESS'
export const GET_ITEMS_ERROR = 'GET_POSTS_ERROR'
export const URL = 'https://localhost:44397/api/'
export const SIGNALR_URL = 'https://localhost:44397/notifications'

export const isAdmin = true;
export const matchDict = [
    { essence: 'Competence', forAction: 'Competence' },
    { essence: 'CompetenceGroup', forAction: 'CompetenceGroups' },
    { essence: 'Department', forAction: 'Departments' },
    { essence: 'Position', forAction: 'Positions' },
    { essence: 'Country', forAction: 'Countries' },
    { essence: 'Employee', forAction: 'Employee' }
];