import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import { URL } from './Constants.jsx'
import AlertTemplate from '../components/templates/AlertAndNotify/AlertTemplate.jsx'

//import Alert from 'react-bootstrap/Alert'

export let updateItemAsync = async (action, data) => {

    console.log('updateItemAsync', data);

    return new Promise(function (resolve, reject) {
        fetch(URL + action, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
            .then(response => {
                if (!response.ok)
                    throw response.text();
                return response.json();
            })
            .then(data => {
                resolve(data)
            })
            .catch((error) => {
                error.then(x => reject(x));
            });
    });
}

export let deleteItemAsync = async (action) => {
    return new Promise(function (resolve, reject) {
        fetch(URL + action, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                console.log('response.ok', response.ok)
                if (!response.ok)
                    throw response.text();
                return resolve(response.text());
            })
            .then(data => {
                resolve(data);
            })
            .catch(error => {
                error.then(x => reject(x));
            });
    });
}


export let createItemAsync = async (action, data) => {
    return new Promise(function (resolve, reject) {
        fetch(URL + action, {
            //mode: 'no-cors',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
            .then(response => {
                if (!response.ok) 
                    throw response.text();
                return response.json();
            })
            .then(data => {
                resolve(data);
            })
            .catch((error) => {
                error.then(x => reject(x));
            });
    });
}

export let getItemsAsync = async (action) => {    
    return new Promise(function (resolve, reject) {
        fetch(URL + action).then(res => res.json()).then(json => {
            resolve(json);
        });
    });
}

export let getItemAsync = async (action, id) => {
    return new Promise(function (resolve, reject) {
        fetch(URL + action + '?id=' + id).then(res => res.json()).then(json => {
            resolve(json);
        });
    });
}

// For filtering tables
function getIdsArrayFromDS(array) {
    let idsArr = [];
    array.forEach(function (item) {
        idsArr.push(item.id)
    });
    return idsArr;
}

export function getDataFromIdsArray(arrayIds, dataSource) {
    let newArr = [];
    dataSource.forEach(function (item) {
        arrayIds.forEach(function (id) {
            if (item.id === id)
                newArr.push(item);
        });
    });
    return newArr;
}

// arrays of integer elements
function getIntersect(arrays) {
    var result = [];
    if (arrays.length > 0) {
        result = arrays.shift().reduce(function (res, v) {
            if (res.indexOf(v) === -1 && arrays.every(function (a) {
                return a.indexOf(v) !== -1;
            })) res.push(v);
            return res;
        }, []);
    }
    return result;
}

export function iterateArraysAndGetResult(arrays, wholeArray) {
    let tmpArray = [];
    arrays.forEach(function (array) {
        if (array.length > 0)
            tmpArray.push(getIdsArrayFromDS(array));
    });
    let res = [];
    let tmpIntersectArray = getIntersect(tmpArray);
    res = getDataFromIdsArray(tmpIntersectArray, wholeArray);

    
    return res;
}

export function getForAction(dict, key) {
    let forAction = '';
    dict.forEach(function (item) {        
        if (item.essence == key)
            forAction = item.forAction;
    });
    return forAction;
}