import { GET_ITEMS_SUCCESS, GET_ITEMS_ERROR, URL } from './Constants.jsx'
import "isomorphic-fetch";

export function receiveItems(data) {
    return {
        type: GET_ITEMS_SUCCESS,
        items: data
    }
}

export function errorReceive(error) {
    return {
        type: GET_ITEMS_ERROR,
        error: error
    }
}

export let getItems = (action) => {
    return (_dispatch) => {
        fetch(URL + action)
            .then(res => res.json())
            .then(json => { _dispatch(receiveItems(json)); })
            .catch((ex) => { _dispatch(errorReceive(ex)); });
    }
}