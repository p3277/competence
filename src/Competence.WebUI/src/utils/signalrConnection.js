import { HubConnectionBuilder, LogLevel, HttpTransportType } from "@microsoft/signalr";
import { SIGNALR_URL } from './Constants.jsx'

const connection = new HubConnectionBuilder()
    .withUrl(SIGNALR_URL, {
        skipNegotiation: true,
        transport: HttpTransportType.WebSockets
    })
    .configureLogging(LogLevel.Information)
    .withAutomaticReconnect()
    .build();

export default connection;