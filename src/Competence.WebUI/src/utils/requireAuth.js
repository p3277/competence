import React from 'react'
import { useSelector } from 'react-redux'
import { signinRedirect } from './userService';

function RequireAuth({ children }) {
    const user = useSelector(state =>
        state.auth.user)
    console.log('auth');
    console.log(user);
    let cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)idsrv.session\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    if (!user) {
        signinRedirect();
        return "Loading...";
    }

    return children;
}

export default RequireAuth;