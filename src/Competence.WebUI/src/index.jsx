import React from 'react';
import ReactDOM from 'react-dom';
import './css/Index.css';
import  App from './components/masters/App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';


console.log("index")
ReactDOM.render(
     <App />,
    document.getElementById('divContent')

)

reportWebVitals();
