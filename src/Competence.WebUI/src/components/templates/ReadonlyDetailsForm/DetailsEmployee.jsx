import React from 'react';
import CloseButton from 'react-bootstrap/CloseButton';

export function DetailsEmployee(data) {
    if (!data.active) { return ''; }

    console.log('DetailsEmployee');
    console.log(data);

    const item = data.active;
    const description = data.description;

    const id = item.id;
    const firstName = item.firstName;
    const middleName = item.middleName;
    const lastName = item.lastName;
    const fullName = lastName + ' ' + firstName;
    const email = '<a href="mailto:' + item.email + '">' + item.email + "</a>";
    const photo = item.imgSrc;

    const countryCode = item.country.code;
    const countryFlag = 'http://purecatamphetamine.github.io/country-flag-icons/3x2/' + countryCode + '.svg';

    const country = '<span><img style="width: 30px;" src="' + countryFlag + '" />  ' + item.country.name + '</span>';
    const department = item.department.name;
    const position = item.position.name;
    const competencies = item.competencies.map(function (key) {
        return '<span class="tag">' + key.name + '</span>&nbsp;';
    }).join('');

    //const sex = Math.floor(Math.random() * 100) + 1;

    return (
        <React.Fragment>
            <div className="thumbnail border " style={{ padding: 15 }}>
            <div><img src={photo} alt="employee details" style={{ maxWidth: 155 }} className="rounded-circle"/></div>
            <div style={{ float: 'right', marginTop: -158 }}><CloseButton onClick={data.onDetailsClose } /></div>
            <br />
            <div className="thumbnail-caption">
                <h3>{fullName}</h3>
                    <table className="employeeDetailsTable table table-responsive" >
                    <tbody>
                        <tr>
                            <td><b>ID:</b></td>
                            <td>{id}</td>
                        </tr>
                        <tr>
                            <td><b>Firest name:</b></td>
                            <td>{firstName}</td>
                        </tr>
                        <tr>
                            <td><b>Middle name:</b></td>
                            <td>{middleName}</td>
                        </tr>
                        <tr>
                            <td><b>Last name:</b></td>
                            <td>{item.lastName}</td>
                        </tr>
                        <tr>
                            <td><b>Email:</b></td>
                            <td><span dangerouslySetInnerHTML={{ __html: email }} /></td>
                        </tr>
                        {/*<tr>*/}
                        {/*    <td><b>Sex:</b></td>*/}
                        {/*    <td style={{ color: 'red' }} >{ sex } times a week</td>*/}
                        {/*</tr>*/}
                        <tr>
                            <td><b>Country:</b></td>
                                <td><span dangerouslySetInnerHTML={{ __html: country }} /></td>
                        </tr>
                        <tr>
                            <td><b>Department:</b></td>
                            <td>{department}</td>
                        </tr>
                        <tr>
                            <td><b>Position:</b></td>
                            <td>{position}</td>
                        </tr>
                        <tr>
                            <td><b>Competencies:</b></td>
                                <td><span dangerouslySetInnerHTML={{ __html: competencies }} /></td>
                        </tr>                        
                    </tbody>
                </table>
                    <p className="text-justify" style={{ maxWidth: 500, wordBreak: 'break-all', textAlign: 'left' }}><b>Description:</b> <i><u>{firstName}</u></i> {description}</p>
            </div>
            </div>
        </React.Fragment>
    );
};