import React from 'react';
import CloseButton from 'react-bootstrap/CloseButton';

export function DetailsItem(data) {
    if (!data.active) { return ''; }

    const item = data.active;
    const title = data.type;
    const description = data.description;

    return (
        <div className="thumbnail border " style={{ padding: 15 }}>
            <div><img src={item.imgSrc} alt='details' style={{ minWidth: 35, maxWidth: 45 }} /></div>
            <div style={{ float: 'right', marginTop: -23 }}><CloseButton onClick={data.onDetailsClose } /></div>
            <br />
            <div className="thumbnail-caption">
                <h3>{item.name}</h3>
                <br/>
                <table className="detailsTable table table-responsive" style={{ textAlign: 'left' }} >
                    <tbody>
                        <tr>
                            <td><b>{title} ID:</b></td>
                            <td>{item.id}</td>
                        </tr>
                        <tr>
                            <td><b>{title} name:</b></td>
                            <td>{item.name}</td>
                        </tr>
                            {
                                (title === 'Country') ?
                                    <tr><td><b>{title} code:</b></td><td>{item.code}</td></tr>
                                    :
                                    null
                            }
                    </tbody>
                </table>
                <p className="text-justify" style={{ maxWidth: 500, wordBreak: 'break-all', textAlign: 'left' }}><i><u>{item.name}</u></i> {description}</p>
            </div>
            </div>
    );
};