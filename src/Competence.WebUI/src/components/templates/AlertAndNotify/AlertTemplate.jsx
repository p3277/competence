import React from 'react'
import Alert from 'react-bootstrap/Alert'

export default function AlertTemplate(props) {
    let mode = props.mode;
    let action = props.action;
    let supportText = 'Please try again later or contact support group';

    let alertText = '';
    let color = 'green';
    if (action == 'create' && (props.mode == 'success'/* || props.mode.id != undefined*/)) {
        alertText = 'Record was created successfully';
        supportText = '';
        color = 'green';
    }
    else if (action == 'create' && mode != 'success') {
        alertText = 'Record was NOT created: ' + mode;
        color = 'red';
    }

    if (action == 'update' && mode == 'success') {
        alertText = 'Record was updated successfully';
        supportText = '';
        color = 'green';
    }
    else if (action == 'update' && mode != 'success') {
        alertText = 'Record was NOT updated: ' + mode;
        color = 'red';
    }

    if (action == 'delete' && mode == 'success') {
        alertText = 'Record was deleted successfully.';
        supportText = '';
        color = 'green';
    }
    else if (action == 'delete' && mode != 'success') {
        alertText = 'Record was NOT deleted: ' + mode;
        color = 'red';
    }

    return (
        <Alert variant={mode} style={{ backgroundColor: 'white' }} >
            <Alert.Heading style={{ textAlign: 'left', fontSize: 16, color: color }}>Action information alert</Alert.Heading>
            <hr />
            <p style={{ textAlign: 'left', fontSize: 15, color: color }}>
                    {alertText}
            </p>

            {supportText != '' ?
                <React.Fragment >
                    <p className="mb-0" style={{ textAlign: 'left', color: 'tomato', fontSize: 11 }}>
                            {supportText}
                        </p>
                </React.Fragment>:
                null
            }
            
        </Alert>
    );
}

/*
export default class AlertTemplate extends React.Component {
    constructor(props) {
        super(props);

        console.log('props.mode');
    }

    render() {
        const mode = this.props.mode;
        const action = this.props.action;


        console.log('props.mode');
        console.log(this.props.mode);

        console.log('props.action');
        console.log(this.props.action);

        let alertText = '';
        if (action == 'create' && mode == 'success')
            alertText = 'Record was created successfully';
        else if (action == 'create' && mode != 'success')
            alertText = 'Record was NOT created.';

        if (action == 'update' && mode == 'success')
            alertText = 'Record was updated successfully';
        else if (action == 'update' && mode != 'success')
            alertText = 'Record was NOT updated.';

        if (action == 'delete' && mode == 'success')
            alertText = 'Record was deleted successfully';
        else if (action == 'delete' && mode != 'success')
            alertText = 'Record was NOT deleted.';

        const supportText = 'Please try again later or contact support group';

        return (
            <React.Fragment >
            <Alert variant="success" >
                <Alert.Heading>Action alert</Alert.Heading>
                <p>
                    {alertText}
                </p>
                <hr />
                <p className="mb-0">
                    {supportText}
                </p>
                </Alert>
            </React.Fragment>
        );
    }
};
*/