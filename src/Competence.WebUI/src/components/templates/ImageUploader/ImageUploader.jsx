import React, { Component } from 'react';

export class ImageUploader extends Component {
    constructor(props) {
        super(props);
        this.handleChangeImage.bind(this);
        this.state = {
            imgSrc: props.imgSrc ? props.imgSrc : require('../../../img/noImage.png')
        };
    }

    componentDidMount() {
        console.log('componentDidMount');
    }

    componentDidUpdate(previousProps, previousState) {
        if (previousProps.imgSrc !== this.props.imgSrc) {
            this.setState(
                {
                    imgSrc: this.props.imgSrc ? this.props.imgSrc : require('../../../img/noImage.png')
                }
            )
        }
    }

    onChange() {
        // Assuming only image
        var file = this.refs.file.files[0];
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);

        reader.onloadend = function (e) {
            this.setState({
                imgSrc: [reader.result]
            })
        }.bind(this);
        console.log(url) // Would see a path?
        // TODO: concat files
    }

    handleChangeImage = e => {
        this.setState({ imgSrc: URL.createObjectURL(e.target.files[0]) });
        this.props.setStateOfParent(e.target.files[0]);
    }

    render() {
        return (
            <>
                <label className="custom-file-upload" style={{ marginBottom: 20, height: 80, maxHeight: 80, minHeight: 80 }} >
                    <div id="parentImageUploder" style={{ cursor: 'pointer' }} title="Click to select" className="rounded-circle">
                        <div id="contentImageUploder" className=" rounded-circle">
                            Select photo
                        </div>
                        <img src={this.state.imgSrc} style={{ minWidth: 85, width: 85, maxWidth: 85 }} className="imgPreview rounded-circle" />
                    </div>
                    <input type="file" id="img" name="img" accept="image/*" onChange={this.handleChangeImage} style={{ maxHeight: 85, display: 'none' }}/>
                </label>
                
            </>
        );
    }
}