import React from 'react';

export function SearchBox(props) {
    return (
        <div className="searchbar" style={{ width: props.width + 'px' }}>
            <input
                type="text"
                className="form-control"
                placeholder={props.placeholderTitle}
                onChange={props.onChange}
            />
        </div>
    );
}