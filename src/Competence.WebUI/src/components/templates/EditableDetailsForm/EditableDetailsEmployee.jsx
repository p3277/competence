
import React, { Component } from 'react'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import CloseButton from 'react-bootstrap/CloseButton';
import { createItemAsync, updateItemAsync, deleteItemAsync, getForAction } from '../../../utils/Core.jsx';
import { matchDict } from '../../../utils/Constants.jsx';
import { MultiSelectBox } from '../MultiSelectBox/MultiSelectBox.jsx';
import { ImageUploader } from '../ImageUploader/ImageUploader.jsx';
import AlertTemplate from '../AlertAndNotify/AlertTemplate.jsx';
import entityActions from '../../../store/actions/entityActions';
import { connect } from 'react-redux';

class EditFormEmployee extends Component {
    constructor(props) {
        super(props);

        const employee = props.active;
        this.state = {
            id: (employee.id) ? employee.id : '',
            firstName: (employee.firstName) ? employee.firstName : '',
            middleName: (employee.middleName) ? employee.middleName : '',
            lastName: (employee.lastName) ? employee.lastName : '',
            email: (employee.email) ? employee.email : '',
            photo: (employee.photo) ? employee.photo : require('../../../img/noImage.png'),
            birthday: (employee.birthday) ? employee.birthday : new Date(),
            competencies: (employee.competencies) ? employee.competencies : '',
            country: (employee.country) ? employee.country : '',
            department: (employee.department) ? employee.department : '',
            position: (employee.position) ? employee.position : '',
            title: 'Employee',
            showAlert: false,
            actionAlert: '',
            actionMode: ''
        };

        this.setStateOfParent = this.setStateOfParent.bind(this);
    }

    handleControlChange = event => {
        let control = event.target.id;
        if (control == 'firstName')
            this.setState({ firstName: event.target.value });
        if (control == 'middleName')
            this.setState({ middleName: event.target.value });
        if (control == 'lastName')
            this.setState({ lastName: event.target.value });
        if (control == 'email')
            this.setState({ email: event.target.value });
    }

    handleCountryChange = event => {
        this.setState({ country: event});
    }

    handleDepartmentChange = event => {
        this.setState({ department: event });
    }

    handlePositionChange = event => {
        this.setState({ position: event });
    }

    handleCompetenciesChange = event => {
        this.setState({ competencies: event });
    }

    deleteItem = () => {
        let action = getForAction(matchDict, this.state.title);
        let answer = window.confirm('Are you sure you want to delete this item? ');
        if (answer)
            deleteItemAsync(action + '/Delete' + this.state.title + '?id=' + this.state.id).then((result) => {
                //this.props.deleteEntity(this.state.title, this.state.id);
                this.successDeleteCallback(result);
            },
                (error) => {
                    this.failureDeleteCallback(error);
                }
            );
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.updateItem();
    }

    updateItem = () => {
        console.log('EMPLOYEE UPDATE Object');
        console.log(this.state);
        const data = {
            FirstName: this.state.firstName,
            MiddleName: this.state.middleName,
            LastName: this.state.lastName,
            Birthday: this.state.birthday,
            Sex: 1,
            CountryId: this.state.country.id,
            DepartmentId: this.state.department.id,
            PositionId: this.state.position.id,
            CompetenceIds: this.state.competencies.map((c) => c.id),
            Email: this.state.email,
            Photo: this.state.photo
        }

        let action = getForAction(matchDict, this.state.title);

        if (this.state.id) 
            updateItemAsync(action + '/Update' + this.state.title + '?id=' + this.state.id, data)
                .then((result) => {
                    //this.props.updateEntity(this.state.title, result);
                    this.successUpdateCallback(result)
                },
                    (error) => {
                        this.failureUpdateCallback(error)
                    }
                );
        else 
            createItemAsync(action + '/Create' + this.state.title, data)
                .then((result) => {
                    //this.props.addEntity(this.state.title, result);
                    this.successCreateCallback(result);
                },
                    (error) => {
                        this.failureCreateCallback(error);
                    }
                );
    }

    handleEscape = (e) => {
        if (e.key === 'Escape') this.props.onDetailsClose();
    }

    componentDidMount() {
        window.addEventListener('keyup', this.handleEscape)
    }

    componentDidUpdate(previousProps, previousState) {
        if (previousProps.active !== this.props.active)
        {
            const employee = this.props.active;
            this.setState(
                 {
                    id: (employee.id) ? employee.id : '',
                    firstName: (employee.firstName) ? employee.firstName : '',
                    middleName: (employee.middleName) ? employee.middleName : '',
                    lastName: (employee.lastName) ? employee.lastName : '',
                    fullName: (employee.fullName) ? employee.fullName : '',
                    email: (employee.email) ? employee.email : '',
                    photo: (employee.photo) ? employee.photo : require('../../../img/noImage.png'),
                    birthday: (employee.birthday) ? employee.birthday : new Date(),
                    competencies: (employee.competencies) ? employee.competencies : '',
                    country: (employee.country) ? employee.country : '',
                    department: (employee.department) ? employee.department : '',
                    position: (employee.position) ? employee.position : '',
                    title: 'Employee'
                }
            )
        }
    }

    componentWillUnmount() {
        console.log('componentWillUnmount');
        window.removeEventListener('keyup', this.handleEscape);
    }

    getBase64(file, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
            return error;
        };
    }

    setStateOfParent(newImg) {
        console.log('setStateOfParent');
        console.log(newImg);

        this.getBase64(newImg, (result) => {
            this.setState({
                photo: (newImg) ? result : require('../../../img/noImage.png'),
            })
        });
    }

    successUpdateCallback = (result) => {

        this.setState({
            actionAlert: 'update',
            actionMode: 'success'
        });

        this.setState({ showAlert: true }, () => {
            window.setTimeout(() => {
                this.setState({ showAlert: false })
            }, 2500)
        });
    }


    failureUpdateCallback(error) {
        this.setState({
            actionAlert: 'update',
            actionMode: error
        });

        this.setState({ showAlert: true }, () => {
            window.setTimeout(() => {
                this.setState({ showAlert: false })
            }, 2500)
        });
    }

    successCreateCallback = (result) => {
        this.setState({
            actionAlert: 'create',
            actionMode: 'success'
        });

        this.setState({ showAlert: true }, () => {
            window.setTimeout(() => {
                this.setState({ showAlert: false })
            }, 2500)
        });

        console.log('successCreateCallback', result);
        let createdObj = result;
        createdObj.imgSrc = '';
        this.props.onRowSelect(createdObj);
    }

    failureCreateCallback(error) {
        this.setState({
            actionAlert: 'create',
            actionMode: error
        });

        this.setState({ showAlert: true }, () => {
            window.setTimeout(() => {
                this.setState({ showAlert: false })
            }, 2500)
        });
    }

    successDeleteCallback = (result) => {

        this.props.onDetailsClose()
        this.setState({
            actionAlert: 'delete',
            actionMode: 'success'
        });

        this.setState({ showAlert: true }, () => {
            window.setTimeout(() => {
                this.setState({ showAlert: false })
            }, 2500)
        });
    }

    failureDeleteCallback = (result) => {

        this.setState({
            actionAlert: 'delete',
            actionMode: result
        });

        this.setState({ showAlert: true }, () => {
            window.setTimeout(() => {
                this.setState({ showAlert: false })
            }, 2500)
        });
    }

    render() {
        const customStyles = {
            control: css => ({
                ...css,
                //width: 334,
                textAlign: 'left'
            }),
            menu: ({ width, ...css }) => ({
                ...css,
                width: '334px',
                //minWidth: '20%',
                textAlign: 'left'
            }),
            option: css => ({ ...css, width: 334, textAlign: 'left' }),
        };

        return (
            <React.Fragment >
                <div className="thumbnail border " style={{ padding: 15, position:'relative' }}>
                    <h4>{this.state.title} details</h4>

                    <ImageUploader imgSrc={this.state.photo} setStateOfParent={this.setStateOfParent} />

                    <div style={{ float: 'right', top: 1, right: 1, position: 'absolute' }}><CloseButton onClick={this.props.onDetailsClose} /></div>
                    <br />
                    <Form>
                        <Form.Group className="employeeFormControl mb-3" controlId="firstName">
                            <Form.Control type="text" placeholder="Enter first name (required)" value={this.state.firstName} required name="firstName" onChange={this.handleControlChange} />
                        </Form.Group>

                        <Form.Group className="employeeFormControl mb-3" controlId="middleName">
                            <Form.Control type="text" placeholder="Enter middle name" value={this.state.middleName} required name="middleName" onChange={this.handleControlChange} />
                        </Form.Group>

                        <Form.Group className="employeeFormControl mb-3" controlId="lastName">
                            <Form.Control type="text" placeholder="Enter last name (required)" value={this.state.lastName} required name="lastName" onChange={this.handleControlChange} />
                        </Form.Group>

                        <Form.Group className="employeeFormControl mb-3" controlId="email">
                            <Form.Control type="email" placeholder="Enter email (required)" value={this.state.email} required name="email" onChange={this.handleControlChange} />
                        </Form.Group>

                        <Form.Group className="employeeFormControl mb-3" controlId="competencies">
                            <MultiSelectBox action='Competence/GetCompetencies' placeholderTitle='&nbsp;Select competecies... (required)' value={this.state.competencies} onChange={this.handleCompetenciesChange} name="position" isMulti="true"
                                customStyles={customStyles} />
                        </Form.Group>

                        <Form.Group className="employeeFormControl mb-3" controlId="country">
                            <MultiSelectBox action='Countries/GetCountries' placeholderTitle='&nbsp;Select country... (required)' value={this.state.country} onChange={this.handleCountryChange} name="country" customStyles={customStyles} />
                        </Form.Group>

                        <Form.Group className="employeeFormControl mb-3" controlId="department">
                            <MultiSelectBox action='Departments/GetDepartments' placeholderTitle='&nbsp;Select department... (required)' value={this.state.department} onChange={this.handleDepartmentChange} name="department" customStyles={customStyles} />
                        </Form.Group>

                        <Form.Group className="employeeFormControl mb-3" controlId="position" >
                            <MultiSelectBox action='Positions/GetPositions' placeholderTitle='&nbsp;Select position... (required)' value={this.state.position} onChange={this.handlePositionChange} name="position" customStyles={customStyles} />
                        </Form.Group>
                        
                        <Button variant="outline-primary" type="submit" size="sm" onClick={this.handleSubmit} style={{ marginRight: 7 }}>
                            Update
                        </Button>
                        {this.state.id ?
                            <Button variant="outline-danger" size="sm" onClick={this.deleteItem} style={{ marginRight: 7 }} >
                                Delete
                            </Button>
                            :
                            ''
                        }
                        <Button variant="outline-secondary" size="sm" onClick={this.props.onDetailsClose} >
                            Cancel
                        </Button>
                    </Form>
                </div>

                {this.state.showAlert ?
                    <AlertTemplate show={this.state.showAlert} mode={this.state.actionMode} action={this.state.actionAlert} /> :
                    null
                }
            </React.Fragment>
        );
    }
}

let mapDispatch = (dispatch) => {
    return {
        updateEntity: (entityType, item) => dispatch(entityActions.updateEntity(entityType, item)),
        addEntity: (entityType, item) => dispatch(entityActions.addEntity(entityType, item)),
        deleteEntity: (entityType, id) => dispatch(entityActions.deleteEntity(entityType, id)),
    }
}

export default connect(null, mapDispatch)(EditFormEmployee)


