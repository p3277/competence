import React, { Component } from 'react'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import CloseButton from 'react-bootstrap/CloseButton';
import { createItemAsync, updateItemAsync, deleteItemAsync, getForAction } from '../../../utils/Core.jsx';
import { matchDict } from '../../../utils/Constants.jsx';
import AlertTemplate from '../AlertAndNotify/AlertTemplate.jsx';
import entityActions from '../../../store/actions/entityActions';
import { connect } from 'react-redux';

class EditForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: (props.active.id) ? props.active.id : '',
            name: (props.active.name) ? props.active.name : '',
            code: (props.active.code) ? props.active.code : '',
            imgSrc: (props.active.imgSrc) ? props.active.imgSrc : require('../../../img/noImage.png'),
            title: (props.type) ? props.type : '',
            showAlert: false,
            actionAlert: '',
            actionMode: '',
            alertErrorMessage: ''
        };
    }

    handleNameChange = event => {
        this.setState({ name: event.target.value });
    }

    handleCodeChange = event => {
        if (event.currentTarget.value.length == 2)
            this.setState({ imgSrc: 'http://purecatamphetamine.github.io/country-flag-icons/3x2/' + event.currentTarget.value.toUpperCase() + '.svg' });
        else this.setState({ imgSrc: require('../../../img/noImage.png') });
        this.setState({ code: event.target.value });
    }    

    handleSubmit = (e) => {
        e.preventDefault();
        this.updateItem();
    }


    successUpdateCallback = (result) => {
       
        this.setState({
            actionAlert: 'update',
            actionMode: 'success'
        });

        this.setState({ showAlert: true }, () => {
            window.setTimeout(() => {
                this.setState({ showAlert: false })
            }, 2500)
        });
    }


    failureUpdateCallback(error) {
        this.setState({
            actionAlert: 'update',
            actionMode: error
        });

        this.setState({ showAlert: true }, () => {
            window.setTimeout(() => {
                this.setState({ showAlert: false })
            }, 2500)
        });
    }

    successCreateCallback = (result) => {
        this.setState({
            actionAlert: 'create',
            actionMode: 'success'
        });

        this.setState({ showAlert: true }, () => {
            window.setTimeout(() => {
                this.setState({ showAlert: false })
            }, 2500)
        });

        console.log('successCreateCallback', result);
        let createdObj = result;
        createdObj.imgSrc = '';
        this.props.onRowSelect(createdObj);
    }

    failureCreateCallback(error) {
        this.setState({
            actionAlert: 'create',
            actionMode: error
        });

        this.setState({ showAlert: true }, () => {
            window.setTimeout(() => {
                this.setState({ showAlert: false })
            }, 2500)
        });
    }

    successDeleteCallback = (result) => {

        this.props.onDetailsClose()
        this.setState({
            actionAlert: 'delete',
            actionMode: 'success'
        });

        this.setState({ showAlert: true }, () => {
            window.setTimeout(() => {
                this.setState({ showAlert: false })
            }, 2500)
        });
    }

    failureDeleteCallback = (result) => {

        this.setState({
            actionAlert: 'delete',
            actionMode: result
        });

        this.setState({ showAlert: true }, () => {
            window.setTimeout(() => {
                this.setState({ showAlert: false })
            }, 2500)
        });
    }


    deleteItem = () => {
        console.log('DELETE Object ID');
        console.log(this.state.id);

        let action = getForAction(matchDict, this.state.title);
        let answer = window.confirm('Are you sure you want to delete this item? ');
        if (answer) {
            const deleteItem = deleteItemAsync(action + '/Delete' + this.state.title + '?id=' + this.state.id);
            deleteItem
                .then((result) => {
                    //this.props.deleteEntity(this.state.title, this.state.id);
                    this.successDeleteCallback(result);
                },
                    (error) => {
                        this.failureDeleteCallback(error);
                    }
                );
        }
    }

    updateItem = () => {
        console.log('CREATE/UPDATE Object');
        console.log(this.state);

        let action = getForAction(matchDict, this.state.title);
        if (this.state.id)
            updateItemAsync(action + '/Update' + this.state.title + '?id=' + this.state.id, this.state)
                .then((result) => {
                        //this.props.updateEntity(this.state.title, result);
                        this.successUpdateCallback(result)
                    },
                    (error) => {
                        this.failureUpdateCallback(error)
                    }
                )

        else
            createItemAsync(action + '/Create' + this.state.title, { id: '', name: this.state.name, code: this.state.code })
                .then((result) => {
                    //this.props.addEntity(this.state.title, result);
                    this.successCreateCallback(result);
                },
                    (error) => {
                        this.failureCreateCallback(error);
                    }
                );
    }

    handleEscape = (e) => {
        if (e.key === 'Escape') this.props.onDetailsClose();
    }

    componentDidMount() {
        console.log('componentDidMount');
        window.addEventListener('keyup', this.handleEscape)
    }

    componentDidUpdate(previousProps, previousState) {
        console.log('componentDidUpdate');
        if (previousProps.active !== this.props.active) {
            this.setState({
                id: (this.props.active.id) ? this.props.active.id : '',
                name: (this.props.active.name) ? this.props.active.name : '',
                code: (this.props.active.code) ? this.props.active.code : '',
                imgSrc: (this.props.active.imgSrc) ? this.props.active.imgSrc : require('../../../img/noImage.png'),
                title: (this.props.type) ? this.props.type : ''
            })
        }
    }

    componentWillUnmount() {
        console.log('componentWillUnmount');
        window.removeEventListener('keyup', this.handleEscape);
    }

    render() {
        return (
            <React.Fragment >
                <div className="thumbnail border " style={{ padding: 15 }}>
                    <h4>{this.state.title} details</h4>
                    <br />
                    <div><img src={this.state.imgSrc} style={{ minWidth: 35, maxWidth: 45, width: 45, height: 35 }} /></div>
                    <div style={{ float: 'right', marginTop: -100, marginRight: -12 }}><CloseButton onClick={this.props.onDetailsClose} /></div>
                    <br />
                    <Form>
                        <Form.Group className="mb-3" controlId="formId" >
                            <Form.Control type="text" value={this.state.id} readOnly />
                            <Form.Text className="text-muted" style={{ float: 'left' }}>
                                It is the ID of current item or empty if new (int/UID)
                            </Form.Text>
                        </Form.Group>
                        <br />
                        <Form.Group className="mb-3" controlId="formTitle">
                            <Form.Control type="text" placeholder="Enter name (required)" value={this.state.name} required name="name" onChange={this.handleNameChange} />
                        </Form.Group>
                        {
                            this.state.title == 'Country' ?
                                <Form.Group className="mb-3" controlId="formCode" >
                                    <Form.Control type="text" placeholder="Enter code (required)" value={this.state.code} name="code" onChange={this.handleCodeChange} maxLength="2" />
                                    <Form.Text className="text-muted" style={{ float: 'left' }}>
                                        It is to show the flag
                                    </Form.Text>
                                    <br /><br />
                                </Form.Group>                                
                            :
                            ''
                        }
                        <Button variant="outline-primary" type="submit" size="sm" onClick={this.handleSubmit} style={{ marginRight: 7 }}>
                            Update
                        </Button>
                        {this.state.id ?
                            <Button variant="outline-danger" size="sm" onClick={this.deleteItem} style={{ marginRight: 7 }} >
                                Delete
                            </Button>
                            :
                            ''
                        }
                        <Button variant="outline-secondary" size="sm" onClick={this.props.onDetailsClose} >
                            Cancel
                        </Button>
                    </Form>
                </div>
                {this.state.showAlert ?
                    <AlertTemplate show={this.state.showAlert} mode={this.state.actionMode } action={this.state.actionAlert} /> :
                    null
                }

            </React.Fragment>
        );
    }
}

let mapDispatch = (dispatch) => {
    return {
        updateEntity: (entityType, item) => dispatch(entityActions.updateEntity(entityType, item)),
        addEntity: (entityType, item) => dispatch(entityActions.addEntity(entityType, item)),
        deleteEntity: (entityType, id) => dispatch(entityActions.deleteEntity(entityType, id)),
    }
}

export default connect(null, mapDispatch)(EditForm)


