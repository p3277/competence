import React from 'react';
import Parser from 'html-react-parser';

function TRow(props) {
    return <tr data-id={props.row['id']} style={{ cursor: 'pointer' }}  >
        {Object.keys(props.row).map((cell, ndx) => {
            if (props.columns != undefined && ndx < props.columns.length) {
                if (props.columns[ndx].type != "hidden" && props.columns[ndx].type != "object") {
                    let imgSrc = props.row['imgSrc'];
                    if (props.columns[ndx].type == 'iconed')
                        return <td key={ndx} style={{ textAlign: 'left', verticalAlign: 'middle', }} onClick={props.onRowSelect.bind(null, props.row)} >
                            <img alt={props.row[cell]} title={props.row[cell]}
                                src={imgSrc}
                                style={{ maxWidth: 23, marginRight: 7 }} />{Parser(props.row[cell])}</td>;

                    else if (props.columns[ndx].type == 'photo')
                        return <td key={ndx} style={{ textAlign: 'left', verticalAlign: 'middle', }} onClick={props.onRowSelect.bind(null, props.row)} >
                            <img src={imgSrc} className="rounded-circle"
                                 style={{ maxWidth: 40, marginRight: 7 }} />{Parser(props.row[cell])}</td>;
                    else
                        return <td key={ndx} style={{
                            textAlign: 'left', 
                            verticalAlign: 'middle',
                            alignItems: 'center'
                        }}>{Parser(props.row[cell])}</td>;
                }
                else if (props.columns[ndx].type == "object") {
                    return <td key={ndx} style={{ textAlign: 'left', verticalAlign: 'middle', }}  >{props.row[cell].name}</td>;
                }                
            }
        })}
    </tr>;
}

export default TRow;
