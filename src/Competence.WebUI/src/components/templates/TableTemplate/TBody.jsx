import React from 'react';
import TRow from './TRow.jsx';

function TBody(props) {
    return <tbody>{props.rows.data.map((row, ndx) => <TRow rowIndex={ndx} key={ndx} customRenderer={props.customRenderer} columns={props.columns} row={row} onRowSelect={props.onRowSelect} ></TRow>)}</tbody>
}

export default TBody;