import React from "react";

function THead(props) {
    return <thead>
        <tr>
            {
                props.columns.filter(col => col.type !== "hidden").map(col => {
                    return <th key={col.name} style={{ textAlign: 'left' }}>{props.columnRenderer(col.title)}</th>;
                }
            )}
        </tr>
    </thead>
}

export default THead;