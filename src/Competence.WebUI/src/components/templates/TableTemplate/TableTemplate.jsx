import React from 'react';
import THead from './THead.jsx';
import TBody from './TBody.jsx';
/*import { theme } from './TableStyle';*/

export function TableTemplate(props) {
    const customRenderer = (row) => {
        return <span>{row.name}</span>;
    };
    const columnRenderer = (column) => {
        return column.slice(0, 1).toUpperCase() + column.slice(1, column.length);
    }

    return <>
        <div className="table-responsive" >
            <table className="table table-striped table-bordered table-hover table-md" >
                <THead columnRenderer={columnRenderer} columns={props.columns.columns}></THead>
                <TBody customRenderer={customRenderer} columns={props.columns.columns} rows={props.data} onRowSelect={props.onRowSelect}></TBody>
            </table>
        </div>
     </>;
}

export function renderTableInnerContent(columns, items, title, icon, onRowSelect) {
    if (items == [] || items.length === 0)
        return <p className="p-5">Loading...</p>;
    else if (items.length > 0) {
        if (icon != undefined)
            items.forEach(function (element) {
                element.imgSrc = icon;
            });
        return (
            <TableTemplate columns={{ columns: columns }} data={{ data: items }} onRowSelect={onRowSelect} />
        );
    }
    else return (<p className="p-5" >No data to display</p>)
}
