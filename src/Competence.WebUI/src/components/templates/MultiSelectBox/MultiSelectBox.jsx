import React, { useState } from 'react';
import AsyncSelect from 'react-select/async';
import { URL } from '../../../utils/Constants.jsx'

export function MultiSelectBox(props) {

    const [inputValue, setInputValue] = useState('');

    const customStyles = {
        control: css => ({
            ...css,
            width: 335,
            textAlign: 'left'
        }),
        menu: ({ width, ...css }) => ({
            ...css,
            width: '335px',
            minWidth: '20%',
            textAlign: 'left',
            overflowX: 'hidden'
        }),
        option: css => ({ ...css, width: 335, textAlign: 'left', overflowX: 'hidden' }),
    };


    // load options using API call
    const loadOptions = () => {
        let ds = fetch(URL + props.action).then(res => res.json());
        return ds;
    }

    const handleInputChange = (newValue) => {

        console.log(newValue);

        const inputValue = newValue.replace(/\W/g, '');
        setInputValue({ inputValue });
        return inputValue;
    }

    return (
        <div className="multiSelectBox" >
            <AsyncSelect
                placeholder={props.placeholderTitle}
                cacheOptions
                defaultOptions
                isMulti={props.isMulti}
                getOptionLabel={e => e.name}
                getOptionValue={e => e.id}
                loadOptions={loadOptions}
                onChange={props.onChange}
                styles={props.customStyles ? props.customStyles : customStyles}
                value={props.value}

                isSearchable
                onInputChange={handleInputChange}
            />
            {/*<pre>Selected Value: {JSON.stringify(selectedValue || {}, null, 2)}</pre>*/}
        </div>
    );
}