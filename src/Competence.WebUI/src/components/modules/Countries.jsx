import React, { Component } from 'react'
import { connect } from 'react-redux';
import { renderTableInnerContent } from '../templates/TableTemplate/TableTemplate.jsx';
import { DetailsItem } from '../templates/ReadonlyDetailsForm/DetailsItem.jsx';
import { SearchBox } from '../templates/SearchBox/SearchBox.jsx';
import { getItemsAsync } from '../../utils/Core.jsx';
import Button from 'react-bootstrap/Button';
import { isAdmin } from '../../utils/Constants.jsx'
import EditForm from '../templates/EditableDetailsForm/EditableDetailsItem.jsx';
import entityActions from '../../store/actions/entityActions';
import connection from '../../utils/signalrConnection';



let countriesAction = 'Countries/GetCountries';
const columns = [
    {
        name: "id",
        type: "hidden",
        title: "ID"
    },
    {
        name: "name",
        type: "iconed",
        title: "Country name"
    }
];

class Countries extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            filteredData: [],
            tableWidth: 100,
            cardWidth: 0,
            active: null,
            type: 'Country',
            description: ' is a fundamental essence in current application. 1: a nation with its own government, occupying a particular territory. 2: districts and small settlements outside large urban areas or the capital.',
            showDetails: false
        };
        this.onFiltered = this.onFiltered.bind(this);
        this.onRowSelect = this.onRowSelect.bind(this);
        this.onDetailsClose = this.onDetailsClose.bind(this);
    }


    componentDidMount() {
        getItemsAsync(countriesAction).then((result) => {
            this.props.setCountries(result);
        });

        connection.start();

        connection.on("ADD_COUNTRY", data => {
            this.props.addCountry(data);
        });

        connection.on("UPDATE_COUNTRY", data => {
            this.props.updateCountry(data);
        });

        connection.on("DELETE_COUNTRY", data => {
            this.props.deleteCountry(data);
            if (this.state.active?.id === data && this.state.showDetails) {
                this.onDetailsClose();
            }
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.items !== this.props.items) {
            this.setState({ filteredData: this.props.items.Countries });
        }
    }

    search = (value) => {
        this.onDetailsClose();
        const filter = this.props.items.Countries.filter(item => {
            return item.name.toLowerCase().includes(value.toLowerCase());
        });

        this.onFiltered(filter);
    }

    onFiltered(items) {
        this.setState({ filteredData: items });
    }

    onRowSelect(row) {
        this.setState({ active: row });
        this.setState({ showDetails: true });
        this.setState({ tableWidth: 75 });
        this.setState({ cardWidth: 25 });
    }

    onDetailsClose() {
        this.setState({ tableWidth: 100 });
        this.setState({ cardWidth: 0 });
        this.setState({ active: null });
        this.setState({ showDetails: false });
    }

    render() {
        if (this.state.filteredData.length > 0)
            this.state.filteredData.forEach(function (element) {
                element.imgSrc = 'http://purecatamphetamine.github.io/country-flag-icons/3x2/' + element.code.toUpperCase() + '.svg';
            });

        return (
            <React.Fragment>
                <main className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
                        <div className="btn-toolbar mb-2 mb-md-0">
                            <div className="btn-group me-2">
                                <SearchBox placeholderTitle='Search...' onChange={event => this.search(event.target.value)} width='400' />
                            </div>
                        </div>

                        {
                            isAdmin ?
                                <div style={{ float: 'right' }}>
                                    <Button variant="outline-secondary" className="btnClass" onClick={this.onRowSelect}>Add new</Button>
                                </div>
                                :
                                ''
                        }
                        
                    </div>
                    <div className="row">
                        <div className="tableContent" style={{ width: this.state.tableWidth + '%' }}>
                            {renderTableInnerContent(columns, this.state.filteredData, '', null, this.onRowSelect)}
                        </div>
                        <div className="divCard" style={{ width: this.state.cardWidth + '%', minWidth: 300, minHeight: 750, backgroundColor: 'white', overflowY: 'auto' }}>
                            <div style={{ height: 300 }}>
                                {
                                    this.state.showDetails ?
                                        isAdmin && this.state.active ?
                                            <EditForm active={this.state.active} type={this.state.type} onDetailsClose={this.onDetailsClose} onRowSelect={this.onRowSelect} />
                                        :
                                            <DetailsItem data={this.state.filteredData} active={this.state.active} type={this.state.type} description={this.state.description} onDetailsClose={this.onDetailsClose} />
                                        :
                                        ''
                                }
                            </div>
                        </div>
                    </div>
                </main>
            </React.Fragment>
        )
    }
}

let mapProps = (state) => {
    return {
        items: state.Countries
    }
}

let mapDispatch = (dispatch) => {
    return {
        setCountries: (items) => dispatch(entityActions.setEntities("COUNTRY", items)),
        addCountry: (item) => dispatch(entityActions.addEntity("COUNTRY", item)),
        updateCountry: (item) => dispatch(entityActions.updateEntity("COUNTRY", item)),
        deleteCountry: (id) => dispatch(entityActions.deleteEntity("COUNTRY", id)),
    }
}

export default connect(mapProps, mapDispatch)(Countries)