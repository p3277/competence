import React, { Component } from 'react'
import { connect } from 'react-redux';
import Button from 'react-bootstrap/Button';
import { isAdmin } from '../../utils/Constants.jsx'
import { renderTableInnerContent } from '../templates/TableTemplate/TableTemplate.jsx';
import  EditForm  from '../templates/EditableDetailsForm/EditableDetailsItem.jsx';
import { DetailsItem } from '../templates/ReadonlyDetailsForm/DetailsItem.jsx';
import { getItemsAsync } from '../../utils/Core.jsx';
import { SearchBox } from '../templates/SearchBox/SearchBox.jsx';
import entityActions from '../../store/actions/entityActions';
import connection from '../../utils/signalrConnection';

let groupPositionsAction = 'Positions/GetPositions';


const columns = [
    {
        name: "id",
        type: "hidden",
        title: "ID"
    },
    {
        name: "name",
        type: "iconed",
        title: "Position name"
    }    
]

class Positions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            filteredData: [],
            tableWidth: 100,
            cardWidth: 0,
            active: null,
            type: 'Position',
            description: ' is a fundamental essence in current application. 1: the way in which something or someone is placed or arranged The seat is in the upright position. 2: a way of looking at or considering things Whats your position on the issue? 3: the place where a person or thing is or should be They took their positions on stage.',
            showDetails: false
        };
        this.onFiltered = this.onFiltered.bind(this);
        this.onRowSelect = this.onRowSelect.bind(this);
        this.onDetailsClose = this.onDetailsClose.bind(this);
    }


    componentDidMount() {
        getItemsAsync(groupPositionsAction).then((result) => {
            this.props.setPositions(result);
        });

        connection.start();

        connection.on("ADD_POSITION", data => {
            this.props.addPosition(data);
        });

        connection.on("UPDATE_POSITION", data => {
            this.props.updatePosition(data);
        });

        connection.on("DELETE_POSITION", data => {
            this.props.deletePosition(data);
            if (this.state.active?.id === data && this.state.showDetails) {
                this.onDetailsClose();
            }
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.items !== this.props.items) {
            this.setState({ filteredData: this.props.items.Positions });
        }
    }


    search = (value) => {
        const filter = this.props.items.Positions.filter(item => {
            return item.name.toLowerCase().includes(value.toLowerCase());
        });

        this.onFiltered(filter);
    }

    onFiltered(items) {
        this.setState({ filteredData: items });
    }

    onRowSelect(row) {
        this.setState({ tableWidth: 75 });
        this.setState({ cardWidth: 25 });
        this.setState({ active: row });
        this.setState({ showDetails: true });
    }

    onDetailsClose() {
        this.setState({ tableWidth: 100 });
        this.setState({ cardWidth: 0 });
        this.setState({ active: null });
        this.setState({ showDetails: false });
    }

    render() {
        let imageSource = require('../../img/template.png');
        return (
            <React.Fragment >
                <main className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
                        <div className="btn-toolbar mb-2 mb-md-0">
                            <div className="btn-group me-2">
                                <SearchBox placeholderTitle='Search...' onChange={event => this.search(event.target.value)} width='400' />
                            </div>
                        </div>
                        {
                            isAdmin ?
                                <div style={{ float: 'right' }}>
                                    <Button variant="outline-secondary" className="btnClass" onClick={this.onRowSelect}>Add new</Button>
                                </div>
                                :
                                ''
                        }
                    </div>
                    <div className="row">
                        <div className="tableContent" style={{ width: this.state.tableWidth + '%' }}>
                            {renderTableInnerContent(columns, this.state.filteredData, 'Positions', imageSource, this.onRowSelect)}
                        </div>
                        <div className="divCard" style={{ width: this.state.cardWidth + '%', minWidth: 300, minHeight: 750, backgroundColor: 'white', overflowY: 'auto' }}>
                            <div style={{ height: 300 }}>
                                {
                                    this.state.showDetails ?
                                        isAdmin && this.state.active ?
                                            <EditForm active={this.state.active} type={this.state.type} onDetailsClose={this.onDetailsClose} onRowSelect={this.onRowSelect} />
                                            :
                                            <DetailsItem data={this.state.filteredData} active={this.state.active} type={this.state.type} description={this.state.description} onDetailsClose={this.onDetailsClose} />
                                        :
                                        ''
                                }
                            </div>
                        </div>
                    </div>
                </main>
            </React.Fragment>
        )
    }
}

let mapProps = (state) => {
    return {
        items: state.Positions
    }
}

let mapDispatch = (dispatch) => {
    return {
        setPositions: (items) => dispatch(entityActions.setEntities("POSITION", items)),
        addPosition: (item) => dispatch(entityActions.addEntity("POSITION", item)),
        updatePosition: (item) => dispatch(entityActions.updateEntity("POSITION", item)),
        deletePosition: (id) => dispatch(entityActions.deleteEntity("POSITION", id)),
    }
}

export default connect(mapProps, mapDispatch)(Positions)