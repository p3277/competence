import React, { Component } from 'react'
import { connect } from 'react-redux';
import Button from 'react-bootstrap/Button';
import { isAdmin } from '../../utils/Constants.jsx'
import { renderTableInnerContent } from '../templates/TableTemplate/TableTemplate.jsx';
import  EditForm  from '../templates/EditableDetailsForm/EditableDetailsItem.jsx';
import { DetailsItem } from '../templates/ReadonlyDetailsForm/DetailsItem.jsx';
import { getItemsAsync } from '../../utils/Core.jsx';
import { SearchBox } from '../templates/SearchBox/SearchBox.jsx';
import entityActions from '../../store/actions/entityActions';
import connection from '../../utils/signalrConnection';

let groupDepartmentsAction = 'Departments/GetDepartments';

const columns = [
    {
        name: "id",
        type: "hidden",
        title: "ID"
    },
    {
        name: "name",
        type: "iconed",
        title: "Department name"
    }    
]

class Departments extends Component {
    constructor(props) {
        super(props);

        this.state = {
            filteredData: [],
            tableWidth: 100,
            cardWidth: 0,
            active: null,
            type: 'Department',
            description: ' is a fundamental essence in current application. 1: a division of a large organization such as a government, university, or business, dealing with a specific area of activity. 2: an administrative district in France and other countries.',
            showDetails: false
        };

        this.onFiltered = this.onFiltered.bind(this);
        this.onRowSelect = this.onRowSelect.bind(this);
        this.onDetailsClose = this.onDetailsClose.bind(this);
        
    }

    componentDidMount() {
        getItemsAsync(groupDepartmentsAction).then((result) => {
            this.props.setDepartments(result);
        });

        connection.start();

        connection.on("ADD_DEPARTMENT", data => {
            this.props.addDepartment(data);
        });

        connection.on("UPDATE_DEPARTMENT", data => {
            this.props.updateDepartment(data);
        });

        connection.on("DELETE_DEPARTMENT", data => {
            this.props.deleteDepartment(data);
            if (this.state.active?.id === data && this.state.showDetails) {
                this.onDetailsClose();
            }
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.items !== this.props.items) {
            this.setState({ filteredData: this.props.items.Departments });
        }
    }

    search = (value) => {
        const filter = this.props.items.Departments.filter(item => {
            return item.name.toLowerCase().includes(value.toLowerCase());
        });

        this.onFiltered(filter);
    }

    onFiltered(items) {
        this.setState({ filteredData: items });
    }

    onRowSelect(row) {
        this.setState({ tableWidth: 75 });
        this.setState({ cardWidth: 25 });
        this.setState({ active: row });
        this.setState({ showDetails: true });
    }

    onDetailsClose() {
        this.setState({ tableWidth: 100 });
        this.setState({ cardWidth: 0 });
        this.setState({ active: null });
        this.setState({ showDetails: false });
    }

    render() {
        let imageSource = require('../../img/department.png');
        return (
            <React.Fragment >
                <main className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
                        <div className="btn-toolbar mb-2 mb-md-0">
                            <div className="btn-group me-2">
                                <SearchBox placeholderTitle='Search...' onChange={event => this.search(event.target.value)}width='400' />
                            </div>
                        </div>
                        {
                            isAdmin ?
                                <div style={{ float: 'right' }}>
                                    <Button variant="outline-secondary" className="btnClass" onClick={this.onRowSelect}>Add new</Button>
                                </div>
                                :
                                ''
                        }
                    </div>
                    <div className="row">
                        <div className="tableContent" style={{ width: this.state.tableWidth + '%' }}>
                            {renderTableInnerContent(columns, this.state.filteredData, 'Deparments', imageSource, this.onRowSelect)}
                        </div>
                        <div className="divCard" style={{ width: this.state.cardWidth + '%', minWidth: 300, minHeight: 750, backgroundColor: 'white', overflowY: 'auto' }}>
                            <div style={{ height: 300 }}>
                                {
                                    this.state.showDetails ?
                                        isAdmin && this.state.active ?
                                            <EditForm active={this.state.active} type={this.state.type} onDetailsClose={this.onDetailsClose} onRowSelect={this.onRowSelect} />
                                            :
                                            <DetailsItem data={this.state.filteredData} active={this.state.active} type={this.state.type} description={this.state.description} onDetailsClose={this.onDetailsClose} />
                                        :
                                        ''
                                }
                            </div>
                        </div>
                    </div>
                </main>
            </React.Fragment>
        )
    }
}

let mapProps = (state) => {
    return {
        items: state.Departments
    }
}

let mapDispatch = (dispatch) => {
    return {
        setDepartments: (items) => dispatch(entityActions.setEntities("DEPARTMENT", items)),
        addDepartment: (item) => dispatch(entityActions.addEntity("DEPARTMENT", item)),
        updateDepartment: (item) => dispatch(entityActions.updateEntity("DEPARTMENT", item)),
        deleteDepartment: (id) => dispatch(entityActions.deleteEntity("DEPARTMENT", id)),
    }
}

export default connect(mapProps, mapDispatch)(Departments)