import React, { Component } from 'react'
import { connect } from 'react-redux';
import { iterateArraysAndGetResult } from '../../utils/Core.jsx';
import Button from 'react-bootstrap/Button';
import { isAdmin } from '../../utils/Constants.jsx'
import { renderTableInnerContent } from '../templates/TableTemplate/TableTemplate.jsx';
import EditFormEmployee from '../templates/EditableDetailsForm/EditableDetailsEmployee.jsx';
import { DetailsEmployee } from '../templates/ReadonlyDetailsForm/DetailsEmployee.jsx';
import { MultiSelectBox } from '../templates/MultiSelectBox/MultiSelectBox.jsx';
import { getItemsAsync } from '../../utils/Core.jsx';
import { SearchBox } from '../templates/SearchBox/SearchBox.jsx';
import entityActions from '../../store/actions/entityActions';
import connection from '../../utils/signalrConnection';

let groupEmployeesAction = 'Employee/GetEmployees';

const columns = [
    {
        name: "id",
        type: "hidden",
        title: "ID"
    },
    {
        name: "firstName",
        type: "hidden",
        title: "First name"
    },
    {
        name: "middleName",
        type: "hidden",
        title: "Middle name"
    },
    {
        name: "lastName",
        type: "hidden",
        title: "Last name"
    },    
    {
        name: "fullName",
        type: "photo",
        title: "Employee name"
    },
    {
        name: "email",
        type: "hidden",
        title: "Email"
    },
    {
        name: "photo",
        type: "hidden",
        title: "Photo"
    },
    {
        name: "birthdaye",
        type: "hidden",
        title: "Birthday"
    },
    {
        name: "country.name",
        type: "object",
        title: "Country"
    },
    {
        name: "department.name",
        type: "object",
        title: "Department"
    },
    {
        name: "competencies",
        type: "hidden",
        title: "Competences"
    },
    {
        name: "position.name",
        type: "object",
        title: "Position"
    },
    {
        name: "competencesList",
        type: "string",
        title: "Competences"
    }
]

let arrFiltered = [];
let filterDepartments = [];
let filterCountries = [];
let filterTermBySearch = '';

let filteredWholeListByDepartment = [];
let filteredWholeListByCountries = [];
let filteredWholeListBySearch = [];

class Employees extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            filteredData: [],
            tableWidth: 100,
            cardWidth: 0,
            cardVisible: 'none',
            active: null,
            type: 'Position',
            description: ' is a fundamental worker in PSI. ',
            showDetails: false
        };
        this.onFiltered = this.onFiltered.bind(this);
        this.onRowSelect = this.onRowSelect.bind(this);
        this.onDetailsClose = this.onDetailsClose.bind(this);
    }


    componentDidMount() {
        getItemsAsync(groupEmployeesAction).then((result) => {
            this.props.setEmployees(result);
        });

        connection.start();

        connection.on("ADD_EMPLOYEE", data => {
            this.props.addEmployee(data);
        });

        connection.on("UPDATE_EMPLOYEE", data => {
            this.props.updateEmployee(data);
        });

        connection.on("DELETE_EMPLOYEE", data => {
            this.props.deleteEmployee(data);
            if (this.state.active?.id === data && this.state.showDetails) {
                this.onDetailsClose();
            }
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.items !== this.props.items) {
            this.setState({ filteredData: this.props.items.Employees });
        }
    }

    onFiltered() {
        arrFiltered = [];
        const data = this.props.items.Employees;

        arrFiltered = iterateArraysAndGetResult([filteredWholeListByDepartment, filteredWholeListByCountries, filteredWholeListBySearch], data);

        if (filterDepartments.length < 1 && filterCountries.length < 1 && filterTermBySearch === '')
            arrFiltered = data;
        this.setState({ filteredData: arrFiltered });
    }

    search = (value) => {
        filterTermBySearch = value.toLowerCase();
        let filter = [];
        filteredWholeListBySearch = [];
        filter = this.props.items.Employees.filter(item => {
            return item.fullName.toLowerCase().includes(value.toLowerCase());
        });
        filteredWholeListBySearch = filter;
        if (filterTermBySearch === '')
            filteredWholeListBySearch = [];
        this.onFiltered();
    }

    handleChangeDepartment = (value) => {
        let arrFilter = [];
        filteredWholeListByDepartment = [];
        if (value.length > 0)
            value.forEach(function (item) {
                arrFilter.push(item.id)
            });

        filterDepartments = arrFilter;
        let data = this.props.items.Employees;
        data.forEach(function (item) {
            if (filterDepartments.indexOf(item.department.id) !== -1)
                filteredWholeListByDepartment.push(item);
        });
        this.onFiltered();
    }

    handleChangeCountry = value => {
        let arrFilter = [];
        filteredWholeListByCountries = [];
        if (value.length > 0)
            value.forEach(function (item) {
                arrFilter.push(item.id)
            });

        filterCountries = arrFilter;
        let data = this.props.items.Employees;
        data.forEach(function (item) {
            if (filterCountries.indexOf(item.country.id) !== -1)
                filteredWholeListByCountries.push(item);
        });
        this.onFiltered();
    }

    onRowSelect(row) {
        this.setState({ tableWidth: 75 });
        this.setState({ cardVisible: '' });
        this.setState({ active: row });
        this.setState({ showDetails: true });
    }

    onDetailsClose() {
        this.setState({ tableWidth: 100 });
        this.setState({ cardWidth: 0 });
        this.setState({ cardVisible: 'none' });
        this.setState({ active: null });
        this.setState({ showDetails: false });
    }

    render() {
        if (this.state.filteredData.length > 0)
            this.state.filteredData.forEach(function (element) {
                element.competencesList = '';
                element.competencies.forEach(function (competence) {
                    element.competencesList += '<span className="tag">' + competence.name + '</span>&nbsp;';
                });
                element.fullName = '<div className="classNameEmployee"><span>' + element.lastName + ' ' + element.firstName + '</span><span><a href="mailto:' + element.email + '">' + element.email + '</span></span></div>';
                if (!element.imgSrc) {
                   
                    element.imgSrc = element.photo;
                }
            });
        return (
            <React.Fragment>
                <main className="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
                        <div className="btn-toolbar mb-2 mb-md-0">
                            <div className="btn-group me-2">
                                <div className="col-sm-3">
                                    <SearchBox placeholderTitle='Search...' onChange={event => this.search(event.target.value)} width='200' />
                                </div>
                                <div className="col-sm-5">
                                    <MultiSelectBox action='Departments/GetDepartments' placeholderTitle='Select Department.....' onChange={this.handleChangeDepartment} isMulti="true" />
                                </div>
                                <div className="col-sm-5">
                                    <MultiSelectBox action='Countries/GetCountries' placeholderTitle='Select Country.....' onChange={this.handleChangeCountry} isMulti="true" />
                                </div>
                            </div> 
                        </div>
                        {
                            isAdmin ?
                                <div style={{ float: 'right' }}>
                                    <Button variant="outline-secondary" className="btnClass" onClick={this.onRowSelect}>Add new</Button>
                                </div>
                                :
                                ''
                        }
                    </div>
                    <div className="row">
                        <div className="tableContent" style={{ width: this.state.tableWidth + '%' }}>
                            {renderTableInnerContent(columns, this.state.filteredData, 'Employees', null, this.onRowSelect)}
                        </div>
                        <div className="divCard" style={{ display: this.state.cardVisible, width: this.state.cardWidth + '%', minWidth: '25%', minHeight: 770, backgroundColor: 'white', overflowY: 'auto' }}>
                            <div style={{ height: 300 }}>
                                {/*<DetailsEmployee data={this.state.filteredData} active={this.state.active} type={this.state.type} description={this.state.description} onDetailsClose={this.onDetailsClose} />*/}
                                {
                                    this.state.showDetails ?
                                        isAdmin && this.state.active ?
                                            <EditFormEmployee active={this.state.active} type={this.state.type} onDetailsClose={this.onDetailsClose} onRowSelect={this.onRowSelect} />
                                            :
                                            <DetailsEmployee data={this.state.filteredData} active={this.state.active} type={this.state.type} description={this.state.description} onDetailsClose={this.onDetailsClose} />
                                        :
                                        ''
                                }
                            </div>
                        </div>
                    </div>                    
                </main>

                

            </React.Fragment>
        )
    }
}

let mapProps = (state) => {
    return {
        items: state.Employees
    }
}

let mapDispatch = (dispatch) => {
    return {
        setEmployees: (items) => dispatch(entityActions.setEntities("EMPLOYEE", items)),
        addEmployee: (item) => dispatch(entityActions.addEntity("EMPLOYEE", item)),
        updateEmployee: (item) => dispatch(entityActions.updateEntity("EMPLOYEE", item)),
        deleteEmployee: (id) => dispatch(entityActions.deleteEntity("EMPLOYEE", id)),
    }
}


export default connect(mapProps, mapDispatch)(Employees)