import React from 'react';
import Header from './Header.jsx';
import Footer from './Footer.jsx';
import SideBar from './SideBar.jsx';
import RequireAuth from '../../utils/requireAuth';

const Layout = ({ children }) => {
    return (
        <div className="App">
            <Header />
            <SideBar />
            <RequireAuth>
                {children}
            </RequireAuth>
            <Footer />
        </div>
    )
}

export default Layout;