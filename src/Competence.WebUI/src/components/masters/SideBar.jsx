import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class SideBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            links: [
                { path: "/", text: "Employees", isActive: false, marginRight: 12, imgSrc: require('../../img/profile.png') },
                { path: "/departments", text: "Departments", isActive: false, marginRight: 13, imgSrc: require('../../img/department.png') },
                { path: "/groupcompetences", text: "Competences groups", isActive: false, marginRight: 12, imgSrc: require('../../img/gcompetence.png') },
                { path: "/competences", text: "Competences", isActive: false, marginRight: 23, imgSrc: require('../../img/competence.png') },
                { path: "/countriesredux", text: "Countries", isActive: false, marginRight: 17, imgSrc: require('../../img/country.png') },
                { path: "/positions", text: "Positions", isActive: false, marginRight: 17, imgSrc: require('../../img/template.png') }
            ]
        }
    }

    handleClick(i) {
        const links = this.state.links.slice();
        for (const j in links) 
            links[j].isActive = i === j;
        this.setState({ links: links });
    }
    
    render() {

        return (
            <nav id="sidebarMenu" className="col-md-3 col-lg-2 d-md-block bg-white sidebar collapse">
                <div className="position-sticky pt-3">
                    <ul className="nav flex-column" >
                        {this.state.links.map((link, i) =>
                            <li className={window.location.pathname === link.path ? 'nav-item activeItem' : 'nav-item'} key={link.path}>
                                <Link className='nav-link' to={link.path}>
                                    <img src={link.imgSrc} alt='sidebar ico' style={{ marginRight: link.marginRight, marginTop: -3 }} />
                                    {link.text}
                                </Link>

                        </li>
                        )}
                    </ul>
                </div>
            </nav>
        );
    }
}
