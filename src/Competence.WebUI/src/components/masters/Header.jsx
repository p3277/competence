import React from 'react';
import { signoutRedirect, signinRedirect } from '../../utils/userService';

export default class Header extends React.Component {

    handleLogin = () => {
        signoutRedirect();
    }

    render() {
        return (
            <header className="navbar navbar-light sticky-top bg-white flex-md-nowrap p-0 " >
                <a className="navbar-brand col-md-3 col-lg-2 px-3" href="/" style={{ color: 'navy' }}><h4>PSI Squad Competence</h4></a>
                <button className="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="w-100" ></div>
                <div className="navbar-nav" >
                    <div className="nav-item text-nowrap" style={{ marginRight: 15, color: 'navy', fontSize: 15 }}>
                        <a className="nav-link px-3" style={{ color: 'navy' }} href="#" onClick={this.handleLogin}><img src={require('../../img/logout.png')} alt='login' style={{ marginRight: 5, marginTop: -3, fontSize: 25 }} />logout</a>
                    </div>
                </div>
            </header>
        );
    }
};