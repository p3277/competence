import React, { useEffect } from 'react';
import '../../css/App.css';
import Employees from '../modules/Employees.jsx';
import CompetencesGroup from '../modules/CompetenceGroups.jsx';
import Competences from '../modules/Competences.jsx';
import Departments from '../modules/Departments.jsx';
import CountriesRedux from '../modules/Countries.jsx';
import Positions from '../modules/Positions.jsx';
import { BrowserRouter as Router, Route, Routes  } from 'react-router-dom';
import Layout from './Layout.jsx';
import SigninOidc from '../../utils/signin-oidc';
import SignoutOidc from '../../utils/signout-oidc';
import RequireAuth from '../../utils/requireAuth';

import { Provider } from 'react-redux'
import configureStore from '../../store/configureStore.js';
import userManager, { loadUserFromStorage } from '../../utils/userService.js';
import AuthProvider from '../../utils/authProvider'

function App() {
    const store = configureStore();
    useEffect(() => {
        // fetch current user from cookies
        loadUserFromStorage(store)
    }, [])

    return (
        <Provider store={store}>
            <AuthProvider userManager={userManager} store={store}>
                <Router>
                    <Routes>
                        <Route path="/" element={<Layout><Employees /></Layout>} />
                        <Route path="/departments" element={<Layout><Departments /></Layout>} />
                        <Route path="/groupcompetences" element={<Layout><CompetencesGroup /></Layout>} />
                        <Route path="/competences" element={<Layout><Competences /></Layout>} />
                        <Route path="/countriesredux" element={<Layout><CountriesRedux /></Layout>} />
                        <Route path="/positions" element={<Layout><Positions /></Layout>} />
                        <Route path="/signout-oidc" element={<SignoutOidc />} />
                        <Route path="/signin-oidc" element={<SigninOidc />} />
                    </Routes>
                </Router>
            </AuthProvider>
        </Provider>
        );
};

export default App;


