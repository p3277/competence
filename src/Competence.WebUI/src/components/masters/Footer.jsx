import React from 'react';

export default function Footer() {
    return (
        <footer className="text-sm px-8 text-center flex-none py-4 bg-dark fixed-bottom">
            <center><p className="text-light">PSI Squad 2022</p></center>
        </footer>
    )
}