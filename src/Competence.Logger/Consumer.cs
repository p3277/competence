﻿using Competence.Logger.Models;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;
using System;
using System.Text;
using System.Text.Json;
using System.Threading;

namespace Competence.Consumers
{
    public class Consumer
    {
        public Consumer()
        {
            Register(GetRabbitConnetion());
        }
        private static IConnection GetRabbitConnetion()
        {
            ConnectionFactory factory = new ConnectionFactory()
            {
                UserName = "guest",
                Password = "guest",
                HostName = "localhost"
            };
            IConnection connection = factory.CreateConnection();

            return connection;
        }
        private class User
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Prefix { get; set; }
        }

        public void Register(IConnection connectionInfo)
        {
            Log.Error("Can't create data base entry: ");

            string queueName = $"queue.loggers";
            string routingKey = $"loggers_routing";

            using (var connection = connectionInfo)
            {
                using (var channel = connection.CreateModel())
                {
                    channel.BasicQos(0, 10, false);
                    channel.QueueDeclare(queueName, false, false, false, null);
                    channel.QueueBind(queueName, $"exchange.loggers", routingKey, null);

                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += (sender, e) =>
                    {
                        Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ff")} Received message");

                        Thread.Sleep(TimeSpan.FromSeconds(2));
                        var body = e.Body;
                        var message = JsonSerializer.Deserialize<LoggerMessageDTO>(Encoding.UTF8.GetString(body.ToArray()));

                        var user = new User { Id = 124, Name = "AA BB", Prefix = "Mr" };


                        Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Information()
                        .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                        .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)

                        .Enrich.WithProperty("App", message.AppName)
                        .Enrich.WithProperty("Username", message.Username)

                        .Enrich.FromLogContext()
                        .WriteTo.Console()
                        .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri("http://localhost:9200"))
                        {
                            AutoRegisterTemplate = true,
                            AutoRegisterTemplateVersion = AutoRegisterTemplateVersion.ESv7,
                            IndexFormat = "Competence." + message.AppName
                        })
                        .CreateLogger();

                        switch (message.LogLevel)
                        {
                            case LogLevel.Debug:
                                {
                                    Log.Debug("{@message}", message.Content); break;
                                }
                            case LogLevel.Information:
                                {
                                    Log.Information("{@message}", message.Content); break;
                                }
                            case LogLevel.Warning:
                                {
                                    Log.Warning("{@message}", message.Content); break;
                                }
                            case LogLevel.Error:
                                {
                                    Log.Error("{@message}", message.Content); break;
                                }
                            case LogLevel.Critical:
                                {
                                    Log.Fatal("{@message}", message.Content); break;
                                }
                            default:
                                {
                                    Log.Verbose("{@message}", message.Content); break;
                                }
                        }


                        Console.WriteLine("  Content: {0}", message.Content.Message);
                        Console.WriteLine("  Application name: {0}", message.AppName);
                        Console.WriteLine("  Username: {0}", message.Username);
                        Console.WriteLine("  Exception: {0}", message.Content.Exception);
                    };

                    channel.BasicConsume(queueName, true, consumer);

                    Console.WriteLine("Subscribed to the queue");

                    Console.ReadLine();
                }
            }
        }
    }
}
